import React, { PureComponent } from 'react';
import { Layout } from './components/Layout';

export default class App extends PureComponent {
	displayName = App.name

    render() {
        return (
            <Layout />
        );
    }
}
