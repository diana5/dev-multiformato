import { Func } from '../func/func';
import conf from '../config.json';
import { func } from 'prop-types';

export const TicketService = (id) => {
	console.log("el tick service",id)
	let user = Func.getUser();
	const request = {
		idImage: id
	}
	fetch(`${conf.url.base}${conf.endpoint.getFile}/${request.idImage}`, {
		method: 'get',
		responseType: 'arraybuffer',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + user.access_token,
		}
	})
		.then(function (response) {
			console.log('response', response.headers.get('Content-Type'));
			response.blob().then(function (blob) {
				if (blob === undefined)
					return console.log('Log: ', { errorCode: 1, errorMessage: 'No existe respuesta' })

				var file = new Blob([blob], {type: 'application/pdf'});
				var fileURL = URL.createObjectURL(file);
				window.open(fileURL);
			})
		})
}
