import M from 'materialize-css';
import conf from '../config.json';

const request = {
	apid: conf.access.apid
}
let data = {
	data: {

	}
}

export const LocalService = () => {
	fetch(conf.url.base + conf.endpoint.getStoreList, {
		method: 'post',
		body: JSON.stringify(request),
		headers: {
			'Content-Type': 'application/json'
		}
	}).then(res => res.json())
		.catch(function (error) {
			console.log(conf.url.base);
		})
		.then(function (response) {
			console.log('Succes:', response);
			if (response === undefined)
				return console.log('Log: ', { errorCode: 1, errorMessage: 'No cargaron los locales' })

			if (response.errorCode === 0 && !response.errorMessage) {
				const listLocal = response.locales;
				listLocal.map((item) => {
					let num = item.numLocal;
					data.data[num] = null;
					return true;
				})
			}else {
				console.log('Error:', response);
				switch (response.errorCode) {
					case -6:
						console.log(response.errorMessage)
						break;

					default:
						console.log(response.errorMessage)
						break;
				}
			}
			let autoComplete = document.querySelectorAll('.autocomplete');
			M.Autocomplete.init(autoComplete, data);
		})
}
