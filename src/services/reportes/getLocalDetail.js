import getService from "./getService";
import { endpointLocalDetail } from "./endpoint";

const getLocalDetail = async request => {
	const response = await getService(endpointLocalDetail, request);
	console.log("request"+JSON.stringify(request))
	console.log("RESPUESTA SERVICIO"+JSON.stringify(response))
	return response;

};

export default getLocalDetail;
