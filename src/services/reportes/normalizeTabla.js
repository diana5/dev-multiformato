import { Fn } from "../../Utils";

const isNotExit = x => typeof x === undefined;

const normalizeTabla = (array = []) => {
	let newArray = [];

	typeof array !== undefined &&
		array.length > 0 &&
		array.map(local => {
			Object.keys(local.products).map((value, index) =>
				newArray.push({
					DOCUMENTOBOLETA: "BOLETA",
					FORMATORECEPTOR: local.originalTransaction.formato || " ",
					MERCADORECEPTOR: local.originalTransaction.mercado || " ",
					LOCALRECEPTOR: local.originalTransaction.numLocal || " ",
					RUTCLIENTERECEPTOR: Fn.formateaRut(local.customerId) || " ",
					FOLIOBOLETARECEPTOR: local.originalTransaction.folio || " ",
					FECHABOLETARECEPTOR:
						Fn.formatDate(local.originalTransaction.date) || " ",
					DOCUENTONC: "NC",
					FORMATOEMISOR: isNotExit(local.formato)
						? " "
						: local.formato,
					MERCADOEMISOR: isNotExit(local.mercado)
						? " "
						: local.mercado,
					LOCALEMISOR: isNotExit(local.loginLocal)
						? " "
						: local.loginLocal,
					RUTCOLABORADOREMISOR: isNotExit(local.docNbr)
						? " "
						:  Fn.formateaRut(local.docNbr),
					IDOPERADOREMISOR: isNotExit(local.opr) ? " " : local.opr,
					FOLIONCEMISOR: isNotExit(local.folio) ? " " : local.folio,
					MOTIVO: isNotExit(local.reasonDescription)
						? " "
						: local.reasonDescription,
					DOCUMENTOPRODUCTOS: "PRODUCTOS",
					DESCRIPCION: isNotExit(local.products[index].description)
						? " "
						: local.products[index].description,
					DEPARTAMENTO: isNotExit(local.products[index].deptNbr)
						? " "
						: local.products[index].deptNbr,
					EAN: isNotExit(local.products[index].ean)
						? " "
						: local.products[index].ean,
					CANTIDAD: local.products[index].grams===0
					? local.products[index].qty : (local.products[index].grams < 1000 && local.products[index].grams > 0) ? "0." +  local.products[index].grams 
					: String(local.products[index].grams).replace(/(.)(?=(\d{3})+$)/g, "$1,").replace(",","."),
					 
					TOTAL: local.total || " "
				})
			);
		});

	console.table(newArray);
	return newArray;
};

export default normalizeTabla;
