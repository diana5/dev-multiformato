import getService from "./getService";
import { endpointLocalDetailData } from "./endpoint";

const getLocalDetailData = async request => {
	const response = await getService(endpointLocalDetailData, request);
	return response;
};

export default getLocalDetailData;
