import conf from "../../config.json";

const baseUrl = conf.url.base;
export const endpointLocalDetail = baseUrl + conf.endpoint.getCreditNotes;
export const endpointLocalList = baseUrl + conf.endpoint.getCreditNotes;
export const endpointLocalDetailData =
	baseUrl + conf.endpoint.getCreditNoteData;
export const endpointLocalListData = baseUrl + conf.endpoint.getCreditNoteData;
