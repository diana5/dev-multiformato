import getLocalDetail from "./getLocalDetail";
import getLocalList from "./getLocalList";
import getLocalDetailData from "./getLocalDetailData";
import getLocalListData from "./getLocalListData";
import normalizeTabla from "./normalizeTabla";
import normalizeAj from "./normalizeAj";
import normalizeProducto from "./normalizeProducto";

export {
	getLocalDetail,
	getLocalList,
	getLocalDetailData,
	getLocalListData,
	normalizeTabla,
	normalizeAj,
	normalizeProducto
};
