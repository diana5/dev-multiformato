/**
 * Funcion para obetener datos atraves de un fetch
 * @param {string} endpoint
 * @param {Object} request
 */
const getService = async (endpoint = "", request = {}) => {
	const token = JSON.parse(sessionStorage.getItem("user"));
	try {
		const response = await fetch(endpoint, {
			method: "POST",
			body: JSON.stringify({ ...request }),
			headers: {
				"Content-Type": "application/json",
				Authorization: "Bearer " + token.access_token
			}
		});

		if (response.errorMessage || typeof response === undefined) {
			console.error(response);
			return;
		}

		const responseJson = await response.json();

		localStorage.setItem("totalSuma", responseJson.total);

		return responseJson;
	} catch (e) {
		console.error(e);
		return;
	}
};

export default getService;
