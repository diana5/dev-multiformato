import getService from "./getService";
import { endpointLocalList } from "./endpoint";

const getLocalList = async request => {
	console.log("Req"+request)
	const response = await getService(endpointLocalList, request);

	return response;
};

export default getLocalList;
