import { Func } from '../func/func';
import conf from '../config.json';
import print from 'print-js';
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content'
import SweetAlert from 'react-bootstrap-sweetalert';
import { isNull } from 'lodash';


//la impresion EN REPORTERIA (BOTON IMPRIMIR)

export const NoteService = (a, b, c, d, e, f, g) => {


	const request = {
		numLocal: a,
		date: b,
		folio: c,
		ncCliente: 1,
		ncTienda: 1,
		voucher: 0,
		ticketMF: 0
	}
	console.log("el rq",JSON.stringify(request))
	let user = Func.getUser();
	fetch(conf.url.base + conf.endpoint.getCreditNotePDF, {
		method: 'post',
		body: JSON.stringify(request),
		responseType: 'arraybuffer',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + user.access_token,
		}
	})
		.then(res => res.blob( console.log(res)))
		.catch(function (error) {
			console.log('Error:', error);
		})
		.then(function (blob) {
			if (blob === undefined)
				return console.log('Log: ', { errorCode: 1, errorMessage: 'No existe respuesta' })
				console.log("estoy aqui")
				console.log(blob)
				const MySwalError = withReactContent(Swal)

			const file = new Blob(
				[blob],
				{ type: 'application/pdf' });

				const data = new Blob(
					[blob],
					{ type: 'text/plain' });

			const href = window.URL.createObjectURL(file);

			window.open(window.URL.createObjectURL(file));
			var w = window.URL.createObjectURL(file);
    		// w.document.write(data);
    		// w.print();

		})



}

//la impresion de nota de credito en hacer NC

export const NoteServiceProducts = (a, b, c, d, e, f, g) => {

	const MySwalError = withReactContent(Swal)

	
	const request = {
		numLocal: a,
		date: b,
		folio: c,
		ncCliente: d,
		ncTienda: e,
		voucher: f,
		ticketMF: g
	}
	console.log("el rq",JSON.stringify(request))
	let user = Func.getUser();
	fetch(conf.url.base + conf.endpoint.getCreditNotePDF, {
		method: 'post',
		body: JSON.stringify(request),
		responseType: 'arraybuffer',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': 'Bearer ' + user.access_token,
		}
	})
		.then(res => res.blob( console.log(res)))
		.catch(function (error) {
			console.log('Error:', error);
		})
		.then(function (blob) {
			if (blob === undefined)
				return console.log('Log: ', { errorCode: 1, errorMessage: 'No existe respuesta' })
				console.log("estoy aqui")
				console.log(blob)

			const file = new Blob(
				[blob],
				{ type: 'application/pdf' });

				const data = new Blob(
					[blob],
					{ type: 'text/plain' });

			const href = window.URL.createObjectURL(file);

			window.open(window.URL.createObjectURL(file));
			var w = window.URL.createObjectURL(file);
			localStorage.removeItem("swal")
    		// w.document.write(data);
    		// w.print();

		})


		console.log("entro en el metodo")
		localStorage.removeItem("numLocal")
		localStorage.removeItem("date")
		localStorage.removeItem("pos")
		localStorage.removeItem("transaction")
		localStorage.removeItem("rutCliente")
		localStorage.removeItem("marcaResumen")
		localStorage.removeItem("screenProducts")
		localStorage.removeItem("screenCliente")
}
