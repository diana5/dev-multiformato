import React from 'react';

export function Error(props) {
    return (
        <div className="Card card p-4">
			<h4>ERROR {props.type}</h4>
			<h6>{props.message}</h6>
		</div>
    )
}
