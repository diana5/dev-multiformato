import React from 'react';
import './Outline.css';

export default function Outline(props) {
    return <a className={"Outline modal-action modal-close transparent waves-effect btn-flat btn-large " + props.className} onClick={props.handleClick}>{props.name}</a>
}
