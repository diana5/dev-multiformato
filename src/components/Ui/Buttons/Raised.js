﻿import React from 'react';
import { Link } from 'react-router-dom';
import './Raised.css';

export default function Raised(props) {
    let button;
    !props.isSubmit
        ? button = <Link to={props.href} className="Raised waves-effect waves-light btn" role="button">
            {props.text}
            {
                props.isIcon &&
                <i className={"fas fa-" + props.icon}></i>
            }
        </Link>

        : button = <button 
                        className={!props.light ? 'Raised btn waves-effect waves-light px-5 pt-2 pb-2' : 'Raised btn waves-effect waves-light' } 
                        type="submit" name="action" 
                        disabled={props.disabled}>
            {props.text}
            {
                props.isIcon &&
                <i className={!props.light ? "fas fa-" + props.icon : "fal fa-" + props.icon }></i>
            }
        </button>
    return (
        <div>
            {button}
        </div>
    )
}
