import React from 'react';
import PDF, { Text, Table, Html, Image } from 'jspdf';
import { Fn } from '../../../Utils';



const PDF_Detalle = (props) =>
<PDF
    properties={props.props}
    preview={true}
    format='a3'
    orientation='l'
    save={true}
>

    <Text x={40} y={20}>{
        'N° Local ' + props.N.detailLocal
        // + '      -      '
        + '      -      Folio NC: ' + props.N.detailFolio
        + '      Fecha: '  + Fn.formatDate(props.N.detailDate) }
    </Text>
    
    <Table
        columns={props.col}
        rows={props.row}
    />
    <Html sourceById='page' />
</PDF>

export default PDF_Detalle;
