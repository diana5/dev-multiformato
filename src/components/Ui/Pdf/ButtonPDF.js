import React from "react";
import getPDFReport from "./getPDFReport";

const ButtonPDF = ({
	boyLocalDetail,
	bodyLocalList,
	productoListDetail,
	productoListRemoto,
	sumaLocalTotal,
	sumaRemotoTotal,
	...props
}) => (
	<div className="col d-flex justify-content-end Excel-Button">
		<a
			className="btn  Raised u-bg-pink" 
			onClick={() =>
				getPDFReport(
					boyLocalDetail,
					bodyLocalList,
					productoListDetail,
					productoListRemoto,
					sumaLocalTotal,
					sumaRemotoTotal
				)
			}
			{...props}
		>
			<i class="fal fa-file-pdf right"></i>Descargar PDF
		</a>
	</div>
);

export default ButtonPDF;
