import React from "react";
import getPDFInforme from "./getPDFInforme";

const ButtonPDFCentral = ({
	rowList,
	sumaTotal,
	informeType,
	reportType,
	isDetalle,
	...props
}) => (
	<div className="">
		<a
			className="waves-effect waves-light btn  Raised u-bg-pink mb-6"
			onClick={() =>
				getPDFInforme(
					rowList,
					sumaTotal,
					informeType,
					reportType,
					isDetalle
				)
			}
			{...props}
		>
			<i class="fal fa-file-pdf right"></i>Descargar PDF
		</a>
	</div>
);

export default ButtonPDFCentral;
