import React, {Fragment} from 'react';
import PDF, { Text, AddPage, Table, Html } from 'jspdf-react';

const PDF_NC = (props) => {

return(


<PDF
    preview={true}
    format='a3'
    orientation='l'
    save={true}
>
{
    props.Local &&
    <Text x={40} y={20}>{
        'Realizadas en el local ' + props.Local.numLocal
        + '      -      Emisor NC Multiformato'
        + '      -      Periodo:  ' + props.Local.fromDate
        + '  al  ' + props.Local.toDate }
    </Text>
}
    <Table
        columns={props.col}
        rows={props.row}
    />
    <AddPage
        format='a3'
        orientation='l'
    />
    {
    props.Local &&
    <Text x={40} y={20}>{
        'Desde otros locales      -      Receptor NC Multiformato'
        + '      -      Periodo: ' + props.Local.fromDate
        + '  al  ' + props.Local.toDate }
    </Text>
}
    <Table
        columns={props.col}
        rows={props.rowRemoto}
    />
    <Html sourceById='page' />
</PDF>

)}

export default PDF_NC;

