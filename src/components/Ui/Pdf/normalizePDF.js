import { Fn } from "../../../Utils";

const isNotExit = x => typeof x === undefined || typeof x === "undefined";

const normalizePDF = (array = []) => {
	let newArray = [];

	!isNotExit(array) &&
		array.length > 0 &&
		array.map(local => {
			Object.keys(local.products).map((value, index) =>
				newArray.push({
					formatoReceptor: local.originalTransaction.formato || " ",
					mercadoReceptor: local.originalTransaction.mercado || " ",
					localReceptor: local.originalTransaction.numLocal || " ",
					rutCliente: Fn.formateaRut(local.customerId) || " ",
					folioBoleta: local.originalTransaction.folio || " ",
					fechaBoleta:
						Fn.formatDate(local.originalTransaction.date) || " ",
					formatoEmisor: isNotExit(local.formato)
						? " "
						: local.formato,
					meracadoEmisor: isNotExit(local.mercado)
						? " "
						: local.mercado,
					localEmisor: isNotExit(local.loginLocal)
						? " "
						: local.loginLocal,
					rutColaborador: isNotExit(Fn.formateaRut(local.docNbr))
						? " "
						:  Fn.formateaRut(local.docNbr),
					idOperador: isNotExit(JSON.stringify(local.userid)) ? " " : JSON.stringify(local.userid),
					folioNc: isNotExit(local.folio) ? " " : local.folio,
					fechaNc: isNotExit(local.date)
						? " "
						: Fn.formatDate(local.date),
					motivo: isNotExit(local.reasonDescription)
						? " "
						: local.reasonDescription,
					description: isNotExit(local.products[index].description)
						? " "
						: local.products[index].description,
					dep: isNotExit(local.products[index].deptNbr)
						? " "
						: local.products[index].deptNbr,
					ean: isNotExit(local.products[index].ean)
						? " "
						: local.products[index].ean,
					cantidad:  local.products[index].grams===0
					?   local.products[index].qty : ( local.products[index].grams < 1000 &&  local.products[index].grams > 0) ? "0." +   local.products[index].grams 
					: String( local.products[index].grams).replace(/(.)(?=(\d{3})+$)/g, "$1,").replace(",","."),
					total: isNotExit(local.products[index].total)
						? " "
						: Fn.toCurrency(local.products[index].total)
				})
			);
		});

	console.table(newArray);
	return newArray;
};

export default normalizePDF;
