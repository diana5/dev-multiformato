import { Fn } from "../../../Utils";

const isNotExit = x => typeof x === undefined || typeof x === "undefined";

const normalizePDFDetail = (detail = []) => {
	let newArray = [];

	!isNotExit(detail) &&
		Object.keys(detail.products).map((value, index) =>
			newArray.push({
				formatoReceptor: detail.originalTransaction.formato || " ",
				mercadoReceptor: detail.originalTransaction.mercado || " ",
				localReceptor: detail.originalTransaction.numLocal || " ",
				rutCliente: Fn.formateaRut(detail.customerId) || " ",
				folioBoleta: detail.originalTransaction.folio || " ",
				fechaBoleta:
					Fn.formatDate(detail.originalTransaction.date) || " ",
				formatoEmisor: isNotExit(detail.formato) ? " " : detail.formato,
				meracadoEmisor: isNotExit(detail.mercado)
					? " "
					: detail.mercado,
				localEmisor: isNotExit(detail.loginLocal)
					? " "
					: detail.loginLocal,
				rutColaborador: isNotExit(Fn.formateaRut(detail.docNbr)) ? " " : Fn.formateaRut(detail.docNbr),
				idOperador: isNotExit(JSON.stringify(detail.userid)) ? " " : JSON.stringify(detail.userid),
				folioNc: isNotExit(detail.folio) ? " " : detail.folio,
				fechaNc: isNotExit(detail.date)
					? " "
					: Fn.formatDate(detail.date),
				motivo: isNotExit(detail.reasonDescription)
					? " "
					: detail.reasonDescription,
				description: isNotExit(detail.products[index].description)
					? " "
					: detail.products[index].description,
				dep: isNotExit(detail.products[index].deptNbr)
					? " "
					: detail.products[index].deptNbr,
				ean: isNotExit(detail.products[index].ean)
					? " "
					: detail.products[index].ean,
				cantidad: 	detail.products[index].grams===0
					?  detail.products[index].qty : (detail.products[index].grams < 1000 && detail.products[index].grams > 0) ? "0." +  detail.products[index].grams 
					: String(detail.products[index].grams).replace(/(.)(?=(\d{3})+$)/g, "$1,").replace(",","."),
				total: isNotExit(detail.products[index].total)
					? " "
					: Fn.toCurrency(detail.products[index].total)
			})
		);

	console.table(newArray);
	return newArray;
};

export default normalizePDFDetail;
