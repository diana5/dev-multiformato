import jsPDF from "jspdf";
import "jspdf-autotable";
import getDataToExcel from "../Excel/getDataToExcel";
import normalizePDF from "./normalizePDF";
import normalizePDFDetail from "./normalizePDFDetail";
import { Func } from "../../../func/func";
import { Fn } from "../../../Utils";

const logo =
	"data:image/jpeg;base64,/9j/4AAQSkZJRgABAgAAZABkAAD/7AARRHVja3kAAQAEAAAAPAAA/+4ADkFkb2JlAGTAAAAAAf/bAIQABgQEBAUEBgUFBgkGBQYJCwgGBggLDAoKCwoKDBAMDAwMDAwQDA4PEA8ODBMTFBQTExwbGxscHx8fHx8fHx8fHwEHBwcNDA0YEBAYGhURFRofHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8f/8AAEQgBfALaAwERAAIRAQMRAf/EAL0AAQACAgMBAAAAAAAAAAAAAAAGBwUIAQIEAwEBAAMAAwEAAAAAAAAAAAAAAAQFBgECAwcQAAEDAgIEBg0HCAkDBQAAAAABAgMEBREGITESB0FRYXEyE4GRoSJSYnKisxR0NgixwUKCkrIV0iMzQ2OTNTfRU3ODJFRVFhjCoxfw4USUJREBAAIBAwAFCQcEAgMBAQAAAAECAxEEBSExURIzQWFxgbHBIjJykaHRQlITBvAjFBViNOHxgpIk/9oADAMBAAIRAxEAPwDakAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA8F5u8Vspeuem3I5dmKPViv9CFfyPIV2uPvT0zPVCRtttOW2kIdU5nvM71VJ+qbwMjREROyuKmKzc7urz83djzL2nHYqx1au9Jmq7wPRZJEqGcLJETHsOTSem35/c0n4p70ef8XXJxuK0dHRKaW24Q19IyphxRrtCtXW1ya0U2+z3ddxji9eqVDmwzjtNZeklPIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAItnln5qkfxOenbRF+Yyf8pr8NJ88rjiJ6bImY5dgE3yWzC0ud4crl7SInzG9/jVdNtr22lneUn+76meNArgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAj2dmY22F/gyp3WqZv+TV1wRPZZZ8VP8AcmPMhZhmgAJ9lRmzY4F8JXu85T6JwNNNrXz6+1meQtrmlly5QgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAwubmbVlkXwHsd3cPnKP8AkNddrPmmE/jZ0zR60EPnzSAFjWBmxZqNv7NF+1pPpvE102uOP+MMrvJ1y29L3lgjAAAAAAAAAAAAAVrcN+2XKGvqaGe31qT0sr4ZU2Y8NqNytX6fIW+Phct6xaJrpP8AXYqr8vjraYmLaw8//IPKv+Qrfsx/lnf/AEWbtr/Xqdf9zi7LPbZd9+Wrtd6O2RUlVFLWSthjkkSPYRztCY4OVdZ55uHy46TaZjSHpi5XHe0ViJ6VilSswABhc3Zrt+V7O661zHyQpIyJscSIr3OeuCYYqicpI2u2tmv3a9bw3O4rhr3rIP8A8g8q/wCQrfsx/lll/os3bX+vUr/9zi7LH/IPKv8AkK37Mf5Y/wBFm7a/16j/AHOLsslWSN4VozelX6hDNA+jVnWMnRqKqSY4KmyrvBUhbzY3wad7Tp7Eza72mfXu+RKCElgACvcwb6st2S81dpnpaqaajf1ckkSMViuwRVwxci6McC1wcRlyUi8TGkqzNymPHeazE6wx/wDyDyr/AJCt+zH+Wev+izdtf69Tz/3OLssyGX99Fhvl5pLTSUFWlRVv2GOekey3BFcrnYOVcERDyz8RkxUm9prpD0w8pTJeKxE6ysIqlmAAAAAAAAfGtqmUlHPVPRXMp43yvRutUY1XKido7Vr3piO11tbSJnsVmnxCZVVEX1Ct0+LH+WXH+izdtf69Sq/3OLssf8g8q/5Ct+zH+WP9Fm7a/wBeo/3OLssmOSs7W7NtBPW0MMsMdPL1L2zI1FV2yjsU2Vdo74gbvZ2wWittOnsTtruq5qzNfIkJESQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAHnuNbHQ2+prZUV0dLE+Z7W61bG1XKiY8Og746Ta0Vjyy63v3azM+RWqfEJlVUx9Qrfsx/llv/os3bX+vUqv9zi7LH/IPKv+Qrfsx/lj/RZu2v8AXqP9zi7LJrk3N1Bmq0uudFFLDC2Z8CsmRqO2mIiqveq5MO+K7d7W2C/dtprpqn7bc1zV71WdIyQAAAAABjblmG229/VyvV83DFGmKpz8CFXveXwbedLTrbshKwbPJk6YjoLZf7dcHbETlZKn6qTQ5ebjOdly2HcdFZ0t2SZ9nkxdfUyRZooAAAAMbmRm3ZKtOJm19lUUrOZrrtb+hK2U6Zq+lXh81alwuoCzrezYoKZngxMTzUPqu0r3cVY/4x7GQyzreZ870Eh5gAAAAxNwzNbKGXqnOdLKnSbGiLs86qqIVG85vBgt3ZnvW8yZg2OTJGsdEed6LberfcEVKd/5xNLonJg5E48P6CRsuSw7n5J6eyet559rfF80PcT0cAAAAGs++O0/h+fq5zW4R1zY6tnKr27L/PYpsuJy9/BH/HoZPk8fdzT5+lCSyV772+sfRXClrWLg+lmjmav9m9HfMdclO9WY7Ydsdu7aJ7JbiQzMmhZNGuLJGo9i8aOTFD59MaTo3MTrGrucOQCnPiIumFPaLU12l75KqVvIxNhn33Gg4HH02v6lFzWTorX1qUNIoACydwlz9WzlNRqveV9K9qJ48So9PN2in5vHrhif0yteHyaZZjthsKZNpwDh72sY57lwa1FVy8SJpERq4mdGn16uDrjeK64OXFauolmx5HvVU7h9Aw4+5SteyGIy3715ntl4z0eaztwNo9ZzTVXJzcWW+nVrHcUk67KeY1xS85l0xRX9U+xb8Pj1yTbsj2tgDKtKAAAAAAAAeDMH8BuXss/o3Hrg+evph55vkn0S0+Z0G8yH0CWGh2OHK+vh692bl7cvoWGX57xK/T72j4Xw7fV7lqFGuQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGLzV7r3j2Ko9E499t4tfqj2vHceHb6Z9jURvRTmN8xLk4Gwu4L3Hl9um+5GZPnPHj6Y97T8P4P8A9Sskp1qAAAADHX+5Lb7c+Zn6ZyoyLyncPYTSVvK73/HwTaPm6o9KVtMH7uSI8ivHOe9yveque5cXOXSqqvCfNrWm06z0zLURERGkDXOa5HNVWuRcUcmhUXkFbTE6x1kxr0Sk9lzc5uzBcVxbqbUprTy0+c1fGfyHTSmf/wDX4qfdcb+bH9n4JXHIyRiPjcjmOTFrkXFFQ11LxaNYnWFNMTE6S7HZwAAPLdmbdsq2ccT/ALqkTf072C8f8Z9j229tMlZ86s01Hy1rXZrdpyN8JUTtnatdZ0cTOkarTY3ZY1vEiJ2j6zWNIiGOmelydnAAA+dRUQU8TpZ3pHG3pOdoQ88uamOvetOkO1KTadI6ZQ+9ZsnqdqChxhg1Ol1PdzeChi+T/kFsmtMXw17fLP4Lza8bFem/TPYjxm1q7wTzU8zJoXKyWNcWuQ9MOa2O8XrOlodL0i8TE9SybbWsraGGpamHWN75OJyaFTtn07ZbmM+KuSPLDKZ8U47zWfI9JKeQAAAUz8RFqxZaLs1OislJK7yk6xn3XGh4HL02p61DzWP5bepS5o1C4VMUwORtZu5uS3HI9mqlXaf6syJ6+NF+bXH7Jhd/j7ma0edstlk72Gs+ZIyIlAGtm+q6+vZ9qYmu2o6CKOmbyKidY/znmw4fF3cET+qdWU5XJ3s0x2dCCForgDO5EuaWzOVnrVdssZVMZIviSr1bu44i77H38No83sSdnk7mWs+dtiYVswCP7wLn+GZLvNYi4PZSvZGvjyp1be64lbHH381Y86NvMncxWnzNUETBETiN2xjkDYXcNZ/VMnyV7m4SXKofIi8ccX5tvdRxkuay97N3f0w0/EYu7i1/VKySnWoAAAAAAAB4MwfwG5eyz+jceuD56+mHnm+SfRLT5nQbzIfQJYaHY4cr6+Hr3ZuXty+hYZfnvEr9PvaPhfDt9XuWoUa5AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAYvNXuvePYqj0Tj323i1+qPa8dx4dvpn2NRG9FOY3zEuTgbC7gvceX26b7kZk+c8ePpj3tPw/g/wD1KySnWoAAAAI5neOR1DA9E7xknf8AJiiohmf5PS04azHVFun7FrxNoi8x2whpiF8AAMjab5W21+Ea7cCr38Dl0LzcSlnx/K5dtPR007Pw7ETc7OmWOnot2pta7xRXGLagdg9E7+J3Sb/64zebHkcW5rrSenyx5YZ/Ptr4p0s9xORwDpMzrIZGeG1W9tMDpkr3qzHbDms6TEqucxzHKxyYOauDkXWiofJ7Vms6T1w2MTrGsPvboXTV9PE1MVdI3RyY4qSNljm+alY/VDy3Fu7jtM9izT6myQAAxl3v9HbW7Ll6yoVO9hbr53cSFXyHLYttGk9N+xL22zvlno6I7UJuV1rLjLt1D+9ToRJoa3mT5zCb3kMu5treejyR5IaDBtqYo0r9rxkFIAAE8ylHIyyxq9MNtz3M8lVPoX8fpNdrGvlmZZrkbROadGZLtBAAACF74LT+I5BuOy3GSj2KuPD9k7F3mK4sOKy9zPXz9H2oHJY+9hnzdLWU2rIhw5bBbgrj6xk+ejVcXUNW9qJxNlRJE7quMnzePTNE9sNNw99cUx2Sswp1s6ySMjjdI9cGMRXOXiRExU5iNXEzo0/vVxfc7xXXF64uq6iSbsPeqp3Df4cfcpFeyGIzX795t2y+NFSy1dZBSRJjLUSMij8p7kanyna94rWbT1Q60rNpiI8r5yxuilfE7pRuVjudq4KdonWNXWY0l1RzmqjmLg5q4tVOBU0oNBt/YLi25WO33Bq4pV08Uy4cb2Iq90wGbH3LzXsluMN+/SLdsPeeT0Vpv8uSU+T4KJFwfXVTGqnGyJFkd3UaXHCY9c2v6YVPMZNMWnbLXw1jMiNc5UaxMXuXBqcaroQa6GjbvLFpbaMu222tTD1Wnjjd5aNTaXsuxMDuMvfyWt2y223x9ykV7Iei5Xe12unWouNXFSQJ+sme1idjFdJ0x47XnSsay73yVpGtp0Qa679Mk0bnMpVnuL04YI9li/WkVncLPFw2e3TOlfSrsnLYa9XSjdV8RTsf8JY+94FmqMF7TWKTI4CfLf7kWeb7K/e8yfETdcdNkgVOJJ3ov3DtPAx+v7nX/dT+llrd8Q9okejbjaZ6dq65IXtmRPqr1anhk4K8R8Non7ntTmqT81ZhYOXc5ZazFGr7TXMne1MXwL3krfKjdg7s6iqz7XJin440WeDdY8sfDLNEd7gHgzB/Abl7LP6Nx64Pnr6Yeeb5J9EtPmdBvMh9Alhodjhyvr4evdm5e3L6Fhl+e8Sv0+9o+F8O31e5ahRrlAbrvqyfbLnV26pZVrUUcr4JVZEit2mLguC7SaC0xcRmvWLRppMa9atycpipaazrrDy/+fMj/wBXW/uU/LO/+kz/APH7XT/cYfP9iR5P3gWLNjqptrbO1aRGLL1zEZ+kxwwwV3gkTdbHJg07+nSlbbeUza93yPZmXOGXst06TXarbCrkxigTvpX+RGmlefUee32uTNOlI1emfc0xRraVa3L4iKdr1bbLO+ViL3slTKkeP1WI/wCUuMfA2mPitoqr81H5avLT/EVWJInrNjjWPh6qdUd5zMDvbgOy/wBzpHNz5apzlPezlPMUrKVkrqG4PwRtLVYN214o3oqtcvJr5Cs3XGZcMazGte2Fht+Rx5ejqt500K9PcOc1jVc5Ua1qYucq4IiJwqBAMx77Mn2mR8FK591qWLgrabDqkVOBZXd79nEtdvxGbJ0z8Mef8FZn5XFToj4p8yITfEVXq9eoskSR8HWTuV3cYTo4GPLf7kKeanyVZC1fEPQSSNZdbTJTtXpS08iSon1XIxTyycDeI+G0T9z1x81Wfmros2w5ksl/okrLTVsqodT9nQ5i+C9i4OavOhT5sF8U6XjSVthz0yRrWdWSPF6gACO37eFk+xq5lfc4knbrp4l62XHiVjNpU7JLw7HNl+Ws6IubeYsfzT0oVcPiFsETlSgtlTVYanyKyFq/fd3Cxx8Fkn5rRH3oF+apHyxMsNJ8RVxVy9VZIUbwbU7lXuMPeOAjy3+54Tzc+SrvT/EVVI7/ABNjYrOHqqhUXzmHE8B2X+5zHN9tfvSiyb88m3B7Yq3rrXK7RtTtR0WP9ozaw7KIQ83DZqdMfFHmS8XLYrdE/CsGmqaaqgZUU0rJoJE2o5Y3I5rk40cmhSqtWYnSVnW0TGsPocOQCNZw3gWLKbqVt0bO5axHrF1LEf8Ao8MccVb4RM2uxyZ9e5p0Im53lMOne8qOf+fMj/1db+5b+WS/9Jn/AOP2ov8AuMPn+xk8t728rZhvENpoGVKVU6PViyxo1neNVy4rtLwIeO44zLip37aael7YORx5bd2uuqY1FTT00ElRUSNhgiRXSSvVGta1Naqq6EK+tZmdI6060xEayrLMG/zL1FM+C00sl0c3FOvxSGHHxVVFc5Pqlzg4TJaNbT3VTm5jHWdKx3mOy/v4uFzvtBbZbPFHHW1EdP1jJnK5vWORu1grNOGJ65+EilJt3uqNep5YeXm94r3eudFnZq917x7FU+icU+28Wv1R7VtuPDt9M+xqI3opzG+YlycCw8gb2m5SsbrWtsWsV075+tSVI+mjU2cNl3glRvuLnPfvd7To06lps+R/Zp3e7r0pL/yLZ/oTv/sJ+QQ/9BP6/uS/93H6fvTLd1vIbnJ9e1KBaL1JIlxWRJNrrdrxW4YbBX7/AI+dvp066p2y337+vRpomhXJ4AA6TQxTxOilYj43pg5q6UVDpkx1vWa2jWJdq2ms6x1o1cclxOxfQSbC/wBTJpb2Ha0MxvP41WenDOnmla4OUmOi8a+dGqy31tE/YqYnRrwOXorzOTQZbc7LLgnS9dPYtsWemSNay85FewB3hmlglbLC9Y5G9F7VwVD0xZbY7Ras6Wh1vSLRpMawl1lzbHPswV+EUy6GzamO5/BU2fGfyCuTSmb4bdvkn8FFuuOmvxU6YSQ0yrAPBW2K11kiyTwIsi63tVWqvPhrK/c8Xt8063r09vUkYt3kxxpWeh3obPbqFVdTQox66FeuLnYc6nfa8fhwdNK6T2+Vxl3N8nzS9hNeDhzmtarnKjWtTFVXQiIhxa0RGskRqit6zf0oLcvI6pX/AKE+cyXJfyHrpg//AF+C52vG/myfZ+KLPe57le9yue5cXOVcVVeVTJWtNp1mdZXMRERpDg6uQD10FpuFc7CmiVzeGRdDE7Kk7acdmzz8Fejt8iPm3WPH80pPbsm0kWD61/rD/wCrTvWf0qarZ/xzHTpyz357PIqM/J3t0V+GPvSJrWtajWojWtTBETQiIhpIiIjSOpVzOrk5AAAA+FdSR1lFUUkumOojfE9PFe1Wr8p2paazEx5HW9e9Ex2tPaullpKueklTCWmkfC9F8KNytX5D6BS0WrEx5WGtXuzMdj5HZwtr4ebksd4u1uVe9qIGTtTlidsr3JCh57HrWtuydPt/9Lrhb6WtXthehmWiRreTdVteR7vVNdsyLA6GJfHm/Nph9ombDF381Y86Jvsncw2nzNVUTBMDcsclW663rXZ+s0SJi2Kb1h/NC1X/ACohA5LJ3cFvPGn2pnH072av2sZm+h9RzXeKTUkNZMjfJV6ub3FPbaX72Ks/8YeW6r3cto88sSSHg2U3K3P13INHGq4voZJaZ3Hg1203zXoY7l8fdzz5+lq+Lyd7DHm6E6KxYqJ+IW59bfLXbWri2mp3zvTxpn7KdyM0/BY9KWt2z7Gd5rJreteyFTl6pUj3c2j8WztaaRzdqJJknmTg2IE6xe3s4EPkMv7eG0+bT7UrY4u/mrDamRrnRuax2w5yKjXppVFVNC6TEQ2MtRMxVF5mvNU281MtVX08r4ZZJnK5UcxytXDHQiaNSG929aRSO5GkTDE57Xm896dZiWOPZ5AAAB9aSrqqOpjqqSZ9PUxLtRTRuVr2qnEqHF6RaNJjWHatprOsTpLYfdRvKXM9M+3XJWsvdK3aVyd6k8errGpwOT6SdkyPJ8f+xPer8k/c0/Hb792O7b54+9YRVLN4MwfwG5eyz+jceuD56+mHnm+SfRLT5nQbzIfQJYaHY4cr6+Hr3ZuXty+hYZfnvEr9PvaPhfDt9XuWoUa5anZ899797fP99Td7LwKfTDF7zxr/AFSwRJR083bZ1psp2jMFYqNkrpkgjoKZV6cnf987xWa3doquR2c570j8vTrKy2O6jDS8+Xo0Q263W43avlr7jO6pq5lxklf3EROBqcCIWWLFXHXu1jSEDJkte3etOsvKd3QAcOPCmlFAvjc5vIkuVJNZr3UItXQxLNBVyrgslOzpbbl+lHx8Kcxl+V4/uTF6R8NvJ5//AC0fGb7vx3Lz0x5fMg+8zelW5kqZLdbZHQWGNdlEbi11SqfTk4djwW9ssuO42MUd63Tf2K/f8hOWe7X5Par8t1YHAHIyeW8yXbLt0juVslWOZiokka47ErOFkicKL3CPuNtTNXu2/wDT2wZ7Yrd6raXK2Y6HMdjprtR6I52/nI1XF0cjdD2O5WqYnc7e2K80nyNht88ZaRaGWPB7K137T3umyxTVFvq5aam6/qq5kTtnbZI3vNpyd9gjm4a+EuOFrS2WYtGs6dCq5a14xxNZ0jXpa9fPrNazDk4AAAORJ8jbwLzlOua+ncs9te5PWre5e8cnC5ngP5U7JB3uwpnjp6LeSUzab2+Gej5exs3aLtQ3e209yoJElpKpiSRPTiXWipwKi6FQxeXHalprbrhrceSL1i0dUvYdHdSvxF/prF5NT8sZo+A/P6veoOb66+v3KbNCok23M/zEty8CMqPQuKzmP+vPphYcX48et6N628WpzHc5bbRSq2xUj1YxrVwSoe1cFlfxtx6Cdk68ZsIxV71o+OfuduR3s5bd2Pkj70BLVWs1kj3zsXt9N6VpH3ng3+mfYkbTxafVDaDNXuvePYqj0TjFbbxa/VHta7ceHb6Z9jURvRTmN8xLk4AABcnw6fpr95NN8spn+f6qev3L3hOu3q966jNr8AAAAHWSKOViskaj2Lra5EVF7CnW9K2jS0aw5i0xOsI/ccm0c2L6N3q8ngL3zF+dDO7z+OYr9OOe5P3LLByd69Fvij70Yr7PcKBf8REqM4JW98xeynzmV3fG5sE/HXo7fIt8O6x5Oqel4yAkuMMQLPt7JGUNO2X9I2NiPx14o1MT6rtK2jFSLdfdj2MhmmJvMx1avuSHmAAAGNzGyV9lq0jx2thFVE8FFRXdwrOYra21vFevT39P3JWymIy11V4fNWpAMhbrDcq7BYotiJf10net7HCvYLPZ8Rnz9MRpXtlEz73Hj651nsSi3ZRt9Ng+o/xMqeFoYi8jf6TV7P8Aj+HF03+O3n6vs/FT5+SyX6I+GGca1rWo1qI1qaERNCIX0RERpCvmXJyAAAAAAANYd7dq/Dc/XJrUwjq1bVx/3re+89HG04rL38FfN0MjyWPu5p8/Sh5YIKY7obj6jvAtiquDKnrKZ394xdnzkQruVx97b283Sn8Zfu56+fobOmMa1VPxB3Tqcv262NXvqypWV6eJA38p6F5wWPXJNuyPapuZyaUivbPsUOahnFp/D7a+vzJX3FzcWUdMkbV8ed35LFKTncmmOte2fYuOGx65Jt2Qwu+m3ep5/rHomDKyKGobyqrdh3nMJHD5O9giOyZeHK07uafOgxZq5c/w73Pvbza3LqWKqjbz4xv+Rpneex/Lb1L7hcnzV9a5zOr5q9vWuX4hn+7SIuLIHtpmcWELUavnYm14vH3cFfP0/ayHI5O9mt5uhEiehLa+Hu0dbd7ndnJi2lhbTRL48y7TsPqsTtlDzuXStadvSuuFxa2tbs6F6GZaJQm8bdvmG47wqlLNROlgr2MqnTr3sLHOTYftvXQnfMxw1mo4/kMdNvHfnpr0ednN9sb3zz3I6J6WYsnw9UyNbJfLm97/AKUFG1GtTk6x6OVfsoR83OzPyV09L2xcLH57fYlVNuW3ewtRHUD51TW6WeVVXtOancIVuX3E/m+6E2OLwR5Pvee47jciVUSpTwz0Mn0ZIZnO08rZNtDtTmc8T0zE+p0vxOGY6ImFMZ7yHc8o3FkFQ5KijqMXUdY1MEejdbXJ9F7cdKGi2W+ruK6x0WjrhQ7zZ2w20npieqUZJqIyGXr3U2O90V2plVJKSVHuRPpM1PYvI5qqh5bjDGXHNJ8r1wZZx3i0eRt1TVEVTTRVELtqKZjZI3cbXJii9pTA2rMTpPkbattY1h5MwfwG5eyz+jcemD56+mHTN8k+iWnzOg3mQ+gSw0Oxw5X18PXuzcvbl9Cwy/PeJX6fe0fC+Hb6vctQo1y1Oz577372+f76m72XgU+mGL3njX+qWCJKOASzJ27PM2aWesUjGU1vRdn12oxRjlTWkbUTafhyaOUgbvkseCdJ6bdkJu12GTN0x0V7Uqr/AIe79FTq+iudPUzImPUvY6JFXiR2L+6Qac7SZ6azCZfhrxHRMSrK5WyvtldNQV8DqergXZlifrReBeJUXgVC6xZa5Kxas6xKpyY7Ut3bRpMPMd3Ryx72KqscrVVFaqoqpiipgqaOBRMRJE6PpR0dXW1UVJSQvnqZnIyGGNNpznLwIiHW94rEzM6RDmtZtOkdMrMtPw/5jqadstwroKB7kx6hqOme3ylRWt7SqU2TnccTpWJlb4+GyTGtpiGGzfuizPlylfXYx3C3x6ZZ6dFR0aeE+NdOzyoqkja8rjyz3flt50fc8bkxR3vmhBy0V4cC2vh+zA+G6V9ikf8AmaqP1qnavBLHg1+HlMVF7BQ87g1rGSPJ0Lvhs2lpp29K9DMtCwWerJ+NZSuluRNqSWBzoE/ax9/H5zUJOzzft5a27JR93i/cxWr5lGZW3MZtvUbKira200b8FR1SirM5F8GFMF+0qGm3PMYsfRX4p83V9rO7fi8mTpn4YWLa9weT6ZqLXTVVfInSxekTF+rGiO84qMnN5p+XSv8AXnWmPh8Udesssu5vd2rNn8Lw8ZJpse3tnj/ttx+r7oe3+swfp+9Fc1bg7e6lkqMt1EkVUxFc2iqHbcb8PoteqbTV4scSbtuctE6ZI1jtRNxw9dNcfX2KSlilhlfDMxY5YnKySNyYOa5q4KipxoppImJjWOpn5iYnSXU5cLl+H3McnWV+Xpn4x4euUbVXUuKNlanba7tmd53b9WSPRPuX3DZ+uk+mF0mdXylfiL/TWLyan5YzR8B+f1e9Qc3119fuU2aFRPZarrV2uqdVUi7M7oZYEfwtSZixuVOXZdoPPNhrkjS3Vrr9j0x5ZpOsdbxImCYcB6vN7rdZL1c1wt1BUVnLBE+RO21MDyyZ6U+a0R6ZelMN7/LEykmVcn5sos22Wers9ZBDHXU7nyvhejGokjVVVdhhgQ91u8VsV4i1Znuz5UvbbXLXLWZrOnejyNic1e6949iqfROMltfFr9Ue1p9x4dvpn2NRG9FOY3zEuTgZS2ZVzLdaZam22yprKdHKxZYY1e3aTBVbinDpPHJucdJ0taIl7Y9vkvGtazMPX/4+zz/oNb+5cef+dg/XX7Xf/Czfpt9i1dxOXr7aJryt0oJ6JJm0/VLOxWbWysm1hjxYoUfNZ6ZIr3ZidNfcueIw3pNu9Ex1e9bRQroAAAAAABw5rXIrXIiouhUXShxMRMaSMJccpW6pxfB/hpV04s6Cryt/oKLefx/Bl6a/BbzdX2LDByOSnRPxQ+FqyhHTVDZ6qVJ3MXGONEwbinCuOs8Nh/Hq4rxfJPemOqPI9NzyU3r3axokZpVWAAAAAqIqKipii6FRTiY1EXrclMknV9JOkUTlxWNyKuz5KoZbdfxmLX1x27sT5PwW+HlZiulo1lkbdle2UeD3M9YmT9ZJpRF5G6iy2fCYMPTp3rds/gi59/kydGukeZly4QgAAAAAAAAAApT4iLVhUWi7NTptkpJV8lesZ8rjR8Dl6LU9ag5rH01t6lOGhUT12eudb7vQ17VwWlqIpsfIejl7iHnmp3qTXtiXfFfu3ieyW4THtexr2ri1yIqLyKfP5jRuInVr3v7unrOcYaJq97QUrWqnjzKr183ZNXwePu4pt+qWa5jJrliOyFbFyqWwW4O1eq5Rnr3NwfcKlzmrxxwp1bfO2jKc3l72aK/phpuHx6Ypn9Uo/wDERbdmps1zRND2y0r15WqkjPlcS+BydFq+tG5rH01t6lOmgUSe7kbl6nn2CFXbLK6CWnXlVE6xvdYVXM4+9gmf0zr7llxN+7miO2GxtRMyCCSd64MiY57l5GpipkaxrOjU2nSNWnddVvrK6prHri+plfM5V45HK75z6Djr3axHZDDXt3rTPa+J2dWyG5K0eoZFp53NwluMklU5eHZVdhnmsxMfzGXv55j9PQ1XFY+7hif1dKfFWsgAB8amso6Vm3VTxwM8KV7WJ23KhzWsz0RDra0R1ywtVvByRSqqTXujRU1o2Vr/ALm0SY2WafyW+x4TvMUfmj7Veb385ZLv2VEprdcoquvhqI5YY2I7aw0tfpVqfRcWvE7bNjy62rMV0VnJ7nFkxaVnWdVKGmZ8A2l3XVj6zIFlleuL2wdUq/2TljTuNMPyNIrntEdrYbC3ew1nzM1mD+A3L2Wf0biPg+evphIzfJPolp8zoN5kPoEsNDscOV9fD17s3L25fQsMvz3iV+n3tHwvh2+r3LUKNctTs+e+9+9vn++pu9l4FPphi9541/qlgiSjs5kjLi5jzRQ2lVVsMrlfUuTQqQxptPw5VRME5yLvdx+zim3l8npSNpg/dyRXyNraSkpqSmipaaNsNPC1GRRMTBrWtTBERDD2tNp1nrbOtYrGkdT6nVyqP4gcvQSWuiv8TESop5Epqhya3RSYqzHyXp3S+4PPMXnH5J6VJzOGJrF/LHQo00zPAF5bgsr08dsqMxzMR1VUSOp6Ryp0ImYI9W8r3aOwZnnNzM2jHHVHTLQ8Pt4is5J656luFCu3V8bJGOY9qOY9Fa5rkxRUXQqKiiJ0cTGrVHPlgZYc23K2RJs08Um3TJxRSoj2J2Edh2DdbHP+7irbysbvMP7eWasCSkZKt1tY6k3gWV6Lgkkywu5UlY5mHbUg8nXXb29CZx9tM9W0hiWwAAADHVuY8v0OPrlypadU1pLNG1e0qnrTBe/y1mfU8r56V67RHrYiXefkCJ2D75TY+Kqu+6intGwz/ot9jynfYY/NDX7ePW2euznca20TNnoapzJWysRUar3MTrNaIvSxNZx1b1w1i/RMMxvrVtlma9Uo0TURL90la6k3g2lyLgkznwO5UkjciedgV/K0723t5k7jbaZ6+ds+YtrlK/EX+msXk1Pyxmj4D8/q96g5vrr6/cps0KiALP3Q7sqa/It8vLNu1xPVlNSroSd7ek53iNXRhwqUnK8jOL+3T5vLPYt+N2EZPjv8vtX3T09PTQsgp4mQwsTBkUbUa1qcSImhDLzMzOstJFYiNIfQ4csXmr3XvHsVR6Jx77bxa/VHteO48O30z7GojeinMb5iXJwNhdwXuPL7dN9yMyfOePH0x72n4fwf/qVklOtQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAINvotXr+QqyRExkoHR1bOPBi7L/ADHKWXE5e5njz9Cv5TH3sM+bpa1GyZNwqYoqcZy4bZ5HuP4jk+z1mOLpaSJHr4zWo13nNUwe8x9zLaPO2u0v3sVZ8zWjPF0/FM4XiuRdpklVI2NfEjXq2eaw2WyxdzDWvmZPd5O/ltPnYNccNCYrwISkdttk20pacq2q3YYOgpo0kTx3JtP85VMFusv7mW1u2W122PuY617IRfflbPXMiS1DUxfQTxVGPiqvVu7jybw2Tu54j9UaIfLY+9hmexrka9lmUyrcfw3M1qr8cEp6qJ7vJ20R3mqp4brH38Vq+aXttr93JWfO2V3k3JbdkW81LVwetM6Ji+NNhEn3zHcfj7+esef2dLV77J3cNp83taqomCYG5Y59IIJKieKniTGWZ7Y4043PXZTuqdbW0iZ7HNa6zp2twLTb4rda6SgiTCOkhjhbhxMajfmPn+S/ftNu2W4x07tYr2Q9Z0d0XzpvEy/lSFErHrPXvTGGghwWRyeE7HQxvKvYxJu02GTPPw9Xaibre0wx09fYpTMW+fOd2c5lLMlqpF0JFS/pMPGld32Pk4GjwcRhp1/FPn/Bn8/KZb9Xwx5kIqqqqq5FlqppKiRdb5Xue7tuVSyrStY0iNIQLXm06zL5YId3VyAOAA2X3MKq7u7byOnT/vPMZy3/AGLer2NZxfgVSjMH8BuXss/o3EPB89fTCZm+SfRLT5nQbzIfQJYaHY4cr6+Hr3ZuXty+hYZfnvEr9PvaPhfDt9XuWoUa5anZ899797fP99Td7LwKfTDF7zxr/VLBElHWRuEY1c7SqqYq2ilVq8WL2IU/OT/Zj6vxWvD+NP0thjJtOAQnfNG1+7u54/RWByc6TMLHiZ//AKK/15EDk4/sWazmzZIORs1ubaibu7VgmvrlXnWd5i+W/wCxb1exreM8Cvr9qaFcngGu+/mFrM8teiaZaOJXc6Oe35jW8JP9j1svy8f3vUrkt1WzuQ/faw+3wffQi7/wL/Sk7Pxq+ltiYVs3WSSOKN0kj0ZGxFc97lRGoiaVVVXUhzEauJnRUucd/FHSSPo8tQtrZW4tdXzYpCi/s2pg5/PoTnL3acLa3xZJ0js8qm3XLxXox9PnVVes85uvTnLcLpO+N36iN3VRJjwbEeynbLzDscOP5ax71Nl3mXJ81mBwTHFda614SUjOTkABwJBu8VUz1YfbYvlIfI+Bf0JWx8avpbXGHbJSvxF/prF5NT8sZo+A/P6veoOb66+v3KbNConDlwaq8SHI23ydborblW00UaYNipYsfKc1HPXsuVVMFu8nfy2nzy2u2p3cdY8zMEd7gGLzV7r3j2Ko9E499t4tfqj2vHceHb6Z9jURvRTmN8xLk4Gwu4L3Hl9um+5GZPnPHj6Y97T8P4P/ANSskp1qAAAAAAAAAAHDnNa1XOVEamlVXQiHEzERrI6w1EEyKsMjZETWrHI5O4dMeWl41rMW9E6u1qTXrjR3PR1AAAAAA+ctTTRKjZZWRq7oo5yNVebE8smelJ0taI17Zdq0tPVGr6YoerqAAAAAAAAAAADy3WhjuFsq6GRMY6qGSF3NI1W/Od8d5raLR5JdMlO9WY7YafTQSU80lPKmEkLnRvTxmLsr3UPoNbaxEsPaNJ0dA4X7uvzAlNujq6pzu+tKVbU5NlFlZ98yvJYNd3Efq0/BpNhm02sz+nVQWLl0u0uXSq8qmq0ZtmcmWr8WzXabeqbTJqmPrU/ZsXbf5rVI+8y/t4rW8z32uPv5a187bUwbasXmi2JdMuXO3qmK1NNLG1PGVi7PnYHtt8ncyVt2S8dxj7+O1e2GouCpoXQqa05TfsS4XHBcNC8CgXhvbzA2p3ZWNWu7+7LTyOw4mRdY/wA7AzPF4NNzbX8uvtaHks2u3r/y0UgaVnkv3TWj8Tz7bGObjFSudVy8WEKYtx+urSv5TL3MFvP0J3G4+/mjzdLZ8xbXIvvFznHlTL0la1Gvrpl6mhhdqWRUx2l8ViaV7RN2G0nPk7v5fKh73dRhpr5fI1frq6sr6yasrZnT1U7lfNM9cXOcptaUrSIrWNIhkb3m06z0zL4nZ1Z7LGRszZlev4VSK+Bi7MlVIvVwtXi211ryNxIu53uLD809PZ5Unb7PJl+WOjtWHb/h2q3NRbjemRu8CnhV/nPc35Covz0a/DX7ZWdOEn81vsfHOG5W02DLFfd47lUTzUcaPbG5sbWOVXo3TgmPCd9ry98uWKTEREuNzxdceObazOipC/UgcDZbcv8Ay7t3lT+meYzlv+xb1exrOL8CqU5g/gNy9ln9G4h4Pnr6YTM3yT6JafM6DeZD6BLDQ7HDlfXw9e7Ny9uX0LDL894lfp97R8L4dvq9y1CjXLU7Pnvvfvb5/vqbvZeBT6YYveeNf6pYIko6ydwXvrP7FJ6RhT854MfV+K14fxZ+lsKZNpwCF74/5d3X+59MwseK/wCxX+vIgcn4Fmsps2SDkbN7nf5d2rmm9M8xfLf9i3q9jW8Z4FfX7UzK5PANet/vvrB7FH6R5rOE8GfSzHMeLHoVsXCqZzInvtYfb4PvoRd94F/pSNn4tfS2yMK2ihd9O8GevuMuWrdIrLfSO2a97F/TTJrYqp9BnFwrzGo4jYRWv7tvmnq8zOcpvZtb9uvVHWqsvVM5Yx73tYxqve9UaxjUVVVV0IiImtTiZiOmSI1WBYNyGcbnG2ar6q1QuTFEqFV0qov7Nmr6yoVOfmcNOivxT9yzw8Tlv0z8KWU3w7UKInrN7me7hSKFjUx53OcQbc/byVj7U2vCR5bfcrveNlCkypf2WylqJKmN1OyZZJUajsXOcmHeoiYd6W/H7qc+PvTGnSq99tow37sTr0IsTUNIN3vv1YfbYvlIfI+Bf0JWx8avpbXGHbJSvxF/prF5NT8sZo+A/P6veoOb66+v3KbNConV/QdzKBuNav4XR/2Ef3EPn2T5p9Lc4/lj0PUdHcAxeavde8exVHonHvtvFr9Ue147jw7fTPsaiN6KcxvmJcnA2F3Be48vt033IzJ8548fTHvafh/B/wDqVklOtQAAAAAAADrLLFExZJXoxia3OVETunS+StI1tOkOa1mZ0hH7jnKkixZRs9Yf4a96xPnUz28/keKnRjjvz9yywcZe3Tb4Y+9GK+73Cvd/iJVVnBG3QxOwnzmV3fJZs8/Hbo7PIuMO1x4/lh86CunoallRA7BWr3zeBzeFFPLabu+3yRes/wDl2z4a5K92VlQStmhjmZ0ZGo9vM5MT6fiyResWjqmNWUtXuzMT5Hc9HUAAAPNc6xKKgmqlTFY24tTjVdCd0i73cfs4bZP0w9cGL9y8V7VbVFRNUzOmncr5Xri5ynzHNmvltNrzrMtXTHWkaR1Pbbr9cqDBIpduJP1UnfN7HCnYJ2z5bPg6KzrXslHz7LHk640ntSi3Zut9Tgyp/wANKvhaWL9b+k1ez/kOHL0X+C33fb+Koz8denTHxQzjXte1HNVHNXSiouKKX1bRMax0wr5jRycuAAAAAAAAABq5vTtX4Zny6womzHPIlVFxYTptr5+0bbjMvfwV83R9jH8hj7ma3n6UUJyGmOX776ru3zRbdrB9RNSJG3klcqSYdiIrs+DXc47dkT93/tPw5tNvevbMf19yHFkgLM3B2n1nNtTcHJiy30y7K8Uky7Ceajil5zLpiiv6p9i24fHrlm3ZHtbBGUaYA1KznbPwvNt3oETBsNVJ1aeI9dtnmuQ3mzyd/DW3mYrdY+5ltHnYYkPBKcz3v13KGUqBHYrRQVKSJy9dsM8xhA2uHu5ss9swmbjL3sWOOyJRYnoa5fh4tGLrteHpq2KSFf8AuSf9Jneey/LT1r7hcfzX9S6TOr5r9v8AbrJU5sprdivVUFM12zwdZOqucv2WtNVweOIxTbyzPsZrmcmuSK9kKyLpUMjly0reL/b7VtKxK2dkT3prRqr3y9huJ47jL+3jtbsh64MffvFe2W2tut1FbqGGhoomwUtOxGRRNTBERDCZMk3tNp6ZltKUisREdUPQdHdEd7b2t3d3naXDGNiJzrKxEJ/Fx/8A0V/ryIPIz/Ys1fNqyIBstuX/AJd27yp/TPMZy3/Yt6vY1nF+BVKcwfwG5eyz+jcQ8Hz19MJmb5J9EtPmdBvMh9Alhodjhyvr4evdm5e3L6Fhl+e8Sv0+9o+F8O31e5ahRrlqdnz33v3t8/31N3svAp9MMXvPGv8AVLBElHWTuC99Z/YpPSMKfnPBj6vxWvD+LP0thTJtOAQvfH/Lu6/3PpmFjxX/AGK/15EDk/As1lNmyQcjZvc7/Lu1c03pnmL5b/sW9Xsa3jPAr6/amZXJ4Br1v999YPYo/SPNZwngz6WY5jxY9Cti4VTOZE99rD7fB99CLvvAv9KRs/Fr6W0t8uCW2y19wX/4lPLMmPHGxXJ8hicNO9eK9sthlv3aTPZDUCSWSaR80rldLK5XyOXWrnLiq9s+gRERGkeRh5nWdZdQLi3A5Yo53VuYamNJJqaRKai2kxRjtlHSPTlwciIvOZ/nNxaNMcdU9Mrzh8ETrefJ0QuwzbQAGu+/l7XZ5YiLpZRwo7kVXPX5zW8JH9j1svy8/wB71K5LdVpBu99+rD7bF8pD5HwL+hK2PjV9La4w7ZKV+Iv9NYvJqfljNHwH5/V71BzfXX1+5TZoVE6v6DuZQNxrV/C6P+wj+4h8+yfNPpbnH8seh6jo7gGLzSirli7omtaKo9E499t4tfqj2vHceHb6Z9jURvRTmN8xDk4ctg9wEjVyVUMRe+ZXS4pzsjVDKc5H96Ppj3tNw0/2Z+pZZTLYAAAAAD5VNXTU0ayVEjYmJwuXA8c2emKNbzFYd6Y7WnSsao5cc6Mbiygj21/rpNCdhutTNbz+S1jowxr55/BaYOLmem86eZGqyvrK1+3UyukXgReinMiaDL7neZc863tMrbFgpjjSsPORXsAe22WetuMmxA3CNOnM7ot/pXkLDY8dl3NtKx8PlnyI243VMUdPX2LEpoGwU8cDdLYmoxF8lMD6ThxRjpFI6qxoy97d60zPlfQ9HUAAAPLdKJK6gmpVdsrIneu4lRcU7qETfbaM+G2Pq7z2wZf27xbsV7X2+roZuqqWK1fou1tcnG1T5xu9lk29u7eNPZLT4c9Mka1l5iI9gD10F1r6F2NNKrW8Ma6WL2FJu05DNgn4LdHZ5EfNtseT5oSa3ZypZcGVrOof/WN75i/Ohqtn/JMd+jLHdnt8ioz8XevTTpj70hhnhmjSSJ7ZGLqc1UVO4aPHkreNazrCstWYnSXc7uAAAAAAAFHfENauruVpurU0TxPppF8aNdtvceppeBy/DanrZ7msfxVt6lRF+pHKOcjVaiqjXYbTeBcNWI0NXByL+3AWn1fK1XcXJg+vqVRq8ccCbCecrjKc5l1yxX9Me1peHx6Y5t2ytApVuAa77+LZ6rnVtWiYMuFNHJj48arG7uI01vCZO9h0/TLL8vj0y69sK5LdVmK4ImOhNSADkbObobR+GZCtzXN2ZatHVcvPMuLfM2TFcpl7+e3ZHQ13G4u5hjz9KZFenNbt98Eke8Gpe5MGzU8D4140RmyvdabDhra4I80yyvLRpnn0QgRaK1k8r3ZtnzHbbo9FdHR1DJJGprViLg/D6qqeO5xfuY7Vjyw9dvk7mStuyW2tFW0lbSRVdJK2emnaj4pWLi1zV1Khg71ms6T0TDa1tFo1jqfZVRExVcETWp1dlG7694VFcWNy3apUngikSS4VLFxY57OjE1U14LpcvHgaXh9has/uX6OxnuV3sW/t19aoy/UgBstuX/l3bvKn9M8xnLf9i3q9jWcX4FUpzB/Abl7LP6NxDwfPX0wmZvkn0S0+Z0G8yH0CWGh2OHK+vh692bl7cvoWGX57xK/T72j4Xw7fV7lqFGuWp2fPfe/e3z/fU3ey8Cn0wxe88a/1SwRJR1k7gvfWf2KT0jCn5zwY+r8Vrw/iz9LYUybTgEL3x/y7uv8Ac+mYWPFf9iv9eRA5PwLNZTZskHI2b3O/y7tXNN6Z5i+W/wCxb1exreM8Cvr9qZlcngGvW/331g9ij9I81nCeDPpZjmPFj0K2LhVM5kT32sPt8H30Iu+8C/0pGz8WvpbL54p5KjJ17hiTGR9FOjU416tVwMbtLaZaz/yhrN1XXFaPM1LRcUxN7LFOThyub4f8yUcUdfYJ5EjqZZUqqRHLh1iK1GyNTlbsouBned29pmMkdXVK+4bPERNJ6+tdBnV8xmYcx2jL9tkuF0nbDAxF2W/Te7gYxutzlPbBgvlt3axrLyzZ646960tWc05hqcw3+su9Qmw6pf8Am48cdiNqbLGdhqG32uCMWOKR5GO3Gact5tPlYo93ikG7336sPtsXykPkfAv6ErY+NX0trjDtkpX4i/01i8mp+WM0fAfn9XvUHN9dfX7lNmhUTq/oO5lA3GtX8Lo/7CP7iHz7J80+lucfyx6HqOjuAeW7U61NrrKZExWaCSNE8tip853x27tonsl0yV1rMeZp0jVb3ruk3QqcqaD6CwzkCyty+e7fl+tqrZdZUgoa9WyRVDuhHM1Nnv14Gvbw8hTcxsrZYi9Y1mvsW3F7uuOZrbqle7b1ZnRpK2vp1jXSj0lZgvZxMx+zfsn7Gi/dp2w70N0ttf1vqNVFVJC7YlWF7Xo1ypjgqtVdJxbHavXGjmt626p1eo6O4AAxWY7rJbqFHw4dfK7YjVdKJoxVSo5nfztsWtfmtOkJmy28Zb6T1QgdRU1FTIslRI6V6/ScuPa4j5/m3F8s63mbS0ePHWkaVjR8zxegATSuCaVXUhzEauEjs2UpZ9mevxih1th1Pd5XgoafjP4/a+l83RXs8s+lVbrkor8NOme1L4IIYImxQsSONuhrWpgiGxx4q0r3axpEKS1ptOs9buejq4cqIiqupNKnEzpAglwzVdKiZywSLTwY94xiJjhyqp8/3fPZ8lp7k9yvk0aPDx2OsfFHel6LLme4NrIoKp/XwyuRiq5E2mq5cEXFCTxnO5oyxTJPeradPPDy3fH07k2r0TCaG4UIB8aujpquFYaiNJI14F4OVF4Dx3G3pmr3bxrDvjyWpOtZ0lDLzlepotqamxnpda+GxOVE1pymI5Lgr4dbY/ip98L7a8hW/RboswZn1kAAPvSV1ZRybdNK6J3Cial501KSdtu8uGdaWmHllwUyRpaNVgWW4LcLfFUqiNeuLZETVtNXBcD6Nxu8/wAjDF/L5fSzO5w/t3mr3E5HAAAAAAhG+OwPu+SKp0LNupt7krIkRMVVI8UkRPqKpZcVn/bzxr1W6FfyeHv4Z0646Ws5s2TcnA4VcEVeIDbPI1p/Cco2mgVMHxUzFlT9o9Nt/nOUwm8y/uZbW87abTH3MVa+ZnCMkAFVfEBY31NhorvE3FbfKrJ1TginwTFeRHtb2y84PN3ck0n80exTczh1pFo/KoY1DOAHptlBLcblSUESYyVc0cDOeRyN+c6Zb9ys27Id8dO9aK9stwKWnipqaKmiTCKBjY404msTBPkPn9rTMzM+VuK10jR9Thyqff3lSast1NmClYr5LeixViJr6h64o/6jtfOXvCbqK2nHP5ur0qXmNvNqxePy9foUSadnQDL2TN+Z7G1WWm5TUsSrisLVR0eK8Ow9HNx7BHzbTFlnW9Yl74t1kx/LaYfe758zld4VhuF3qJYHaHQtVImKnjNjRiL2Tri2OGk61rGv9drtk3mW8aWtLCQU89RKyCnjdLM/QyKNFc5cEx0IhJtaIjWZ6EeKzM6Q+Zy4cgbJbkZNrd7RJ4EtQ1f3zl+cx3LxpuLepq+Kn+xCW5g/gNy9ln9G4g4Pnr6YTc3yT6JafM6DeZD6BLDQ7HDlfXw9e7Ny9uX0LDL894lfp97R8L4dvq9y1CjXLU7Pnvvfvb5/vqbvZeBT6YYveeNf6pYIko6ydwXvrP7FJ6RhT854MfV+K14fxZ+lsKZNpwCF74/5d3X+59MwseK/7Ff68iByfgWaymzZIORs3ud/l3auab0zzF8t/wBi3q9jW8Z4FfX7UzK5PANet/vvrB7FH6R5rOE8GfSzHMeLHoVsXCqZzInvtYfb4PvoRd94F/pSNn4tfS2xkYx7HMeiOY5Fa5q6lRdCoYWJ0bOY1aoZ4yvUZazJV22Rq9RtLLRSLqfA9cWKnN0V5UN1stzGbHFvL5fSxu7284sk18nkYElIzmOSSORskb3RyMVHMe1Va5qpqVFTSiiYiY0kiZjphKId6O8CGBIWXudWImCK9I3u+29iu7pBnjNvM69yPvTI5DPEad5gbnd7pdKj1m5Vc1ZPqSSZ6vVE4kx1dglYsNMcaVjSEbJltedbTq86wTpA2oWNyQPcsbZVRdlXtRFc1HasURUO/ejXTyuvdnTXyOhy4Z3IT0ZnewuXUldD3XYEXfxrgv6EnZzpmr6W2JhWzUr8Rf6axeTU/LGaPgPz+r3qDm+uvr9ymzQqJ1f0HcygbjWr+F0f9hH9xD59k+afS3OP5Y9D1HR3AAGqu8XLsthzhcKNW7MEsi1NIvAsUyq5MPJXFvYNxx+4/dw1nyx0Sx2+w/t5ZjydcI2TEQA42W6sEwORenw7Kn4LeGpowqY17cZl+e8Sv0+9ouF+S3pW2US6AAEfzpTPktsczUx6iTF3kuTDHt4Gd/kuCb4ItH5J9vQsuLyRGTTthCjCNCAeihoKuumSGmjV7vpLqa1ONy8BK2uzyZ7d2kavHNnrjjW0ppZstUlBhLJhNVf1ipob5KfObnjeFx7f4p+LJ29noUG631svRHRVmS6QQAB8K9/V0VQ/wY3r2mqeG6t3cVp7Ky74o1tEedWCakPlLYPpA/Ynjf4L2r2lPXDbu3rPZMOmSNazHmWkmlMT6xDHgAABH71lWnqtqejwhqF0qz6D1+ZTPclwNM2tsfw3+6fwWW15C1Oi3TVDqmmqKaZ0M7FjkbravypxmJz4L4rd28aSvseSt41rOsPmeLuAT/K9M+ns0KPTB0mMmC8Tl0dw+jcHgnHtaxPXPT9rMb/JFss6eToej8Wjc53UQy1DGqrVkjRNnFNaNVyptYchJnf1mfhra0dse7p6Xl+xPlmIemlqoamJJYXbTcVRUVMFRU1oqLqVCThzVyV71Xlek1nSX1PV1AAADhzWuarXIitVMFRdKKigUFvB3NXagrZrhl2B1ZbJXK9aSPTNAq6Va1v02cWGlDU7Dl6WrFck6W7fJLN73i7VnvY41r2dit5bZcoXrHLRzxvTQrXRPRceZULiMlZjWJhUzjtHXEs3lPJ17ut9t8K26pSjfUR+sTuie2NsaORXqrlRE6KEbdbymOlvijvadSTttre946J01bVoiImCak1IYdsQAB8K+gpLhRT0VZGk1LUsWOaJ2pzXJgp2peazFo64db0i0TE9Utd85bncyWWqkltcD7pa1VVifCm1MxvgyRppXDwm6+Q1u05fHkjS89233MxuuMyUn4Y71UIfbbkx/Vvo52v8FYnovawLOMlZjXWFdNLR5JT7c/lC7TZyo6+soJ4aGia+dJponsYsiN2WIiuRMVxdj2Cp5Xd0jDNazE2noWfGba05YtMdENiDJtOAdZI45Y3RSNR8b0Vr2OTFFaqYKiovApzE6OJjVR2e9x9dBPLX5Xb6xSPVXOtqrhJHw4RK7Q5vEi6ec0my5mJju5evt/Fn95xMxPex9XYqutt9fQSuirqaWllbocyZjmKn2kQvKZa2jWsxMKe2O1Z0mJh52qjl2W985dSJpXuHfWHTSUky/u8zhfpGpRW6RkDtdVUIsMSJx7Tkxd9VFIefkMOLrt09kJWHZZcnVHQvTd9uutWVGetSuStvEjdmSrVMGxoutkTV1IvCute4ZjfcjfPOnVTs/Fo9nsK4enrt2/grbePuhu1vuE9zsNO6stc7lkdTRJtSwOcuLmoxNLmcWGrUXHH8rS1YrknS0eXtVO+421bTaka19it3UNc2RY1ppkkRcFYsb9rHmwLnv17YVXcnsbC7jYa+nya+nrKaWmcyrkdEkzHRq5j2tVFRHImjHEyfM2rbNrWdehp+Ji0YtJjTpTa+RyS2W4RRNV8klNM1jG6VVyxqiInOVuKdLxM9sJ+WNaz6Grbcg532U/8Awq3Un6lxtp3+D9dWQ/ws36Zc/wCwc7/6FW/uXD/Pwfrqf4eb9Mro3G2a7WrL9whudHLRyyVm2yOZqsVW9UxMUReDFDO8zmpkyRNZiej3r7icVqUmLRp0rIKdataM55KzfVZvvVTTWarmp5q2Z8UrInK1zXPVUci8Smx2m9w1w1ibxExWGT3W0yzltMVnTWWG/wBg53/0Kt/cuJP+fg/XV4f4Wb9Mp/uVyvmO15tmqblbKijgWjkYks0asarlexUTFeHQVXL7nHkxRFbRM95ZcVt8lMszasxGi8TNNCARPepQV1wyLcqShgfU1UnVdXDEm092ErVXBE5EJ3G5K0z1m06QhchSbYbREay17/2Dnf8A0Kt/cuNZ/n4P11Zn/Czfpk/2Dnf/AEKt/cuH+fg/XU/ws36ZbCbq6Cut+RbbSV0D6aqjSXrIZU2XtxleqYovIpk+SyVvntNZ1jo9jTcfSa4Yi0aT/wCUsIKaAUdvryvmO6ZthqLbbKisp0pI2LLDGrmo5HvVUxTh0ml4jdYqYpi1oidWe5Tb5L5da1mY0QD/AGDnf/Qq39y4tf8APwfrqrf8PN+mWZyZkrN9Lm6y1NRZquGnhrIXyyvicjWta9FVyrxIRt5vMNsNoi0TMw99rtMsZazNZ01bLmOaxHM75GtObbalLWYxVMWLqSsYiK+Jy6/KavC0l7PeXwW1r1eWEXdbSuauk9fklr9mbdlm/L8r+vonVVI1e9raVFljVONyIm0z6yGr23JYcvVOk9kszuNhlx9cax2wijl2F2X965NaO0L3SdrCJpL70lDW1siR0dPLUyO6LIWOkVfsop1vkrWNZmIc1pa06RCxcobj7/cpY6i+42ugxxdDii1L04kbpRnO7TyFRu+ZpSNMfxW+5abbib36b/DH3rVzLu2sd0ykzL1HG2hZS/nLfI1MerlRF0u4XbeK7XGUe3398eX9yenXrXOfY0vi7kdGnU15v+S8z2GpdDcbfKxGrg2oja6SF6cbZGph29JrMG8xZY1rLMZtrkxzpaHXLFFd/wAet09LQ1EzqeqhlVI4nu0MkRV1JxIc7nJSMdomY6Ylxt6W78TET1w22MG2qpN+2X75dpbMtroJ61IWz9asDFfs7Sswxw48C94XcY8fe78xXXT3qXl8N793uxM9aqv9g53/ANCrf3Li9/z8H66qb/Czfplw/IOd1Y7/APCrdS/qXHH+fg/XU/w836ZbU21j2W6lY9Fa9sMbXNXWio1EVDEXn4pbDHHwx6HoOruAAIrn/IFuzdbWxSu9XuFPitHWImKtVdbHp9JjuFCbsd7bBbWOms9cIe82dc9dJ6Jjqlr5mPIGbMvyubX0EjoEXvauBFlhcnHtNTvfrYGs2+/xZflnp7JZnPssuOemOjtR1XNRcFVEVNaLoJesIuj6wU9RUORtPE+Zy6EbGxz1X7KKcTeI65cxWZ6oXzuGst4ttqui3Gjmo0qJo3wJMxWK5qMVFVGu0mV5rNS9692YnSGk4jFalJ70aaytEpluAAOssUcsbopGo6N6K1zV1KinW9ItWaz0xLmtpidYQi65UrqeVXUjFqKdV73Dpt5FThMJv+Ay47a4479PvaDb8jS0aX6Lfc5tWU62pejqtFpoE1ovTdyInBzqc7D+P5ck65Pgr98m45KlY0p8U/cmNHQ0tHCkNPGkbE4tarxqvCbXb7bHhr3aRpCiyZbXnW06y+57vMAAAPDfX7FnrHfsnJ29BX8rbTbZJ/4ykbSNctfSrg+ZNW4UC0qZ/WU8T/CY13bTE+s4bd6kT2xDHXjSZh9D0dQAAA8lxtdHcIerqWY4dB6aHNXkUibzY4txXu3j1+WHthz2xzrWUNuOV7nSy4QsWphXovYmn6zTE7zgc+K3wR36+b3r3ByOO8fFPdl67LlOoklbNcG9XC1cUhXpO58NSEzjOAva0XzRpWPJ5Z9Lx3XI1iNKdM9qWVkcjqKaOHRIsbmx4aNKtwQ1+4racdor16Topccx3omerVHn0zqqBr6aNHRxUjGYYLttexy7TGLj3sicxn74pyViccfDXHHpiY64jsssK2is6W65t/WvbDKWtzZK2umi0073Ma13A57W4PVO4hZ7Ge9lyWr8kzH2xHSjZ+itYnr6f/DJlmigAAAAAAAAAAAAAAAAAAAAOksEMzdmWNsjfBeiOTunMTo4mInrfKK3W+J21FTRRuTUrWNRe4hzNpnrlxFKx1Q9B1dgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB55bdb5XbUtNFI7wnMa5e6h2i0x1S6zSs9cPrFBDC3ZijbG3iYiNTuHEy5iIjqdzhyAAAAAAAAAAAAAAAeZ9stz3bT6WFzvCWNqr8h2i8x5XScdZ8kPtFBBCmEUbY04mNRvyHEy7RER1O5w5AAAAAAAAAAAAAxOan7NjqPG2W9tyFPz19Nrbz6e1N4+uuaEAPnTTAFk2d+3aqR3HEzuNwPqHHX723pP/GGT3NdMlo872E14AAAAAAAAHkntVvnkWSSBqvd0lTFNrysMMeyRb7PFadZr0+309r0jNaI01emONkbEYxqMY1MGtamCInIiEilIrGkRpDpMzM6y7HZwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABgc5v2bQjfDlanaxX5jP/yS2m207bQseLj+76kIME0QBYWWX7djpV4mq3tOVD6RwttdrT0Mvvo0zWZMtUQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAI1nh+FJSs8KRV7Tf/cy/8otpjpHbb3LbiY+K0+ZDzFL0AnWT37VmangSPTu4/OfQP47bXax5rSznJxpm9UM2XqvAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACO5zop5qWGeJqubArusamlURyJ33YwM1/JNrfJjresa9zrWnF5q1tMT5UMMOvwCe5Vop6S1okyK18rlkRi60RUREx7R9D4La3w7f4+ibTrozPIZa3yax1QzBdIQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMbU5ds9Q9Xvpmo5dasVWY/ZwKzNxG2yTrNI18yTTeZaxpFneksVqpHo+Gnaj01Pdi5U5trE77fi9vinWtY17XGTdZLxpM9D3lgjgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAf/Z";

const getPDFInforme = (
	rowList = [],
	sumaLocalTotal = 0,
	informeType = 0,
	reportType = 0,
	isDetalle = false
) => {
	const head = [
		[
			"Formato",
			"Mercado",
			"Local Receptor",
			"RUT Cliente",
			"Folio Boleta",
			"Fecha Boleta",
			"Formato",
			"Mercado",
			"Local Emisor",
			"Rut Colaborador",
			"ID Operador",
			"Folio NC",
			"Fecha NC",
			"Motivo",
			"Descripcion",
			"Dep.",
			"EAN",
			"Cant",
			"Total"
		]
	];

	const local = getDataToExcel(normalizePDF(rowList));
	const detail = getDataToExcel(normalizePDFDetail(rowList));

	const user = Func.getUser();
	const initDate = localStorage.getItem("fromD");
	const endDate = localStorage.getItem("toD");

	const titlePDF = {
		0: "Informe Control Nota de Crédito - Detalle de la Nota de Crédito realizada",
		1: "Informe Notas de Crédito de DMF - Detalle de la Nota de Crédito realizada",
		3: "Informe Ajuste Proceso de Dañados - Detalle de la Nota de Crédito realizada",
		4: "Informes Detalles de las Notas de Crédito realizadas",
		5: "Detalle nota de crédito"
	};

	const fechaActual = new Date()
		.toLocaleDateString("es-CL")
		.replace(new RegExp("-", "g"), "");

	const filePdf = {
		0: "control_nota_de_credito",
		1: "nota_de_credito_DMF",
		3: "ajuste_preceso_de_dañados",
		5: "nota_de_credito"
	};

	const fileName = isDetalle
		? fechaActual + "_" + "detalle" + "_" + filePdf[informeType]
		: fechaActual + "_" + filePdf[informeType];

	const doc = new jsPDF({
		orientation: "landscape",
		format: "letter"
	});

	doc.autoTableSetDefaults({
		styles: {
			fontSize: 6,
			rowHeight: 8,
			cellPadding: 1,
			halign: "center",
			valign: "middle",
			cellWidth: "wrap"
		},
		headStyles: { fillColor: "0071ce", cellWidth: "wrap" },

		columnStyles: {
			0: { cellWidth: 8 }, // Forma
			1: { cellWidth: 10 }, // Mercador
			2: { cellWidth: "wrap" }, //  Local Receptor
			3: { cellWidth: 15 }, // Rut cliente *
			4: { cellWidth: 10 }, // Folio boleta
			5: { cellWidth: 12 }, // Fecha boleta
			6: { cellWidth: 8 }, // Formato
			7: { cellWidth: 12 }, // Mercado
			8: { cellWidth: "wrap" }, // Local Emisor
			9: { cellWidth: 14 }, // Ruc colaborador *
			10: { cellWidth: 10 }, // ID operador
			11: { cellWidth: 20 }, // Folcio NC *
			12: { cellWidth: 12 }, // Fecha Nc
			13: { cellWidth: 14 }, // Motivo
			14: { cellWidth: 10 }, // Descripcion
			15: { cellWidth: 15 }, // Dep.
			16: { cellWidth: 18 }, // EAN *
			17: { cellWidth: 5 }, // Cantidad
			18: { cellWidth: 10 } // Total *
		}
	});

	//Titulo mas imagen
	doc.addImage(logo, "JPEG", 14, 5, 25, 13);
	doc.setFontSize(10);
	doc.setFontType("bold");
	doc.text(`${titlePDF[informeType]}`, informeType === 5 ? 120 : 80, 13);

	//Tamaño de letras globales
	const sizeTitle = 8;
	const sizeSubtitle = 6;
	const sizeTotal = 6;
	//Espacios de palabras globales
	const spaceNC = 99;
	const spaceProduct = 218;
	const spacePeriodo = 47;
	const spaceFecha = spacePeriodo + 13;
	const spaceTotal = 240;
	//Contenido Locales y Otros locales
	const startYTableLocal = 30;
	const tituloFinalLocal = startYTableLocal - 5;
	const subtituloFinalLocal = startYTableLocal - 1;

	if (reportType === 1 && isDetalle) {
		//Titulo Local
		doc.setFontSize(sizeTitle);
		doc.setFontType("bold");
		doc.text(`Local N°${user.num_local}`, 14, tituloFinalLocal);
		doc.text("Período: ", spacePeriodo, tituloFinalLocal);
		doc.setFontType("normal");
		doc.text(`${initDate} al ${endDate}`, spaceFecha, tituloFinalLocal);
		//Subtitulo
		doc.setFontSize(sizeSubtitle);
		doc.setFontType("bold");
		doc.text("Boleta", 14, subtituloFinalLocal);
		doc.text("Nota de Credito", spaceNC, subtituloFinalLocal);
		doc.text("Productos", spaceProduct, subtituloFinalLocal);
		doc.autoTable({ head, body: detail, startY: startYTableLocal });
		doc.setFontSize(sizeTotal);
		doc.setFontType("bold");
		doc.text(
			`TOTAL MONTO: ${Fn.toCurrency(sumaLocalTotal) || 0}`,
			spaceTotal,
			doc.autoTable.previous.finalY + 5
		);

		return doc.save(`${fileName}.pdf`);
	}
	if (reportType === 2 && isDetalle) {
		//Titulo Otros Locales
		doc.setFontSize(sizeTitle);
		doc.setFontType("bold");
		doc.text("Otros Locales", 14, tituloFinalLocal);
		doc.text("Período: ", spacePeriodo, tituloFinalLocal);
		doc.setFontType("normal");
		doc.text(`${initDate} al ${endDate}`, spaceFecha, tituloFinalLocal);
		//Subtitulo
		doc.setFontSize(sizeSubtitle);
		doc.setFontType("bold");
		doc.text("Boleta", 14, subtituloFinalLocal);
		doc.text("Nota de Credito", spaceNC, subtituloFinalLocal);
		doc.text("Productos", spaceProduct, subtituloFinalLocal);
		// Tabla Otros Local
		doc.autoTable({
			head,
			body: detail,
			startY: startYTableLocal
		});
		doc.setFontSize(sizeTotal);
		doc.setFontType("bold");
		doc.text(
			`TOTAL MONTO: ${Fn.toCurrency(sumaLocalTotal) || 0}`,
			spaceTotal,
			doc.autoTable.previous.finalY + 5
		);
		return doc.save(`${fileName}.pdf`);
	}

	doc.setFontSize(sizeSubtitle);
	doc.text("Boleta", 14, 21);
	doc.text("Nota de Credito", spaceNC, 21);
	doc.text("Productos", spaceProduct, 21);
	doc.autoTable({ head: head, body: isDetalle ? detail : local, startY: 22 });
	doc.setFontSize(sizeTotal);
	doc.text(
		`TOTAL MONTO: ${Fn.toCurrency(sumaLocalTotal) || 0}`,
		spaceTotal,
		doc.autoTable.previous.finalY + 5
	);

	return doc.save(`${fileName}.pdf`);
};

export default getPDFInforme;
