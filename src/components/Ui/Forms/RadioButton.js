import React from 'react';
import './RadioButton.css';

export function RadioButton(props) {
    return (
        <label>
            <input className="with-gap" type="radio" defaultChecked={props.check} {...props.field} />
            <span>{props.text}</span>
        </label>
    )
}
