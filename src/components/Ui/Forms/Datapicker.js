import React, {Fragment} from 'react';

export function Datapicker(props) {
    return (
        <Fragment>
            <input className="datepicker" type="text" placeholder={props.placeholder} />
        </Fragment>
    )
}
