import React from 'react';

export function CheckboxButton(props) {
    return (
        <label>
            <input className="filled-in" type="checkbox" defaultChecked={props.check} disabled={props.disabled} id={props.id} />
            <span>{props.text}</span>
        </label>
    )
}
