import React from 'react';

export function Label(props) {
	const styleLabel = {
		fontSize: props.labelSize
	}
    return <label htmlFor={props.name} className={props.labelClass} style={styleLabel}>{props.label}</label>
}
