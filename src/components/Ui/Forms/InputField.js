import React from 'react';
import { Label } from './Label';
import { Field, ErrorMessage } from 'formik';
import './Input.css';

export default function InputField(props) {
	let label = '';
	let isInputField = false;
//	var z=parseInt(document.getElementById("pw").value);
	let  classNameV='';
	
	
	if (props.name !== 'groups' && props.groups !== false) {
		label = <Label name={props.name} label={props.label} labelClass={props.labelclass} labelSize={props.labelsize} />
		isInputField = true
	}
	return (
		<div className={(isInputField === true ? "input-field  "  + props.yio+ " " : " " + props.yio)+(props.maxContent && ' max-content ')} >
				<Field id="pw"  className={props.className} {...props} defaultValue={props.defaultValue} max={props.max}>{props.children}</Field>
				{label}
				{
					props.ishelptext ? <span className="helper-text">{props.help}</span> : ""
				}
				{
					props.helperText && <span className="helper-text">{props.helperText}</span>
				}
				<ErrorMessage name={props.name} component="div" className="red-text" />
		</div>
	)
}
