﻿import React from 'react';
import './Input.css';

export default function Input(props) {
    return (
        <div className="input-field">
            <input id={props.id} type={props.type} className="validate" placeholder={props.placeholder} autoComplete={props.autocomplete} />
            <label htmlFor={props.labelFor}>{props.label}</label>
        </div>
    )
}
