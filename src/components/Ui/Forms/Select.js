import React from 'react';

export function Select(props) {
    return (
        <div className="input-field">
            <select>
                {props.children}
            </select>
        </div>
    )
}
