import React from 'react';
import './Input.css';
import { ErrorMessage } from 'formik';
import { Fn } from '../../../Utils';

export default function InputFieldAutocomplete(props) {
	return (
		<div className={"input-field "} labelClassName="labelActive">
			<input
				type="text"
				id="numLocal"
				name="numLocal"
				className={props.className}
				value={props.values.email}
				onChange={props.handleChange}
				onBlur={props.handleBlur}
				onKeyDown={Fn.keyPrevent}
				max="99999"
				min="0"
				autoComplete="off"
				
				/>
			<label htmlFor="numLocal" className="usuario">N° Local</label>
			<ErrorMessage name="numLocal" component="span" className="red-text" />
		</div>
	)
}
