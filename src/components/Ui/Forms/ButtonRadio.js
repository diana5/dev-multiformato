import React from 'react';
import './ButtonRadio.css';

export function ButtonRadio(props) {
    return (
        <label className={props.class ? 'BtnRadio-container '+props.class : 'BtnRadio-container'}>
            <input className="with-gap" type="radio" name={props.name ? props.name : 'Lorem'} checked={props.checked}/>
            <span>{props.text ? props.text : 'Text: Lorem Ipsum'}</span>
        </label>
    )
}
