/**
 * 
 * @param {Array} dataset 
 * @example
 * 
    const dataSet = [
        {
            project: 'falabella-datalake-prod',
            label: 'cliente',
            dataset_name: 'cliente',
            id: '159760',
        },
        {
            project: 'falabella-datalake-prod',
            label: 'eabecerra',
            dataset_name: 'eabecerra',
            id: '163856',
        }
    ]
    console.log(getDataToExcel(dataSet))
// [ 
//   [ 'falabella-datalake-prod', 'cliente', 'cliente', '159760' ],
//   [ 'falabella-datalake-prod', 'eabecerra', 'eabecerra', '163856' ]
// ]
 */
const getDataToExcel = (dataset = []) =>
	Object.values(dataset).map(child => Object.values(child));

export default getDataToExcel;
