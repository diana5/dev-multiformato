import React from 'react';
import ReactExport from "react-data-export";
import { Fn } from '../../../Utils';

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;

class Excel extends React.Component {
    render() {
		const { ncLocal, ncRemoto } = this.props;
		const datasetLocal = []
		const datasetRemoto = []

		ncLocal.map(item =>
			datasetLocal.push(
				{
					formato: item.formato,
					mercado: item.mercado,
					localOrigen: item.originalTransaction.numLocal,
					rutCliente: Fn.formateaRut(item.customerId),
					folioBoleta: item.originalTransaction.folio,
					fechaBoleta: item.originalTransaction.date,
					imagenBoleta: "",
					localDestino: item.loginLocal,
					rutColaborador: Fn.formateaRut(item.username),
					folioNc: item.folio,
					fechaNc: item.date,
					nc: item.total
				}
			)
		)
		ncRemoto.map(item =>
			datasetRemoto.push(
				{
					formato: item.formato,
					mercado: item.mercado,
					localOrigen: item.originalTransaction.numLocal,
					rutCliente: Fn.formateaRut(item.customerId),
					folioBoleta: item.originalTransaction.folio,
					fechaBoleta: item.originalTransaction.date,
					imagenBoleta: "",
					localDestino: item.loginLocal,
					rutColaborador: Fn.formateaRut(item.username),
					folioNc: item.folio,
					fechaNc: item.date,
					nc: item.total
				}
			)
		)
        return (
            <ExcelFile filename={"REPORTE_NC_LOCAL_" + new Date().toLocaleDateString()} element={<button className="waves-effect waves-light btn Raised u-bg-green"><div className="col-lg-3"><i class="fal fa-file-excel right"></i>Descargar Excel</div></button>}>
                <ExcelSheet data={datasetLocal} name="REALIZADAS EN EL LOCAL">
                    <ExcelColumn label="FORMATO" value="formato"/>
                    <ExcelColumn label="MERCADO" value="mercado"/>
                    <ExcelColumn label="RECEPTOR" value="localOrigen"/>
                    <ExcelColumn label="RUT CLIENTE" value="rutCliente"/>
                    <ExcelColumn label="FOLIO BOLETA" value="folioBoleta"/>
                    <ExcelColumn label="FECHA BOLETA" value="fechaBoleta"/>
                    <ExcelColumn label="IMAGEN BOLETA" value="imagenBoleta"/>
                    <ExcelColumn label="EMISOR" value="localDestino"/>
                    <ExcelColumn label="RUT COLABORADOR" value="rutColaborador"/>
                    <ExcelColumn label="FOLIO NC" value="folioNc"/>
                    <ExcelColumn label="FECHA NC" value="fechaNc"/>
                    <ExcelColumn label="$ NC" value="nc"/>
                </ExcelSheet>
                <ExcelSheet data={datasetRemoto} name="REALIZADAS DESDE OTROS LOCALES">
                    <ExcelColumn label="FORMATO" value="formato"/>
                    <ExcelColumn label="MERCADO" value="mercado"/>
                    <ExcelColumn label="RECEPTOR" value="localOrigen"/>
                    <ExcelColumn label="RUT CLIENTE" value="rutCliente"/>
                    <ExcelColumn label="FOLIO BOLETA" value="folioBoleta"/>
                    <ExcelColumn label="FECHA BOLETA" value="fechaBoleta"/>
                    <ExcelColumn label="IMAGEN BOLETA" value="imagenBoleta"/>
                    <ExcelColumn label="EMISOR" value="localDestino"/>
                    <ExcelColumn label="RUT COLABORADOR" value="rutColaborador"/>
                    <ExcelColumn label="FOLIO NC" value="folioNc"/>
                    <ExcelColumn label="FECHA NC" value="fechaNc"/>
                    <ExcelColumn label="$ NC" value="nc"/>
                </ExcelSheet>
            </ExcelFile>
        );
    }
}

export default class ExcelYup extends React.Component {
    render() {
		const { detailLocal, detailFolio, detailDate, detailListProducts } = this.props;
		const listDataProducts = detailListProducts.map(item => {
			let arrItem = [item.description, item.deptNbr, item.ean, item.qty, item.weight ? item.grams : 0, item.price];
			return arrItem;
		})
		const multiDataSet = [
			{
				columns: ["N° LOCAL", "EMISOR", "RECEPTOR", "FOLIO NC", "FECHA"],
				data: [
					[detailLocal, "", "", detailFolio, detailDate]
				]
			},
			{
				xSteps: 0,
				ySteps: 3,
				columns: ["DESCRIPCIÓN", "DEPARTAMENTO", "CÓDIGO BARRA", "UNIDADES", "KILOS", "PRECIO"],
				data:listDataProducts
			}
		];
        return (
            <ExcelFile filename={"REPORTE_DETALLE_NC_LOCAL_NROLOCAL_" + this.props.detailLocal + "_FOLIO_" + this.props.detailFolio + "__" + new Date().toLocaleDateString()} element={<button className="waves-effect waves-light btn Raised u-bg-green excelH"><i className="fal fa-file-excel right buttonNewEx topEx"></i><p className="ex">Descargar Excel</p></button>}>
                <ExcelSheet dataSet={multiDataSet} name="Organization"/>
            </ExcelFile>
        );
    }
}

export {Excel,ExcelYup}
