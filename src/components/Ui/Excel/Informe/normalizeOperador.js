import { Fn } from "../../../../Utils";

const isNotExit = x => typeof x === undefined;

const normalizeOperador = (array = []) => {
	let newArray = [];

	typeof array !== undefined &&
		array.length > 0 &&
		array.map(local => {
			newArray.push({
				LOCAL: isNotExit(local.loginLocal) ? " " : local.loginLocal,
				OPERADOR: isNotExit(JSON.stringify(local.userid)) ? " " : (JSON.stringify(local.userid)),
				NOMBREAPELLIDO: isNotExit(local.nombre) ? " " : local.nombre,
				FECHAUNO: isNotExit(local.date)
					? " "
					: Fn.formatDate(local.date),
				MONTODIFERENCIA: isNotExit(local.montoDif)
					? " "
					: local.montoDif,
				TIPO: isNotExit(local.tipo) ? " " : local.tipo,
				MONTODEAJUSTE: isNotExit(local.motivoAjuste)
					? " "
					: local.motivoAjuste,
				RUTPERSONAL: isNotExit(local.docNbr)
					? " "
					: Fn.formateaRut(local.docNbr),
				FECHADOS: isNotExit(local.date)
					? " "
					: Fn.formatDate(local.date),
				FECHATRES: isNotExit(local.date)
					? " "
					: Fn.formatDate(local.date),
				CODIGOAJUSTE: isNotExit(local.codigoAjuste)
					? " "
					: local.codigoAjuste,
				VALORAJUSTE: isNotExit(local.valorAjuste)
					? " "
					: local.valorAjuste,
				MONEDA: isNotExit(local.moneda) ? " " : local.moneda,
				FECHACUATRO: isNotExit(local.date)
					? " "
					: Fn.formatDate(local.date),
				DIFERENCIAFINAL: isNotExit(local.difAjustadaFinal)
					? " "
					: local.difAjustadaFinal
			});
		});

	console.table(newArray);
	return newArray;
};

export default normalizeOperador;
