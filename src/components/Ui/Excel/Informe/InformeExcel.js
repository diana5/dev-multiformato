import React from "react";
import ReactExport from "react-data-export";

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;

const Button = () => (
	<button className="waves-effect waves-light btn Raised u-bg-green excelH">
		<i className="fal fa-file-excel right buttonNewEx topEx"></i>
		<p className="whiteTx">Descargar Excel</p>
	</button>
);

const InformeExcel = ({ localDetail, title, ...props }) => (
	<ExcelFile
		filename={`${title}` + " " + new Date().toLocaleDateString('es-CL')}
		element={<Button />}
		{...props}
	>
		<ExcelSheet dataSet={localDetail} name={title} />
	</ExcelFile>
);

export default InformeExcel;
