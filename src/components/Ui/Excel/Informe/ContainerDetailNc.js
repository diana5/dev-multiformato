import React from "react";
import InformeExcel from ".";
import createDataExcel from "../createDataExcel";
import normalizeInforme from "./normalizeInforme";
import normalizeOperador from "./normalizeOperador";
import normalizeTotal from "./normalizeTotal";
import normalizeDetail from "./normalizeDetail";
import normalizeCantidad from "./normalizeCantidad";



const columnListLocal = [
	"DOCUMENTO",
	"FORMATO",
	"MERCADO",
	"LOCAL RECEPTOR",
	"RUT CLIENTE",
	"FOLIO BOLETA",
	"FECHA BOLETA",
	"DOCUMENTO",
	"FORMATO",
	"MERCADO",
	"LOCAL EMISOR",
	"RUT COLABORADOR",
	"ID OPERADOR",
	"FOLIO NC",
	"FECHA NC",
	"MOTIVO",
	"DOCUMENTO",
	"DESCRIPCION",
	"DEPARTAMENTO",
	"EAN",
	"CANTIDAD",
	"TOTAL"
];

const columnListOperador = [
	{
		title: "LOCAL",
		width: { wch: 5.5 },
		height: { wch: 40 }
	},
	{ title: "OPERADOR", width: { wch: 9.58 } },
	{
		title: "NOMBRE Y APELLIDO",
		width: { wch: 18.42 }
	},
	{
		title: "FECHA DIFERENCIA",
		width: { wch: 16.25 }
	},
	{
		title: "MONTO DIFERENCIA",
		width: { wch: 17.08 }
	},
	{ title: "TIPO", width: { wch: 6.58 } },
	{ title: "MOTIVO DE AJUSTE", width: { wch: 16.33 } },
	{ title: "RUT PERSONAL", width: { wch: 13.75 } },
	{
		title: "FECHA DIFERENCIA",
		width: { wch: 16.25 }
	},
	{ title: "FECHA DIFERENCIA", width: { wch: 16.25 } },
	{ title: "CODIGO DE AJUSTE", width: { wch: 16.25 } },
	{ title: "VALOR DE AJUSTE", width: { wch: 15.25 } },
	{ title: "MONEDA", width: { wch: 8 } },
	{ title: "FECHA DIFERENCIA", width: { wch: 16.25 } },
	{
		title: "DIFERENCIA AJUSTADA FINAL",
		width: { wch: 20.25 }
	}
];

const renderNormalize = (body, informeType, total, isDetatil,cantidadLineas) => {
	if (informeType === 2) {
		const table = normalizeOperador(body,total);
		return createDataExcel(
			[...table, ...normalizeCantidad(cantidadLineas)],
			columnListOperador
		);
	}

	if (
		typeof body === undefined ||
		body.length === 0 ||
		typeof body === "undefined"
	) {
		return;
	}

	const table = [...normalizeInforme(body), ...normalizeTotal(total)];
	return isDetatil
		? createDataExcel(
				[...table, ...normalizeTotal(total)],
				columnListLocal
		  )
		: createDataExcel(table, columnListLocal);
};

const ContainerDetailNc = ({
	rowList = [],
	rowOperador = {},
	sumaTotal = 0,
	cantidadTotal = 0,
	informeType = 0,
	isDetalle = false,
	cantidadLineas=0,
	...props
}) => {
	const titleType = {
		3: "AJUSTE PROCESO DE DAÑOS",
		0: "CONTROL NOTA DE CREDITO",
		1: "NOTAS DE CREDITO DMF",
		2: "INFORME AJUSTE OPERADOR"
	};

	const filenameType = {
		3: "ajuste_proceso_de_dañados",
		0: "control_nota_de_credito",
		1: "detalle_nota_de_credito_DMF",
		2: "ajuste_operador"
	};

	const fechaActual = new Date()
		.toLocaleDateString("es-CL")
		.replace(new RegExp("-", "g"), "");

	const dataSetLocal = renderNormalize(
		informeType === 2 ? rowOperador.reportDetail : rowList,
		informeType,
		sumaTotal,
		isDetalle,
		cantidadLineas
	);

	return (
		<InformeExcel
			title={
				isDetalle
					? titleType[informeType] + " DETALLE"
					: titleType[informeType]
			}
			filename={
				isDetalle
					? fechaActual +
					  "_" +
					  "detalle" +
					  "_" +
					  filenameType[informeType]
					: fechaActual + "_" + filenameType[informeType]
			}
			localDetail={dataSetLocal}
			sumaTotal={sumaTotal}
			{...props}
		/>
	);
};

export default ContainerDetailNc;
