import getDataToExcel from "./getDataToExcel";

const createDataExcel = (dataset, columns) => [
	{
		columns,
		data: getDataToExcel(dataset),
	},
];

export default createDataExcel;
