import React from "react";
import ReactExport from "react-data-export";

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;

const Button = ({ children }) => (
	<button className="waves-effect waves-light btn Raised u-bg-green excelH bigFoot toptop" >
		<i className="fal fa-file-excel right buttonNewEx topEx "></i>
		<p className="exAjuste">{children}</p>
	</button>
);

const DetalleNcExcel = ({operadorDetail }) => (
	<ExcelFile
		filename={"REPORTE_NOTA_CREDITO_" + new Date().toLocaleDateString()}
		element={<Button>Descargar Excel</Button>}
	>

		<ExcelSheet dataSet={operadorDetail} name="AJUSTE OPERADOR" />
	</ExcelFile>
);

export default DetalleNcExcel;