import React from "react";
import NotaCreditoExcel from "./NotaCreditoExcel";
import createDataExcel from "../createDataExcel";
import { normalizeTabla } from "../../../../services/reportes";
import normalizeTotalNc from "./normalizeTotalNc";

const operadorDetail = [
	{
		local: "LIDER",
		operador: "LIDER",
		nombreCompleto: "ALEJANDRO VEGA",
		fechaUno: "12-02-2020",
		tipo: "TIPO",
		motivo: "SIN MOTIVO",
		rut: "18.123.123-0",
		fechaDos: "12-02-2020",
		fechaTres: "12-02-2020",
		codigo: "ERT-DF",
		fechaCuatro: "12-02-2020",
		fechaCinco: "12-02-2020"
	}
];

const columnListOperador = [

	{
		title: "LOCAL",
		width: { wch: 5.5 },
		height: { wch: 40 }
	},
	{ title: "OPERADOR", width: { wch: 9.58 } },
	{
		title: "NOMBRE Y APELLIDO",
		width: { wch: 18.42 }
	},
	{
		title: "FECHA DIFERENCIA",
		width: { wch: 16.25 }
	},
	{
		title: "MONTO DIFERENCIA",
		width: { wch: 17.08 }
	},
	{ title: "TIPO", width: { wch: 6.58 } },
	{ title: "MOTIVO DE AJUSTE", width: { wch: 16.33 } },
	{ title: "RUT PERSONAL", width: { wch: 13.75 } },
	{
		title: "FECHA DIFERENCIA",
		width: { wch: 16.25 }
	},
	{ title: "FECHA DIFERENCIA", width: { wch: 16.25 } },
	{ title: "CODIGO DE AJUSTE", width: { wch: 16.25 } },
	{ title: "VALOR DE AJUSTE", width: { wch: 15.25 } },
	{ title: "MONEDA", width: { wch: 8 } },
	{ title: "FECHA DIFERENCIA", width: { wch: 16.25 } },
	{
		title: "DIFERENCIA AJUSTADA FINAL",
		width: { wch: 20.25 }
	}
];

const columnListLocal = [
	"DOCUMENTO",
	"FORMATO",
	"MERCADO",
	"LOCAL RECEPTOR",
	"RUT CLIENTE",
	"FOLIO BOLETA",
	"FECHA BOLETA",
	"DOCUMENTO",
	"FORMATO",
	"MERCADO",
	"LOCAL EMISOR",
	"RUT COLABORADOR",
	"ID OPERADOR",
	"FOLIO NC",
	"MOTIVO",
	"DOCUMENTO",
	"DESCRIPCION",
	"DEPARTAMENTO",
	"EAN",
	"CANTIDAD",
	"TOTAL",
];

const NotaCreditoExcelContainer = ({
	rowLocal,
	rowRemoto,
	totalLocal = 0,
	totalRemoto = 0
}) => {
	const localDetail = [
		...normalizeTabla(rowLocal),
		...normalizeTotalNc(totalLocal)
	];
	const localList = [
		...normalizeTabla(rowRemoto),
		...normalizeTotalNc(totalRemoto)
	];

	const dataSetLocal = createDataExcel(localDetail, columnListLocal);
	const dataSetRemoteLocal = createDataExcel(localList, columnListLocal);
	const dataSetOperador = createDataExcel(operadorDetail, columnListOperador);
	return (
		<NotaCreditoExcel
			localDetail={dataSetLocal}
			localList={dataSetRemoteLocal}
			operadorDetail={dataSetOperador}
		/>
	);
};

export default NotaCreditoExcelContainer;
