import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { Func } from '../func/func';

export const LoginRoute = ({ component: Component, ...rest }) => {
	let user = Func.getUser();
	let perfil = user === null ? 0 : user.perfil
	let path = null;
	switch (perfil) {
		case 1:
		path = <Route {...rest} render={props => {
				return (
					<Redirect to={{ pathname: '/boleta', state: { from: props.location } }} />
				)
			}}
			/>
			break;
		case 2:
		path = <Route {...rest} render={props => {
				return (
					<Redirect to={{ pathname: '/reportlocal', state: { from: props.location } }} />
				)
			}}
			/>
			break;
		case 3:
		path = <Route {...rest} render={props => {
				return (
					<Redirect to={{ pathname: '/reportcentral', state: { from: props.location } }} />
				)
			}}
			/>
			break;

			case 3:
		path = <Route {...rest} render={props => {
				return (
					<Redirect to={{ pathname: '/localesreport', state: { from: props.location } }} />
				)
			}}
			/>
			break;

		default:
			path = <Route {...rest} render={props => {
				return (
					<Component {...props} />
				)
			}}
			/>
			break;
	}
	return(path)
}
