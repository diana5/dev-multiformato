import React, { Component } from 'react'
import M from 'materialize-css';
import { withFormik, Form, ErrorMessage,Field } from 'formik'
import * as Yup from 'yup'
import MainCardLead from '../Shared/Main/MainCardLead'
import InputField from '../Ui/Forms/InputField'
import Raised from '../Ui/Buttons/Raised';
import Loader from '../Loader';
import { Fn } from '../../Utils';
import Modal from '../Shared/Modal/Modal';
import ModalHeader from '../Shared/Modal/ModalHeader';
import ModalMain from '../Shared/Modal/ModalMain';
import ModalFooter from '../Shared/Modal/ModalFooter';
import ModalButtonRaised from '../Shared/Modal/ModalButtonRaised';
import conf from '../../config.json';
import BreadCum from '../Shared/Main/BreadCum';
import Ciudades from "./Ciudades";
import Comunas from "./Comunas";
import TipoPago from "./TipoPago";
import Regiones from "./Regiones";
import Checkbox from '@material-ui/core/Checkbox';
import InputMask from "react-input-mask";
import MaterialInput from '@material-ui/core/Input';
import PropTypes from "prop-types";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import InputLabel from '@material-ui/core/InputLabel';
import './Login.css';
import { isThisSecond } from 'date-fns';
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'
import * as iconoLimpiarDatosPng from '../../images/icono-limpiar-datos.png';
import { handleClickAllLogout } from '../../Utils';

const MySwal = withReactContent(Swal)


const styles = { opacity: 1 }


function validationEmail(value, nameField) {
	let classEmail = ""
	const errors = {};
	const emailPattern = /(.+)@(.+){2,}\.(.+){2,}/;
	if (!emailPattern.test(value)) {
		return classEmail = "usuario-error";
	}

	if (emailPattern.test(value)) {
		this.classEmail = "";
	}
}

function validationString(value, nameField) {
	if (value === "" | value === undefined) {
		this.class[nameField] = "usuario-error"
	}
}

//Funciones que llamo dentro de formik		
//no se puede llamar al una funcion con los mismos nombres que las de formik

let obj = []
const setComunas = (comunasFiltradas) => {
	obj = comunasFiltradas;
};




const Application = ({
	values,
	errors,
	touched,
	handleChange,
	isSubmitting,
}, handleComuna = { handleComuna }) => (
		<Form onload="disableF5()">
			<div className="row ">

				<div className="col-md-4 col-lg-4 col-10 col-sm-10 mt-4">

					<InputField
						id="inputRut"
						type="text"
						name="inputRut"
						label="RUT"
						yio="m-0"
						labelclass="active"
						autocomplete="off"

					/>

				</div>
				<div className="col-md-2 col-lg-2 col-2">

					<button type="button" id="search" name="search" disabled tabIndex="2" className="Raised waves-effect waves-light greenBtn btn mt-4"><i className="fal fa-search u-fz-16"></i></button>
				</div>
			</div><div className="col-md-4 col-lg-4 "></div>
			<div className="col-lg-4"></div>
			<div className="col-lg-4 col-md-4 col-sm-12 col-12">
			<div className="warning"><div><b>Recuerda</b>
			<p> Los datos solicitados a continuación es información requerida por SII para poder realizar devoluciones.</p>
			
			</div></div></div>

			

			<span className="mostrarForm">
				<div className="row mt-2">
					<div className="col-md-6 col-lg-6 col-sm-12 col-12"></div>

					<div className="col-md-6 col-lg-6  col-sm-12 col-12">
						<InputField
							id="inputNombre"
							type="text"
							name="inputNombre"
							label="Nombre*"
							labelclass="active"
							className=""

						/>
					</div>
					<div className="col-md-6 col-lg-6  col-sm-12 col-12"></div>
					<div className="col-md-6 col-lg-6  col-sm-12 col-12">
						<InputField
							id="inputEmail"
							type="email"
							name="inputEmail"
							label="Correo"
							labelclass="active"
							onKeyDown={(e) => validationEmail(e, "classNombre")}
							className=""

						/>
						
					</div>

					<div className="col-md-6 col-lg-6  col-sm-0 col-0"></div>
					<div className="col-md-6 col-lg-6  col-sm-12 col-12">
						<InputField
							id="inputDireccion"
							type="text"
							name="inputDireccion"
							label="Dirección*"
							labelclass="active"
							maxLength="50"
							className=""

						/>
					</div>


					<div className="col-md-6  col-sm-0 col-0 col-lg-6"></div>
					<div className="col-md-4 col-lg-4  col-sm-6 col-6">

						<Regiones
							id="inputRegion"
							name="" inputRegion
							placeholder="Region*"
							placeholder="Regiones" {...this.props}
							regionSelected={localStorage.getItem("regionCustomer")}
							comunaSelected={localStorage.getItem("comunaCustomer")}
							required
							handleRegiones={(e) => {


								let regNombre = e.target.value.nombre
								let regId = e.target.value.id
								localStorage.setItem("regionId", regId)
								localStorage.setItem("regionNombre", regNombre)
								let token = JSON.parse(sessionStorage.getItem('user'));
								const rq = {
									apid: conf.access.apid,
									region: e.target.value.id
								}
								fetch(conf.url.base + conf.endpoint.getComunas, {
									method: 'post',
									body: JSON.stringify(rq),
									headers: {
										'Content-Type': 'application/json',
										'Authorization': 'Bearer ' + token.access_token
									}
								}).then(res => res.json())
									.catch(function (error) {
									})
									.then(function (response) {
										// handleChange(response)
										setComunas(response.comunas)
										localStorage.removeItem("filtroComunas")
										localStorage.setItem("filtroComunas", JSON.stringify(response.comunas))


										if (response != undefined) {
										}

										if (response === undefined) {
											//setSubmitting(false);
											return console.log('Log: ', { errorCode: 1, errorMessage: 'No existe respuesta' })
										}
									})
							}}

							
							className=""

						/>

					</div>

					<div className="col-md-2 col-lg-2  col-sm-6 col-6 ciudad">
						<InputField
							id="inputCiudad"
							type="text"
							name="inputCiudad"
							label="Ciudad*"
							style={{height:'2.5em'}}
							labelclass="active"
							maxLength="20"
							onBlur={(e) => {
							
								localStorage.setItem("ciudad", e.target.value)
							}}
							

						/>

					</div>


					<div className="col-md-6  col-sm-0 col-0 col-lg-6"></div>
					<div className="col-md-6  col-sm-6 col-6 col-lg-6"></div>
					<div className="col-md-6 col-lg-6 col-6"></div>


					<div className="col-lg-6  col-sm-6 col-6 col-md-6"></div>
					<div className="col-lg-6 col-md-6 col-0 col-sm-0"></div>

					<div className="col-md-6 col-lg-6 col-sm-12  col-12 tipoP">
					
					<TipoPago placeholder="Tipo de Devolucion" id="inputTipoPago" name="inputTipoPago" 
					value={values.inputTipoPago}

					handleTipoPago={(e) => {
						values.inputTipoPago=e.target.value

						localStorage.setItem("medioPago", JSON.stringify(e.target.value));
						// localStorage.setItem("tipoPago", JSON.stringify(e.target.value.nombre))
						// localStorage.setItem("pago", JSON.stringify(e.target.value))


					}} defaultValue={localStorage.getItem("medioPago")}

						className="" />
						{errors.inputTipoPago ? (             
						<div>{errors.inputTipoPago}</div>           
						) : null}</div>
					<div className="col-lg-6 col-md-6 col-6 col-sm-6"></div>
					<div className="col-lg-4 col-md-6 col-sm-12 col-12 compartir">
						<div id="top">
							<div class="mdc-checkbox__background">
								<div class="mdc-checkbox mt-4 autorizo">

									<InputField
										class="mdc-checkbox__native-control"
										type="checkbox"
										name="politica"
										id="politica"
										value={values.politica}
										label=""
										className="ch"

									/></div></div>

							<p className="leftText">&nbsp;&nbsp; Cliente autoriza la utilización de sus datos &nbsp;&nbsp; 												
												<i className="fas fa-question-circle fa-lg tooltippedInfo" data-position="top" onClick={()=>{
													let usr = JSON.parse(sessionStorage.getItem('user'));

													fetch(conf.url.base + conf.endpoint.getFixedFile+"/politicas", {
														method: 'get',
														responseType: 'arraybuffer',
														
														headers: {
															'Content-Type': 'application/json',
															'Authorization': 'Bearer ' + usr.access_token
														}
													}).then(res => res.blob( console.log(res)))
													.catch(function (error) {
														console.log('Error:', error);
													})
													.then(function (blob) {
														if (blob === undefined)
															return console.log('Log: ', { errorCode: 1, errorMessage: 'No existe respuesta' })
															console.log("estoy aqui")
															console.log(blob)
											
														const file = new Blob(
															[blob],
															{ type: 'application/pdf' });
											
															const data = new Blob(
																[blob],
																{ type: 'text/plain' });
											
														const href = window.URL.createObjectURL(file);
											
														window.open(window.URL.createObjectURL(file));
														var w = window.URL.createObjectURL(file);
														// w.document.write(data);
														// w.print();
											
													})
											
											
											
									
													 }} ></i></p></div>

					</div>
					<div className="col-md-6 col-lg-6"></div>
					<div className="col-md-6 col-lg-6"><p className="mt mt">Campos Obligatorios*</p></div>

					

				</div>
				<div className="row">
					<div className="col-lg-12">
						<br></br><br></br>
					</div>
				</div>
				<div className="row pasos">
					<div className="col-lg-3 col-md-3 col-lg-3 col-1  col-sm-3" >
						<a type="button" id="butt" name="button_2" value="Anterior" className="Raised btn waves-effect waves-light px-5 pt-2 pb-2" onClick={(e) => {
							e.preventDefault();
							localStorage.setItem("recargaProductos", "si")
							window.location.href = '/dmf/product';
						}}>Anterior</a></div>
					<div className="col-md-5 col-lg-5 col-0 col-sm-5"></div>
					
					<div className="col-md-1 col-lg-1 col-md-1">
									<button class="Raised btn btn-limpiar px-5 pt-2 pb-2" type="button" onClick={()=> {
																				localStorage.removeItem("rutCliente")
																				localStorage.removeItem("screenCliente")
																				window.location.reload();
																				
																			}
									}><img style={{marginLeft:"-130%", marginRight:"10%", marginTop:"10%"}} src={iconoLimpiarDatosPng} alt="Limpiar Icono" width={120} /> <p style={{marginTop:"-43px", marginLeft:"5px"}}>Limpiar</p></button>
					</div>
					<div className="col-md-1 col-lg-1 col-0 col-sm-1"></div>

					<div className="col-md-2 col-lg-2 col-1 col-sm-3">
						<Raised isSubmit={true} disabled={isSubmitting || !values.inputTipoPago} text={isSubmitting === true ? <Loader /> : 'Siguiente'} isIcon={false}></Raised>
					
					</div>
				</div>
				<br></br><br></br>
				{/* <div className="Card-Footer card-action p-cliente">
					<span></span>
				</div> */}
			</span>
		</Form>

	)

const st = this.state
const FormikApp = withFormik({
	mapPropsToValues: ({ rut, nombre, inputRut, inputNombre, inputTelefono, inputEmail, inputDireccion, inputRegion, inputComuna, inputCiudad, inputTipoPago, politica,tipoPago }) => ({ rut: localStorage.getItem("rutCliente") || '', nombre: nombre || '', inputRut: localStorage.getItem("rutCliente") || '', inputNombre: inputNombre || '', inputTelefono: inputTelefono || '', inputEmail: inputEmail || '', inputDireccion: inputDireccion || '', inputComuna: inputComuna || '', inputCiudad: inputCiudad || '', politica: politica }),
	validate: values => {
		const errors = {};

		if (!values.rut) {

			this.classRut = "usuario-error"
		} else if (values.rut) {
			this.classRut = ""
		}

		return errors;
	},



	handleSubmit: (values, { props, resetForm, setErrors, setSubmitting }) => {
		console.log("entra dubmit")
		setTimeout(() => {
			let usr = JSON.parse(sessionStorage.getItem('user'));
			let rut = values.inputRut.replace('-', '')
			let com = ""
			let reg = ""
			let pago = null
			if (localStorage.getItem("comunaN")) {
				com = localStorage.getItem("comunaN")
			} else {
				com = values.inputComuna
			}

			if (localStorage.getItem("regionN")) {
				reg = localStorage.getItem("regionN")
			} else {
				reg = values.inputRegion
			}

			if (localStorage.getItem("tipoPago")) {
				pago = localStorage.getItem("medioPago")
			} else {
				pago = values.inputTipoPago
			}
			const request = {
				numLocal: usr.num_local,
				customerId: localStorage.getItem("rutCliente").replace("-", "").replace("-", ""),
				nombre: values.inputNombre,
				email: values.inputEmail,
				telefono: values.inputTelefono,
				direccion: values.inputDireccion,
				comuna: com,
				ciudad: localStorage.getItem("ciudad"),
				region: reg,
				compartir: values.politica,
			}
			localStorage.setItem("peticion", JSON.stringify(request))

			localStorage.setItem("nombreCliente", request.nombre)
			localStorage.setItem("email", request.email)
			localStorage.setItem("direccion", request.direccion)

			fetch(conf.url.base + conf.endpoint.addCustomer, {
				method: 'post',
				body: JSON.stringify(request),
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'Bearer ' + usr.access_token
				}
			}).then(res => res.json())
				.catch(function (error) {
				
				})
				.then(function (response) {
					
					if (response.errorCode === 0 && !response.errorMessage) {
						
						if (response.customerId) {
							let rut = values.inputRut.replace('-', '')
							props.data.customerId = rut;
							props.data.numLocal = usr.num_local;
							console.log('REQUEST', props.data)



						} else {
							console.log('Error:', response);


							switch (response.errorCode) {
								case 1:
									props.openModalErr(response.errorMessage)
									break;
								case 2:
									props.openModalErr(response.errorMessage)
									break;
								case 3:
									props.openModalErr(response.errorMessage)
									break;
								case 4:
									props.openModalErr(response.errorMessage)
									break;

								default:
									setErrors({ inputCiudad: response.errorMessage })
									break;
							}
						}
					}
				})
		}, 10);


		setTimeout(() => {
			let usr = JSON.parse(sessionStorage.getItem('user'));
			let rut = values.inputRut.replace('-', '')
			const request = {
				numLocal: usr.num_local,
				customerId: rut,
				nombre: values.inputNombre,
				email: values.inputEmail,
				telefono: values.inputTelefono,
				direccion: values.inputDireccion,
				comuna: localStorage.getItem("comuna"),
				ciudad: values.inputCiudad,
				region: localStorage.getItem("region"),


			}

			const pantallaAnterior = JSON.parse(localStorage.getItem("pantallaProductos"));
			console.log(pantallaAnterior)
			console.log("MEDIOPAGO",localStorage.getItem("medioPago"))
			const rq = {
				numLocal:  localStorage.getItem("numLocal"),
				idFile: pantallaAnterior.idFile,
				customerId: localStorage.getItem("rutCliente").replace("-", ""),
				reason: localStorage.getItem("motivoSelected"),
				originalTransaction: {
					numLocal: localStorage.getItem("numLocal"),
					date: localStorage.getItem("date").replace("-", ""),
					pos: localStorage.getItem("pos"),
					transaction: localStorage.getItem("transaction"),
				},
				products: pantallaAnterior.products,
				tipoPagoDescription: "",
				tipoPago: JSON.parse(localStorage.getItem("medioPago")),
				payment: {

				}
			}
			console.log(rq)

			fetch(conf.url.base + conf.endpoint.saveCreditNote, {
				method: 'post',
				body: JSON.stringify(rq),
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'Bearer ' + usr.access_token
				}
			}).then(res => res.json())
				.catch(function (error) {
					console.log('Error:', error);
					console.log('Agregar tipo pago')
				})
				.then(function (response) {
					console.log(request)
					console.log('Succes Respuesta SaveC:', response);

					if (response.errorCode === 0) {

						localStorage.setItem("recargarResumen", "si")
						localStorage.setItem("recargarResumen", "si")

						localStorage.getItem("pantallaProductos")

						// props.history.push({
						// 	pathname: "/resumen",
						// 	state: st

						// })}
						window.location.href='/dmf/resumen'
						localStorage.setItem("warning",response.errorMessage)
					}
					else
					if(response.errorCode===-1){
						MySwal.fire({

							text: "Atención",
							html:
								"<b>Atención</b><br><hr><br>" +
								'<h7>Su sesión ha expirado</h7>,<br>' +
								'<h7>Presiona aceptar para volver al inicio</h7> </div><br><br><hr>',
							showCancelButton: true,
							confirmButtonColor: '#fff',
							cancelButtonColor: '#3085d6',
							confirmButtonText: 'Cancelar',
							cancelButtonText: 'Aceptar'
						}).then((result) => {
							handleClickAllLogout()
							
						});
			
					}


					if (response.customerId) {
						let rut = values.inputRut.replace('-', '')
						props.data.customerId = rut;
						props.data.numLocal = usr.num_local;
						console.log('REQUEST', props.data)



					} else {
						console.log('Error:', response);
						switch (response.errorCode) {
							case 1:
								props.openModalErr(response.errorMessage)
								break;
							case 2:
								props.openModalErr(response.errorMessage)
								break;
							case 3:
								props.openModalErr(response.errorMessage)
								break;
							case 4:
								props.openModalErr(response.errorMessage)
								break;

							default:
								setErrors({ inputCiudad: response.errorMessage })
								break;
						}
					}
				})
		}, 1000);

	},
	


	validationSchema: Yup.object().shape({
		inputTipoPago: Yup.number()
		.required('Dato Invalido'),
		inputRut: Yup.string()
			.required('Dato invalido'),

		inputNombre: Yup.string()
			.required('Dato Invalido'),
		inputDireccion: Yup.string()
			.required('Dato invalido'),
		inputEmail: Yup.string()
			.email('El correo electónico ingresado no es válido'),
		inputCiudad : Yup.string()
			.required('Dato Invalido'),
		

	
		

	}),

})(Application)

export class Client extends Component {


	constructor(props) {
		super(props)


		this.state = {
			inputRut: '',
			inputNombre: '',
			inputTelefono: '',
			inputEmail: '',
			inputDireccion: '',
			inputComuna: '',
			inputCiudad: '',
			nombre: '',
			rut: '',
			isChange: false,
			isErr: false,
			messageErr: '',
			description: 'Ingresa el RUT del cliente y presiona la lupa para buscar.',
			isError: false,
			errorMessage: '',
			clase: 'esconderForm',
			politica: false,
			alertError: '',
			alertSuberror: '',
			comunaSelected: '',
			classN: '',
			comunas: [],
			claseWarning:"warning"

		}
		
		this.handleCheckbox = this.handleCheckbox.bind(this)
		this.anterior = this.anterior.bind(this)
		this.handleComuna = this.handleComuna.bind(this)
		this.setCurrentState = this.setCurrentState.bind(this);
		this.setPago= this.setPago.bind(this)
		this.getPolitic= this.getPolitic.bind(this)
		// this.getComunasFiltered= this.getComunasFiltered.bind(this);


	}



	displayName = Client.name


	handleSome = (e) => {
		console.log("ENTRAAAAAAA", e)
	}

	handleComuna = (event) => {

		this.setState({ comunaSelected: event })
		localStorage.setItem("comuna", this.state.comunaSelected)
		console.log("comuna===>", this.state.ciudadSelected)
	}

	setPago = (event) =>{
		console.log("el event", event)
		this.setState ({ tipoPagoDescription:event})
	}

	setCurrentState = (ComunasFiltered) => {
		console.log("las comunsa filtradas", ComunasFiltered)

	}

	anterior = () => {

		console.log("Esta entrando")
		window.location.href = '/dmf/product';

		localStorage.setItem("regreso", "si")
		this.setState({ isErr: false })

	}

	getPolitic =() =>{
		let usr = JSON.parse(sessionStorage.getItem('user'));

		fetch(conf.url.base + conf.endpoint.getFixedFile+"/politicas", {
			method: 'post',
			
			headers: {
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + usr.access_token
			}
		}).then(res => res.json())
			.catch(function (error) {
				console.log('Error:', error);
				console.log('Agregar tipo pago')
			})
			.then(function (response) {
				console.log()
				console.log('fixedfile:', response);
			})
	}


	handleCheckbox(event, isInputChecked, value) {
		console.log(event);
		this.res.add(value);
		if (this.res.size === 3) console.log(this.res);
	}

	setWidth = () => {
		this.props.setWidth("col-12 col-md-12 col-lg-12")
	}
	componentDidMount() {
		localStorage.setItem("screenProducto", "si")
		this.setWidth()
		let region=""
		// if(localStorage.getItem(("regionCustomer"))){
		// region=localStorage.getItem("regionCustomer")
		
		  const request = {apid: conf.access.apid}

		  var comu=[]
		  let token = JSON.parse(sessionStorage.getItem('user'));
		  setTimeout(() => {
		  fetch(conf.url.base + conf.endpoint.getRegiones, {
			  method: 'post',
			  body: JSON.stringify(request),
			  headers: {
				  'Content-Type': 'application/json',
				  'Authorization': 'Bearer ' + token.access_token
			  }
		  }).then(res => res.json())
			  .catch(function (error) {
				  console.log('Error:', error);
			  })
			  .then((response) => {
				  console.log('REGIONESSSSSSSSS:', response);
	  
				  if (response != undefined) {
				  this.setState({
					  regiones: response.regiones
				  }) }
				  
				  if (response === undefined) {
					  //setSubmitting(false);
					  return console.log('Log: ', { errorCode: 1, errorMessage: 'No existe respuesta' })
				  }
		}) },500);


				localStorage.setItem("screenProducto", "si")
		this.setWidth()

	
		  var comu=[]
		  setTimeout(() => {
		  fetch(conf.url.base + conf.endpoint.getComunas, {
			  method: 'post',
			  body: JSON.stringify(request),
			  headers: {
				  'Content-Type': 'application/json',
				  'Authorization': 'Bearer ' + token.access_token
			  }
		  }).then(res => res.json())
			  .catch(function (error) {
				  console.log('Error:', error);
			  })
			  .then((response) => {
				  console.log('REGIONESSSSSSSSS:', response);
	  
				  if (response != undefined) {
				  this.setState({
					  comunas: response.comunas
				  }) }
				  
				  if (response === undefined) {
					  //setSubmitting(false);
					  return console.log('Log: ', { errorCode: 1, errorMessage: 'No existe respuesta' })
				  }
		}) },500);
		


		  var comu=[]
		  setTimeout(() => {
		  fetch(conf.url.base + conf.endpoint.getComunas, {
			  method: 'post',
			  body: JSON.stringify(request),
			  headers: {
				  'Content-Type': 'application/json',
				  'Authorization': 'Bearer ' + token.access_token
			  }
		  }).then(res => res.json())
			  .catch(function (error) {
				  console.log('Error:', error);
			  })
			  .then((response) => {
				  console.log('REGIONESSSSSSSSS:', response);
	  
				  if (response != undefined) {
				  this.setState({
					  comunas: response.comunas
				  }) }
				  
				  if (response === undefined) {
					  //setSubmitting(false);
					  return console.log('Log: ', { errorCode: 1, errorMessage: 'No existe respuesta' })
				  }
		}) },500);

	  
		//////////

		if (localStorage.getItem("marcaResumen")) {
			this.handleLoad()
			
			console.log("request DIDMOUN=>", localStorage.getItem("peticion"))
			setTimeout(() => {
				let token = JSON.parse(sessionStorage.getItem('user'));
				fetch(conf.url.base + conf.endpoint.getCustomer, {
					method: 'post',
					body: localStorage.getItem("peticion"),
					headers: {
						'Content-Type': 'application/json',
						'Authorization': 'Bearer ' + token.access_token
					}
				}).then(res => res.json())
					.catch(function (error) {
						console.log('Error:', error);
						console.log('ERROR=======>')
					})

					.then((res) =>{
						console.log('RESPUESTA GETCUSTOMER:', res)
						if (res.errorCode === 0 && !res.errorMessage) {
							console.log("La ciudad que obtengo del server", res.ciudad)
							
							this.setState({
								inputRut:  res.rut,
								inputNombre: res.nombre,
								inputTelefono:  res.telefono,
								inputEmail: res.email,
								inputDireccion: res.direccion,
								inputComuna: res.comuna,
								inputCiudad: res.ciudad,
								nombre: res.comuna,
								rut: res.region,
								description: 'Completa los datos requeridos',
								clase: 'mostrarForm'
							})
						 }

						

						 else {
							console.log('Error:', res);
							this.setState({ messageErr: res.errorMessage })
						} 


					})


			}, 1000)


						setTimeout(() => { isChange : true }, 5000)
  
			let token = JSON.parse(sessionStorage.getItem('user'));
			const rq = {
				apid: conf.access.apid
			}
			
		
		}
	}



	openModalErr = (message) => {
		this.setState({
			isError: true,
			errorMessage: message
		})
		var elem = document.querySelector('.modal');
		var instance = M.Modal.init(elem);
		instance.open();
	}


	anterior = () => {

		console.log("Esta entrando")
		window.location.href = '/dmf/product';

		localStorage.setItem("regreso", "si")
		this.setState({ isErr: false })

	}

	handleBlur = () => {
		

		localStorage.setItem("rutCliente", document.getElementById('rutSearch').value)

		if (!Fn.validaRut(localStorage.getItem("rutCliente"))) {
			this.setState({
				isErr: true,
				messageErr: 'No es un rut válido',
				alertError: "No se ha encontrado datos con RUT ingresado",
				alertSuberror: "Por Favor, ingrese los datos del cliente manualmente dentro del formulario",
				claseError: "pink-square"
			})
			this.classRut = "usuario-error";
		} else {
			this.setState({
				isErr: false,
				messageErr: ''
			})
		}


	}

	afterError = () => {
		if( this.state.isErr===true){
			window.location.reload();
		}

	}

	handleCheckbox = (e) => {
		console.log(e.target.value)

	}

	handleLoad = () => {


		// this.setState({
		// 	inputRut: localStorage.getItem("rutCliente"),
		// 	inputNombre: localStorage.getItem("nombreCliente"),
		// 	inputTelefono: localStorage.getItem("telefono"),
		// 	inputEmail: localStorage.getItem("email"),
		// 	inputDireccion: localStorage.getItem("direccion"),
		// 	inputComuna: localStorage.getItem("comuna"),
		// 	inputRegion: localStorage.getItem("region"),
		// 	inputCiudad: localStorage.getItem("ciudad"),
		// 	nombre: localStorage.getItem("nombreCliente"),
		// 	inputTipoPago: localStorage.getItem("tipoPago"),
		// 	rut: localStorage.getItem("rut"),
		// 	isChange: true,
		// 	description: 'Completa los datos requeridos',
		// 	clase: 'mostrarForm'
		// })



	}

	handleClick = () => {

		

		if (!localStorage.getItem("rutCliente")) {
			localStorage.setItem("rutCliente", document.getElementById("rutSearch").value)
		}

		const request = {
			customerId: localStorage.getItem("rutCliente").replace("-", "")
		}
		console.log('request!!', request);
		const nose = {
			inputRut: localStorage.getItem("rutCliente").replace(".").replace(".").replace("-"),
			inputNombre: '',
			inputTelefono: '',
			inputEmail: '',
			inputDireccion: '',
			inputComuna: '',
			inputCiudad: '',
			nombre: '',
			rut: ''
		}
		let token = JSON.parse(sessionStorage.getItem('user'));
		fetch(conf.url.base + conf.endpoint.getCustomer, {
			method: 'post',
			body: JSON.stringify(request),
			headers: {
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + token.access_token
			}
		}).then(res => res.json())
			.catch(function (error) {
				console.log('Error:', error);
				console.log('ERROR=======>')
			})
			.then((response) => {
				console.log('HANDLE CLICL GETCUSTOMER:', response)
				if (response.errorCode === 0) {
					console.log("La ciudad que obtengo del server", response.ciudad)
					if (localStorage.getItem("rutCliente")) {
						if (response.nombre!=undefined){
						this.setState({
							inputRut: localStorage.getItem("rutCliente"),
							inputNombre: response.nombre,
							inputTelefono: response.telefono,
							inputEmail: response.email,
							inputDireccion: response.direccion,
							inputCiudad: response.ciudad,
							rut: response.rut,
							description: 'Completa los datos requeridos',
							clase: 'mostrarForm',
							tipoPago: localStorage.getItem("tipoPago"),
						})}

						const nose= {
							inputNombre: response.nombre,
							inputTelefono: response.telefono,
							inputEmail: response.email,
							inputDireccion: response.direccion,
							nombre: response.nombre,
							rut: response.rut,
							description: 'Completa los datos requeridos',
							clase: 'mostrarForm',
							tipoPago: localStorage.getItem("tipoPago"),
						}

						console.log("HANDLE CLICCK ESTADO",this.state)
					}
		
		
		
		
		
					// localStorage.setItem("nombreCliente", nose.inputNombre)
					// localStorage.setItem("rutC", nose.rut)
					// localStorage.setItem("tlf", response.telefono)
					// localStorage.setItem("direccion", nose.inputDireccion)
					// localStorage.setItem("email", nose.inputComuna)
					// localStorage.setItem("comuna", nose.comuna)
					// localStorage.setItem("ciudad", nose.inputCiudad)
					// localStorage.setItem("region", nose.inputRegion)

					localStorage.setItem("warning",response.errorMessage)
					console.log("COMUNAS====>",this.state.comunas)

					if((this.state.regiones) && (response.region!=undefined)){
					let regionCustomer=this.state.regiones.find(x => x.nombre.replace(/ /g, "") === response.region.replace(/ /g, ""))
					if(regionCustomer.id){
					this.setState({regionCustomer: regionCustomer.id})
					localStorage.setItem("regionCustomer",regionCustomer.id);
					localStorage.setItem("regionN",regionCustomer.nombre); }

				}
					if(response.region==undefined){
						if(localStorage.getItem("regionCustomer"))
						localStorage.removeItem("regionCustomer")

					}

					if((this.state.comunas) && (response.comuna!=undefined)){
						let comunaCustomer=this.state.comunas.find(x => x.nombre.replace(/ /g, "") === response.comuna.replace(/ /g, ""))
						console.log("las comunas filtradas=>",this.state.comunas)
						console.log("el nombre de comuna que trae el customer==>", response.comuna)
						console.log("comunaselected"+ JSON.stringify(comunaCustomer));
						if(comunaCustomer!=undefined){
						this.setState({comunaCustomer: comunaCustomer.id})
						localStorage.setItem("comunaCustomer",comunaCustomer.id);
						localStorage.setItem("comunaN",comunaCustomer.nombre);
					} 
						
					
					}
					if(response.comuna==undefined){
						if(localStorage.getItem("comunaCustomer"))
						localStorage.removeItem("comunaCustomer")

					}

					localStorage.setItem("ciudad",response.ciudad)
				} 
				
				if(response.errorCode===-1){
					MySwal.fire({

						text: "Atención",
						html:
							"<b>Atención</b><br><hr><br>" +
							'<h7>Su sesión ha expirado</h7>,<br>' +
							'<h7>Presiona aceptar para volver al inicio</h7> </div><br><br><hr>',
						showCancelButton: true,
						confirmButtonColor: '#fff',
						cancelButtonColor: '#3085d6',
						confirmButtonText: 'Cancelar',
						cancelButtonText: 'Aceptar'
					}).then((result) => {
						handleClickAllLogout()
						
					});
		
				}
				else {
					console.log('Error:', response);
					
					this.setState({ messageErr:"" })
					
				}
			})



		setTimeout(() => {
			// if (localStorage.getItem("rutCliente")) {
			// 	this.setState({
			// 		inputRut: localStorage.getItem("rutCliente")
			// 	})
			// }

			this.setState({ isChange:true   })



			// this.setState({
			// 	inputRut: localStorage.getItem("rutCliente"),
			// 	inputNombre: nose.inputNombre,
			// 	inputTelefono: nose.inputTelefono,
			// 	inputEmail: nose.inputEmail,
			// 	inputDireccion: nose.inputDireccion,
			// 	inputComuna: nose.inputComuna,
			// 	inputCiudad: nose.inputCiudad,
			// 	nombre: nose.nombre,
			// 	rut: nose.rut,
			// 	isChange: true,
			// 	description: 'Completa los datos requeridos',
			// 	clase: 'mostrarForm',
			// 	inputRegion: nose.inputRegion,
			// 	tipoPago: localStorage.getItem("tipoPago"),
			// })

			console.log("EL NOMBRE ACTUAL", this.state.inputNombre)
		}, 1000);
	}
	render() {
		const responseProducts = this.props.location.state;
		return (
			<div className="pad30">
			<div className="Card card clientProducto p-0 ">
				<div className="Card-HeaderP card-titleP white-text center-align ">
					<BreadCum></BreadCum>
				</div>
				<div className="Card-Main card-content pt9">
					<div id="step">2</div>
					<div className="Card-Main card-content contPad">

					<div className="Desktop">
						<MainCardLead
							title={this.state.description} spark={true}
						/></div>
						<div className="Phone">
						

						<MainCardLead
							title="Completa los Campos Requeridos" spark={true}
						/></div>
						
						{
							!this.state.isChange &&
							<div className="leftContainer">
								<div className="mt-4">

									<div className="col-lg-12">

										<div className="d-flex ">
											<div className="col-md-4 col-md-4 col-10">
												<div className="input-field ">
													<input type="text" name="rutSearch" id="rutSearch" label="RUT o pasaporte"
													 onBlur={() => this.handleBlur()} 
													 tabIndex="1" className=""
													 autocomplete="off"
													 onKeyPress={(e) => {

														console.log("toma el evento" + e.target.value)
														console.log("longitud",e.target.value.length)
														if(localStorage.getItem("regionCustomer")){
															localStorage.removeItem("regionCustomer");
														}
														if(localStorage.getItem("comunaCustomer")){
															localStorage.removeItem("comunaCustomer");
															}
														if(e.target.value.length<8){
															var number = document.getElementById("rutSearch").value;
															number += '';
															number = number.replace(".", "");
															let x = number.split('.');
															console.log("largo", number.length)
	
															let x1 = x[0];
															let x2 = x.length > 1 ? '.' + x[1] : '';
															var rgx = /(\d{1})(\d{3})(\d{3})/;
															while (rgx.test(x1)) {
	
																x1 = x1.replace(rgx, '$1' + '.' + '$2' + '.' + '$3') + "-";
	
															}
	
															if (number.length === 12) {
	
															}
	
															document.getElementById("rutSearch").value = x1 + x2;
														}
														else{
															
															console.log("entra en el 8")
														var number = document.getElementById("rutSearch").value.replace(".","").replace(".","").replace("-","");
														number += '';
														number = number.replace(".", "");
														let x = number.split('.');
														console.log("largo", number.length)

														let x1 = x[0];
														
														let x2 = x.length > 1 ? '.' + x[1] : '';
														var rgx = /(\d{2})(\d{3})(\d{3})/;
														while (rgx.test(x1)) {

															x1 = x1.replace(rgx, '$1' + '.' + '$2' + '.' + '$3') + "-";

														}

				

														if (number.length === 12) {

														}

														document.getElementById("rutSearch").value = x1 + x2;
													}	} } 


													onFocus={this.afterError}
													
													
													/>
													<label htmlFor="rutSearch">RUT</label>
												</div>
											</div>

											<div className="col-md-2 col-lg-2 col-2">
												<button type="button" name="busca" disabled={this.state.isErr} tabIndex="2" onClick={() => this.handleClick()} className="Raised waves-light btn  greenBtn">
													<i className="fal fa-search u-fz-16"></i></button>
											</div>
											<div className="col-lg-6 col-md-6"></div>
										</div>
									
										<div className="col-lg-6"></div>
										<div className="col-lg-4">
											<div className={this.state.claseError}><div>&nbsp;&nbsp; &nbsp;&nbsp;  { this.state.claseError ? <i class="fas fa-exclamation-triangle"></i> : <p></p> } {this.state.alertError}</div><div className="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {this.state.alertSuberror}</div></div>
										</div>
										<div className="col-lg-4 col-md-4">
											<div className="warning"><div></div><div><b>Recuerda:</b> 
											<p>Los datos solicitados a continuación es información requerida por SII para poder realizar devoluciones.</p> 
											</div></div>
										</div>
										
										<div className="col-lg-6"></div>
										{this.state.isChange &&
										<span className={this.state.clase}>
											<div className="d-flex mt-3">
												<div className="col-md-6 col-lg-6"></div>

												<div className="col-md-6 col-lg-6">
													<div className="input-field">
														<input type="text" name="nombre-ui" placeholder="" disabled />
														<label htmlFor="nombre-ui" className={this.state.classNombre}>Nombre</label>
													</div>
												</div>
											</div>
											{/* <div className="d-flex">

											<div className="col p-0">
												<div className="input-field">
													<input type="text" name="telefono-ui" placeholder="" disabled maxLength="70"/>
													<label htmlFor="telefono-ui" className="">Correo</label>
												</div>
											</div>
										</div> */}
											<div className="d-flex">
												<div className="col-md-6 col-md-6"></div>
												<div className="col-lg-6 col-md-6">
													<div className="input-field">
														<input type="text" name="correo-ui" placeholder="" disabled maxLength="100" />
														<label htmlFor="correo-ui" className="">Correo</label>
													</div>
												</div>
											</div>
											<div className="d-flex">
												<div className="col-md-6 col-md-6"></div>
												<div className="col-md-6 col-lg-6">
													<div className="input-field">
														<input type="text" name="nombre-ui" placeholder="" disabled maxLength="100" />
														<label htmlFor="nombre-ui" className="">Dirección</label>
													</div>
												</div>
											</div>
											<div className="col-lg-3 col-md-3 mt-4">
												<div className="input-field m-0 hideForm">
													<input type="text" name="nombre-ui" placeholder="" maxLength="50" />
													<label htmlFor="nombre-ui" className="">Comuna</label>
												</div>
											</div>
											<div className="col-lg-3 col-md-3">
												<div className="input-field m-0 hideForm">
													<input type="text" name="nombre-ui" placeholder="" disabled maxLength="100" />
													<label htmlFor="telefono-ui" className="">Ciudad </label>
												</div>
											</div>
											<div className="col-lg-6 col-md-6"></div>
											<div className="col-lg-5 col-md-6 hideForm"><TipoPago /></div>
											<div className="col-lg-8"></div>
											<div className="d-flex">
												<div className="col-lg-2">
													<span class="MuiIconButton-label">
														<input
															type="checkbox"
															id="politica"
															name="politica"
															label="Cliente autoriza la utilización de sus datos"
															className="ch"

														/></span>
												</div>
												<div className="col-md-1 col-lg-1 col-1">
												<i className="fas fa-question-circle fa-lg tooltippedInfo" data-position="top" ></i></div>
												<div className="col-md-5 col-lg-5"></div>


									</div></span> }
									</div> 
									{/* <div className="row justify-content-center mt-5">
										<div className="col-md-10 col-lg-10"></div>
										<div className="col-md-2 col-lg-2">
											<Raised isSubmit={true} disabled text='Finalizar' isIcon={false}></Raised>
										</div>
									</div> */}
								</div>


								<div className="row   sig1">

									<div className="col-md-2 col-lg-2 col-1">
										<button type="button" id="otro" name="button_1" text='Anterior' value="Anterior" onClick={() => this.anterior()} className="Raised btn waves-effect waves-light px-5 pt-2 pb-2 ">Anterior</button>
									</div>
									<div className="col-md-8 col-lg-8 col-0"></div>
									<div className="col-md-2 col-lg-2 col-1">
										<Raised name="otroboton" isSubmit={true} disabled text='Siguiente' isIcon={false}></Raised>
									</div>
								</div>


							</div>
						}

						{
							this.state.isChange &&
							<div className="row">
								<div className="col">
									<FormikApp {...this.state} data={responseProducts} {...this.props} openModalErr={this.openModalErr} />
								</div>
							</div>
						}
					</div>
					{
						this.state.isError ?
							<Modal size="small">
								<ModalHeader
									icon="fal fa-exclamation-square"
									title="Error"
									color="danger"
								/>
								<ModalMain>
									<h6>{this.state.errorMessage}</h6>
									<hr className="mt-5" />
								</ModalMain>
								<ModalFooter>
									<ModalButtonRaised name="Cerrar" />
								</ModalFooter>
							</Modal>
							: null
					}
				</div>

			</div></div>
		)
	}
}
