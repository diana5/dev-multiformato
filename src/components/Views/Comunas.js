import React from "react";
import { render } from "react-dom";
import PropTypes from "prop-types";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import conf from '../../config.json';
import InputLabel from '@material-ui/core/InputLabel';


class Comunas extends React.Component {

    constructor(props){
        super(props);
        this.state={
            comunas:[],
            regionSelected:[]
        }

    }
  static propTypes = {
    value: PropTypes.string.isRequired,
    index: PropTypes.number.isRequired,
    change: PropTypes.func.isRequired
  };

 componentDidMount=(props) =>{
  let {region} = this.props
  console.log("comunas",this.state.comunas)

  console.log("toodo",this.props)

  this.setState({
   comunas: localStorage.getItem("filtroComunas")
  })

  console.log("el estado en el select",this.state)

   let objRegion=JSON.parse(localStorage.getItem("regionObject"))
   if(objRegion!=null){
    const request = {
      apid: conf.access.apid,
      region: JSON.parse(localStorage.getItem("regionId"))
    }
    var comu=[]
    let token = JSON.parse(sessionStorage.getItem('user'));
    console.log("REQUEST COMUNA",request)
    setTimeout(() => {
    fetch(conf.url.base + conf.endpoint.getComunas, {
        method: 'post',
        body: JSON.stringify(request),
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token.access_token
        }
    }).then(res => res.json())
        .catch(function (error) {
            console.log('Error:', error);
        })
        .then(function (response) {
            console.log('Succes COMUNAS:', response);

            if (response != undefined) {
           }
            
            if (response === undefined) {
                //setSubmitting(false);
                return console.log('Log: ', { errorCode: 1, errorMessage: 'No existe respuesta' })
            }
  }) },1300)



console.log("EL ESTADP", this.state.comunas)
    var component = this; }

 console.log("las Comunas Render",this.props)
    }



  render() {
    
    const { value,id, handleComuna,defaultValue, className, filteredComunas} = this.props;

    console.log("value",{value})
    let x=[]
    if(localStorage.getItem("filtroComunas")!=null){
    x=JSON.parse(localStorage.getItem("filtroComunas").replace("/",""))
    console.log("lo que quiero",comunasFiltradas)
    }
   
   let comunasFiltradas={filteredComunas}

    if(comunasFiltradas===undefined){
      comunasFiltradas=[]
    }

    if (comunasFiltradas){
     comunasFiltradas= filteredComunas }

    return (
      <FormControl>
       <InputLabel id="demo-simple-select-label">Comuna</InputLabel>
        <Select
          value={value}
          style={{ fontSize: "inherit", width:"150px"}}
          className="col-md-12"
          onLoad={handleComuna}
          defaultValue={defaultValue}
          className={className}
          id={id}
          onClick={filteredComunas}
          
        >
         { comunasFiltradas?
          
          comunasFiltradas.map((comuna, index) => (
            <MenuItem key={index} value={comuna}>
              {comuna.nombre}
            </MenuItem>
          )) : <MenuItem></MenuItem>
         } 
        </Select>
      </FormControl>
      
    );
  }
}

export default Comunas;
