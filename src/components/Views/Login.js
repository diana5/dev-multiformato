import React, { PureComponent } from 'react';
// import M from 'materialize-css';
import { Form, Formik } from 'formik';
import InputField from '../Ui/Forms/InputField';
import Raised from '../Ui/Buttons/Raised';
import MainCardLead from '../Shared/Main/MainCardLead';
import { RadioButton } from '../Ui/Forms/RadioButton';
import './Login.css';
import { Fn } from '../../Utils';
import Loader from '../Loader';
import conf from '../../config.json';
import InputFieldAutocomplete from '../Ui/Forms/InputFieldAutocomplete';
import { LocalService } from '../../services/LocalService';
import { Eye } from '../../components/eye/Eye';
import InputMask from "react-input-mask";
import { MainTitleLead } from '../Shared/Main/MainTitleLead';
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";


var classN = "";
var classNR = "";
var classZ = "";



const FormLogin = (props) => (
	<Formik
		initialValues={{
			groups: '0',
			numLocal: '',
			userid: '',
			password: '',
			asd: conf.access.apid
		}}
		validate={values => {
			let errors = { numLocal: false, userid: false, password:false };
			// console.log(document.getElementById("userid").value)
		var x= parseInt(document.getElementById("userid").value);
		console.log("caracteres"+x);
			
		if (!values.password.toString().length) {
			errors.password = "El campo no puede superar los 20 caracteres";
			this.classZ = "usuario-error";
		}
		else {
			this.invalid = false;
			this.classZ = "";
		}
			if (values.numLocal.toString().length < 1) {
				errors.numLocal = "Dato Incorrecto/Campo Incompleto";
				this.invalid = true;
				this.classNR = "usuario-error";


			}
		
			else if (values.numLocal.toString().length > 5) {
				errors.numLocal = "Dato Incorrecto/Campo Incompleto";
				this.invalid = true;
				this.classNR = "usuario-error";

			} else {
				this.invalid = false;
				this.classNR = "";
			}

			if (!document.getElementById("userid").value) {
				errors.userid = "Usuario no encontrado.Sólo números mas digito Verificador(n-o-k)";
				this.invalid = true;
				//document.getElementsById("userId").classList.add= "usuarioError";
				this.classN = "usuario-error";

			}else {
				this.classN = "";

			}

			// if (!Fn.validaRut(document.getElementById("userid").value)){
			// 	errors.userid = "Usuario no encontrado.Sólo números mas digito Verificador(n-o-k)";
			// 	this.invalid = true;
			// 	//document.getElementsById("userId").classList.add= "usuarioError";
			// 	this.classN = "usuario-error";
			// } else{				
			// 	this.classN = "";
			// }

			// if (x < 12) {
			// 	errors.userid = "Usuario no encontrado.Sólo números mas digito Verificador";
			// 	this.classN = "usuario-error";

			// }
			// if (x > 12) {
			// 	this.classN = "";

			// }

			if (!document.getElementById("userid").value) {
				errors.userid = "Dato incorrecto";
				this.invalid = true;
				//document.getElementsById("userId").classList.add= "usuarioError";
				this.classN = "usuario-error";

			}
			else if (!Fn.validaRut(document.getElementById("userid"))) {
				errors.userid = "No es un rut válido";
				this.invalid = true;
				this.classN = "usuario-error"
			}else if (values.userid.toString().length < 10) {
			this.classN = "usuario-error"}
			else {
				this.classN="";
			};

			
		

			if (values.password.length===0) {
				errors.password = "El campo no puede superar los 20 caracteres";
				this.classZ = "usuario-error";
			}
			else {
				this.invalid = false;
				this.classZ = "";
			}

			if (!document.getElementById("password").value) {
				this.classZ= "usuario-error";
				errors.password = "Usuario y/o contraseña inválida";
				this.invalid = true;
				
			} else {
				this.invalid = false;
				this.classZ= "";
			}


			return errors;
		}}
		onSubmit={(values, { setSubmitting, setErrors }) => {
			
			// console.log("hola " + document.getElementById("userid").value)
			localStorage.setItem("Localini",values.numLocal)
			const request = {
				numLocal: values.numLocal,
				userid: Fn.separator(document.getElementById("userid").value),
				password: values.password,
				usermode: values.groups,
				apid: values.asd
			}
			// console.log(request)
			setTimeout(() => {

				fetch(conf.url.base + conf.endpoint.authUser, {
					method: 'post',
					body: JSON.stringify(request),
					headers: {
						'Content-Type': 'application/json'
					}
				}).then(res => res.json())
					.catch(function (error) {
						console.log('Error:', error);
					})
					.then(function (response) {
						// console.log('Succes Respuesta:', response);
						if (response === undefined) {
							setSubmitting(false);
							return console.log('Log: ', { errorCode: 1, errorMessage: 'No existe respuesta' })
						}

						if (response.errorCode === 0 && !response.errorMessage) {
							if (response.access_token !== null) {
								console.log("USUARIO RESPUESTA---->"+ JSON.stringify(response))
								setSubmitting(false);
								const { perfil } = response;
								const user = {
									access_token: response.access_token,
									expires_in: response.expires_in,
									num_local: values.numLocal,
									perfil: perfil,
									nombre: response.nombre,
									user: response.userid
								}
								const paths = { 1: '/boleta', 2: '/reportlocal', 3: '/reportcentral',4: '/localesreport' }
								switch (perfil) {
									case 1:
										if (values.groups.toString() === "0") {
											sessionStorage.setItem("user", JSON.stringify(user));
											props.setStateLogin();
											props.history.push(paths[1]);


										} else {
											let info = values.groups === "0" ? "Nota Crédito" : "Reportes";
											setErrors({ password: "El usuario ingresado no tiene permisos de acceso a " + info })
											// console.log('Log: ', { errorCode: 2, errorMessage: 'Ups ! El usuario no ingreso correctamente el usermode' })

										}
										break;

									case 2:
										if (values.groups.toString() === "1") {
											sessionStorage.setItem("user", JSON.stringify(user));
											props.setStateLogin();
											props.history.push(paths[2]);


										} else {
											let info = values.groups === "0" ? "Nota Crédito" : "Reportes";
											setErrors({ password: "El usuario ingresado no tiene permisos de acceso a " + info })
											console.log('Log: ', { errorCode: 2, errorMessage: 'Ups ! El usuario no ingreso correctamente el usermode' })

										}
										break;

										
									case 4:
										if (values.groups.toString() === "1") {
											sessionStorage.setItem("user", JSON.stringify(user));
											props.setStateLogin();
											props.history.push(paths[4]);


										} else {
											let info = values.groups === "0" ? "Nota Crédito" : "Reportes";
											setErrors({ password: "El usuario ingresado no tiene permisos de acceso a " + info })
											console.log('Log: ', { errorCode: 2, errorMessage: 'Ups ! El usuario no ingreso correctamente el usermode' })

										}
										break;

									case 3:
										if (values.groups.toString() === "1") {
											sessionStorage.setItem("user", JSON.stringify(user));
											props.setStateLogin();
											props.history.push(paths[3]);


										} else {
											let info = values.groups === "0" ? "Nota Crédito" : "Reportes";
											setErrors({ password: "El usuario ingresado no tiene permisos de acceso a " + info })
											console.log('Log: ', { errorCode: 2, errorMessage: 'Ups ! El usuario no ingreso correctamente el usermode' })

										}
										break;

									default:
										break;
								}
							}
						} 
						
						else {
							console.log('Error:', response);
							setSubmitting(false);

							// RESPONSE ERRORS SERVICE
							//
							//  0 => sin error
							//  1 => Usuario o clave incorrectos
							//  2 => usuario centralizado quizo hacer NC
							//  3 => usuario local quizo ingresar a reporteria
							// -1 => token de autenticación invalido
							// -2 => local no habilitado para DMF
							// -3 => servicio no disponible en local
							// -4 => error de formato de parametros
							// -5 => error interno SQL
							// -6 => apid invalido

							// switch (response.errorCode) {
							switch (response.errorCode) {
								// case 1:
								// 	setErrors({ password: response.errorMessage })
								// 	break;
								// case 2:
								// 	setErrors({ password: response.errorMessage })
								// 	break;
								// case 3:
								// 	setErrors({ password: response.errorMessage })
								// 	break;
								default:
									setErrors({ password: response.errorMessage })
									break;
							}
						}
					})
			}, 1000);
		}}


		render={props => (
			
			<Form>
				<div class="container">
					<div className="row justify-content-center">
						<div className="col">
							<div>
								<div className="row">
									<div className="col-sm-8">
										<InputField
											component={RadioButton}
											type="radio"
											name="groups"
											value="0"
											text="Nota Crédito"
											check={true}
											onChange={props.handleChange}
											onBlur={props.handleBlur}
											checked={props.values.groups === '0'}

										/>
									</div>
									<div className="col-sm-2 ">
										<div class="reporeR">
											<InputField
												class="reporteR"
												component={RadioButton}
												type="radio"
												name="groups"
												value="1"
												text="Reportes"
												onChange={props.handleChange}
												onBlur={props.handleBlur}
												checked={props.values.groups === '1'}

											/></div>
									</div></div>
							
								<InputMask 
									id="userid"
									type="text"
									name="userid"
									placeholder="ID Colaborador"
									autoComplete="username"
									ishelptext="true"
									help="Solo numeros y digiro verificador con n ó k"
									label="RUT Colaborador"
									onChange="handleUserChange()"
									style={{marginBottom:'8px'}}

								/>
								<div class="helper"></div>

								<InputFieldAutocomplete {...props} />

								<Eye {...props} id="password" propiedad="usuario-error"/>
							</div>
						</div>
					</div></div>
				<div className="row justify-content-center ingresar">
					<div className="col-auto subir">
						<Raised isSubmit={true}  text={props.isSubmitting === true ? <Loader /> : 'Ingresar'} isIcon={false} ></Raised>
					</div>
				</div>
			</Form>
		)}
	/>
)

export class Login extends PureComponent {

	constructor() {
		super();
		this.state = {
			classN:"",
			classNR:"",
			classZ:"",
			isError: false,
			errorMessage: '',
			className: "usuario",
			useridError: false,
			passwordError: false,
			numLocalError: false,
			borderBottom: '',
		}
		var x=[]
	


	}

	displayName = Login.name

	state = {
		classN:"",
		clasNR:"",
		classZ:"",
		isError: false,
		errorMessage: '',
		className: "usuario",
		useridError: false,
		passwordError: false,
		numLocalError: false,
		borderBottom: ''
	}
	// openModalErr = (message) => {
	// 	this.setState({
	// 		isError: true,
	// 		errorMessage: message
	// 	})
	// 	var elem = document.querySelector('.modal');
	// 	var instance = M.Modal.init(elem);
	// 	instance.open();
	// }

	handleNData = (e) => {
		this.setState({classN: e})
		  }

	handlePasswordChange = evt => {
		this.setState({ password: evt.target.value });
	};

	handleUserChange = ()=> {
	//	this.setState({ userid: evt.target.value });
		


	};

	handlePassChange = () =>{
		var y=parseInt(document.getElementById("password").value);
		console.log(y)
		if((y)<1){
		
		this.classNM="usuario-error"; }
	
	}

	setWidth = () => {
		this.props.setWidth("col-sm-8 col-md-5 col-lg-5 login")

	}
	setHeight = () => {
		this.props.setHeight("100px")

	}

	setBorderBottom = () =>{
		this.props.setBorderBottom("usuario-error")
	}
	componentDidMount() {
		window.onbeforeunload = null;

		this.setWidth()
		LocalService();

		var y=parseInt(document.getElementById("password").value);
		console.log(y)
		if((y)<1){
		
		this.classNM="usuario-error"; 
		this.setBorderBottom();
	}
	
	
	}

	render() {  

	
		return (
			<div className="IniciarS" className="Card card cardlogin p-0 " style={{maxHeight:'460px'}}>
				<div className="Card-Header white-text p-4 center-align init " style={{fontSize:'20px'}}>
					<span style={{fontSize:'20px'}}><i className="fas fa-user-circle"></i> Iniciar Sesión</span>
				</div>
				<div className="login-title">Plataforma para realizar Nota de Crédito (VERSION 1-09-2020- 4 pM).</div>
				<div className=" p-5 pt-7" style={{marginTop:'-45px', height:'20px'}}>
					<MainCardLead title="Selecciona e ingresa los datos" size="u-fz-1_5" spark={true} />
					<br></br>
					<FormLogin {...this.props} openModalErr={this.openModalErr} />
				</div>
				{/* <div className="Card-Footer card-action p-0 pt-4">
					<span></span>
				</div> */}
				{/* {
					this.state.isError
					?	<Fragment>
							<Modal size="small">
								<ModalHeader title="Error" />
								<ModalMain message={this.state.errorMessage} />
								<ModalFooter />
							</Modal>
						</Fragment>
					:	null
				} */}
			</div>
		)
	}
}
Login.propTypes = {
	classes: PropTypes.object.isRequired,
	id: PropTypes.string.isRequired,
	label: PropTypes.string.isRequired
};

export default withStyles()(Login);