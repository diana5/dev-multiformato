import React, { PureComponent } from 'react'
import ReactToPrint from "react-to-print"
import M from 'materialize-css';
import './Note.css'
import { NoteService,NoteServiceProducts } from '../../services/NoteService';
import MainCardLead from '../Shared/Main/MainCardLead';
import conf from '../../config.json';
import { Checkbox } from '@material-ui/core';
import { withRouter } from 'react-router-dom';
import clsx from 'clsx';
import Swal from 'sweetalert2';
import { makeStyles } from '@material-ui/core/styles';

import withReactContent from 'sweetalert2-react-content'
import SweetAlert from 'react-bootstrap-sweetalert';

const MySwal = withReactContent(Swal)
const MySwal5 = withReactContent(Swal)
const MySwalError = withReactContent(Swal)

const useStyles = makeStyles({
	root: {
	  '&:hover': {
		backgroundColor: 'transparent',
	  },
	},
	icon: {
	  borderRadius: 3,
	  width: 16,
	  height: 16,
	  boxShadow: 'inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)',
	  backgroundColor: '#rgba(0, 0, 0, 0.26)',
	  backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))',
	  '$root.Mui-focusVisible &': {
		outline: '2px auto rgba(19,124,189,.6)',
		outlineOffset: 2,
	  },
	  'input:hover ~ &': {
		backgroundColor: '#ebf1f5',
	  },
	  'input:disabled ~ &': {
		boxShadow: 'none',
		borderRadius: '2px',
		marginLeft:'3px'
	  },
	},
	checkedIcon: {
	  backgroundColor: '#137cbd',
	  backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))',
	  '&:before': {
		display: 'block',
		width: 16,
		height: 16,
		backgroundImage:
		  "url(\"data:image/svg+xml;charset=utf-8,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 16 16'%3E%3Cpath" +
		  " fill-rule='evenodd' clip-rule='evenodd' d='M12 5c-.28 0-.53.11-.71.29L7 9.59l-2.29-2.3a1.003 " +
		  "1.003 0 00-1.42 1.42l3 3c.18.18.43.29.71.29s.53-.11.71-.29l5-5A1.003 1.003 0 0012 5z' fill='%23fff'/%3E%3C/svg%3E\")",
		content: '""',
	  },
	  'input:hover ~ &': {
		backgroundColor: '#106ba3',
	  },
	},
  });
  function StyledCheckbox(props) {
	const classes = useStyles();
  
	return (
	  <Checkbox
		className={classes.root}
		
		color=""
		checkedIcon={<span className={clsx(classes.icon, classes.checkedIcon)} />}
		icon={<span className={classes.icon} />}
		inputProps={{ 'aria-label': 'decorative checkbox' }}
		{...props}
	  />
	);
  }

class ComponentToPrint extends React.Component {
	render() {
		return (
			<div className="Voucher center p-3">
				<h6 className="m-0">DEVOLUCIÓN DE O MULTIFORMATO</h6>
				<p className="m-0">NO APTO PARA LA VENTA</p>
				<h6 className="m-0">DESECHAR SIN REGISTRAR EN SMART</h6>
			</div>
		)
	}
}

export class Note extends PureComponent  {
	displayName = Note.name

	state = {
		isClick: true,
		ticket: false,
		nc:false,
		op:null
	
	}
	constructor(props){
		super(props)
		console.log(localStorage.getItem("folio"))
		console.log(localStorage.getItem("hoy"))
		
	
	}

	setWidth = () => {
		this.props.setWidth("col-4 col-sm-5 col-md-5 col-lg-5 col-12 leftPrint ")
	}





	componentDidMount() {
		this.getOptionNc()
		
		console.log("las props",this.props)
		console.log("Fecha"+localStorage.getItem("hoy"))
		this.setWidth()
		let tooltip = document.querySelectorAll('.tooltipped');
		M.Tooltip.init(tooltip);
		localStorage.removeItem("marcaResumen")

		function confirmExit() {
			return "Ha intentado salir de esta pagina. Si ha realizado algun cambio en los campos sin hacer clic en el boton Guardar, los cambios se perderan. Seguro que desea salir de esta pagina? ";
		}
	}


		  componentWillUnmount() {
		}

	getOptionNc= () =>{
		const search = this.props.location.search; // could be '?foo=bar'
		const params = new URLSearchParams(search);
		let op = params.get('n'); 
		console.log("lasop",op)
		if (op==="1"){
		console.log("esta entrando")
		this.setState({ op:1})}

		else {this.setState({ op:2 })}
	
		

		console.log("state",this.state)

	}

	// onUnload = e => { // the method that will be used for both add and remove event
	// 	e.preventDefault();
	// 	e.returnValue = 'Ok';
	//  }

	goBeginning =() =>{
		this.props.history.push({
			pathname: "/boleta"
		})
		// swal.close()

	}

	handleClickLogout = () => {
		console.log("entra")
		sessionStorage.removeItem('user');
		this.props.history.push({
			pathname: "/"
		})
		// swal.close()
	}
	handleClickNewNote = () => {
		this.props.history.push({
			// pathname: "/boleta"
		})
	}
	handleClickPrintVoucher = () => {
		this.setState({
			isClick: false
		})
	}
	handleClickPrintNc = (a, b, c, d, e, f, g, op) => {
		// swal({
		
		// 	content: (
		// 		<div>
						
		// 			  <b>¿Desea abrir ventana emergente con el documento a imprimir?</b><br></br>
		// 			 </div>),
		// 	buttons: true,
		// 	dangerMode: true,
		// 	icon: "warning",

		// 	buttons: ["Cancelar", "Abrir"],
		//   })
		//   .then((willDelete) => {
		// 	if (willDelete) {
		// 		swal("Ahora puedes ver la nueva ventana a la derecha");
		// 		console.log("lanc:"+d, "el ticket="+g)
		// 		let ticket=0
		// 		let nc=0
		
		// 		if(g===true){
		// 		 ticket=1
		// 		}else{
		// 			ticket=0
		// 		}
		
		// 		if(d===true){
		// 			nc=1
		// 		   }else{
		// 			   nc=0
		// 		   }
		// 		   NoteServiceProducts(a, b, c, nc, e, f, ticket);
			  
		// 	} else {
			  
		// 	}
		//   });
		MySwal5.fire({

			text: "Atención",
			html:
				"<b>Atención</b><br><hr><br>" +
				'<h7> ¿Desea abrir ventana emergente con el documento a imprimir?</h7><br>',
			showCancelButton: true,
			confirmButtonColor: '#fff',
			cancelButtonColor: '#3085d6',
			confirmButtonText: 'Abrir',
			cancelButtonText: 'Cancelar'
		}).then((result) => {
			if (result.value) {
				// swal("Ahora puedes ver la nueva ventana a la derecha");
					
						//    ncCliente: d,
						//    ncTienda: e,
						//    voucher: f,
							let ticketMF= g
						   if(this.state.op==2){
						   NoteServiceProducts(a, b, c, 1, 1, 1, 1); }

						   else{
						   NoteServiceProducts(a, b, c, 1, 1, 1, g);}



			} 
			else {			window.location.reload()

			}
		});

	}

	handleInputChangeNC = (e) =>{
		console.log("la nota de c",e.target.checked)
		this.setState({nc: e.target.checked})


	}
	
	handleInputChangeTicket = (e) =>{
		console.log("el ticket",e.target.checked)
		this.setState({ticket: e.target.checked})

	}
	start =() => {
		// window.location="/dmf/boleta"
		let btn="cerrarS"

		MySwal5.fire({

			text: "Atención",
			html:
				"<b>Atención</b><br><hr><br>" +
				'<h7> Recuerda que al presionar <b>"Finalizar"</b>, seguirás conectado a la plataforma</h7><br>' +
				'<h7>para realizar devoluciones, De lo contrario, debes <b>"Cerrar Sesión"</b>', 
			showCancelButton: true,
			confirmButtonColor: '#fff',
			cancelButtonColor: '#3085d6',
			confirmButtonText: 'Finalizar',
			cancelButtonText: 'Cerrar Sesión',
			footer: '<button class="cerrarModal" onclick="window.location.reload(); return false;">x</button>'

		}).then((result) => {
			if (result.value) {
			this.goBeginning()


			} 
			else {			this.handleClickLogout()


			
		

			}
		});

	
	}

	//obtener el valor de la url


	render() {

				// const { date, folio, numLocal } = this.props.location.state;
		const date =localStorage.getItem("hoy").toString()
		const folio = localStorage.getItem("folio")
		const numLocal = localStorage.getItem("numLocal")

		const correo = localStorage.getItem("email")
		console.log("fecha "+date+"  folio: "+folio+" local "+numLocal)

		return (
			<div style={{marginTop:'-20px'}}>
			<div className="Card card p-0 ">
				<div className="Card-Header card-title white-text p-4 center-align">
					<span><i className="fal fa-print mr-3"></i>Imprimir y/o Enviar vía E-mail</span>
				</div>
				<div className="Note Card-Main card-content position-relative  p-4 px-5 ">
					<div className="col-lg-12"><MainCardLead title="Documentos" spark={true} />
					</div>
					<div className="col-lg-12"><br></br></div>

					<div className="row">
							<div className="col-lg-6 col-6 col-md-6 "><b className="tienda"> Tienda</b> (Impresión obligatoria):</div>
							<div className="col-lg-6 col-6 col-md-6"><b className="cliente"> Cliente:</b></div>

							<div className="col-md-1 col-1 col-lg-1"><Checkbox checked disabled color="default" defaultChecked></Checkbox></div>
							<div className="col-md-4 col-4 col-lg-5 col-sm-3"><p className="check">Nota de Crédito</p></div>
							<div className="col-md-1 col-1 col-lg-1 ">
								{this.state.op===2 ?
								<Checkbox checked disabled  defaultChecked   color="secondary" id="nc" name="nc" onChange={(e)=>this.handleInputChangeNC(e)}></Checkbox>
								: <Checkbox  defaultChecked color="secondary" id="nc" name="nc" onChange={(e)=>this.handleInputChangeNC(e)}></Checkbox>}

							</div>
							<div className="col-md-5 col-5 col-lg-5 col-sm-5"><p className="check">Nota de Crédito</p></div>
							<div className="col-md-1 col-1 col-lg-1 col-sm-1"><Checkbox checked disabled color="default"  defaultChecked></Checkbox></div>
							<div className="col-md-4 col-4 col-lg-5 col-sm-3"><p className="check">Voucher</p></div>
							<div className="col-md-1 col-1 col-lg-1 col-sm-1">

							{this.state.op===1?
							<StyledCheckbox disabled />

							:<Checkbox  checked disabled  defaultChecked color="secondary" id="tk" name="tk" onChange={(e)=>this.handleInputChangeTicket(e)}></Checkbox>
							}</div>
							<div className="col-md-5 col-4"><p className="check">Ticket Multiformato</p></div>
							<br></br>
							<div className="col-lg-12 col-12">
						
								<button className="ImprimirButton" href="#"  onClick={() => this.handleClickPrintNc(numLocal, date, folio, this.state.nc, 1,1,this.state.ticket,this.state.op)}><i className="blueIcon fal fa-print "></i> &nbsp;&nbsp; <div className="print">Imprimir documentos seleccionados</div></button>
							</div>
							{/* <div className="col-lg-12 col-12">
							<div className="blueSquare">
								<a className="" href="#"  onClick={() => this.handleClickPrintNc(numLocal, date, folio, this.state.op)}>
							   <i class="blueIcon far fa-envelope"></i>
							   &nbsp;&nbsp; <div className="print"> Enviar a email del cliente los documentos seleccionados</div></a>
							</div>
							
							</div> */}
							

						{ localStorage.getItem("warning") ? 
						<div className="col-lg-12 col-md-12 col-12">

							<div className="bluesquare-Recuerda">
								<b>Recuerda</b><br></br>
								{localStorage.getItem("warning")}
						</div></div>
						: <p></p>} 

					</div>						
				</div>	
				
				<div className="row">
				<div className="col-lg-8 col-md-8 col-8"></div>
				<div className="col-lg-3 col-md-1  col-3 col-sm-3">
							<div className="px-2">
								<br></br>
								<button className="Raised btn waves-effect waves-light px-5  pt-2 pb-2 finalizar" onClick={this.start}>Finalizar</button></div>
				</div></div>
				
				{/* <div className="Card-FooterX">
				
				<span></span>
				</div> */}
		
			</div></div>
			
		)
	}
}
export default withRouter(Note)