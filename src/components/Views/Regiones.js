import React from "react";
import { render } from "react-dom";
import PropTypes from "prop-types";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import conf from '../../config.json';
import InputLabel from '@material-ui/core/InputLabel';
import PDF, { Text, AddPage, Table, Html } from 'jspdf-react';
import jsPDF from 'jspdf'
import { FormHelperText } from '@material-ui/core';

class Regiones extends React.Component {

    constructor(props){
        super(props);
        this.state={
            regiones:[],
            comunas:[],
            regionSelected:0,
            selected: null,
            hasError: false

        }

        this.getComunas=this.getComunas.bind(this)

    }
  static propTypes = {
    value: PropTypes.string.isRequired,
    index: PropTypes.number.isRequired,
    change: PropTypes.func.isRequired
  };



  
  logChange = (event) => {
     console.log(event.target.value)
     localStorage.setItem("regiones", event.target.value.nombre)
     localStorage.setItem("regionId", JSON.stringify(event.target.value.id))
 }

 
getComunas = (e) =>{

  let regi= document.querySelector("#reg").textContent
  console.log("RG",regi)
  localStorage.setItem("regionId",e.target.value)
  console.log("entra al rq",e.target.value)
  let comunasF=[]
  let token = JSON.parse(sessionStorage.getItem('user'));
  const rq={
    apid: conf.access.apid,
    region: e.target.value

  }
  console.log("Request GETCOMUNAS",rq)
  fetch(conf.url.base + conf.endpoint.getComunas, {
    method: 'post',
    body: JSON.stringify(rq),
    headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + token.access_token
    }
}).then(res => res.json())
    .catch(function (error) {
        console.log('Error:', error);
    })
    .then(function (response) {
        console.log('COMUNASSSS:', response);

        if (response != undefined) {
          component.setState({
            comunas: response.comunas
          }) 
        }
        
        if (response === undefined) {
            //setSubmitting(false);
            return console.log('Log: ', { errorCode: 1, errorMessage: 'No existe respuesta' })
        }

} ) 
console.log("LAS COMUNASF EN EL",this.state.comunas)

var component=this


}

setComuna = (e) =>{
  localStorage.setItem("comunaId",e.target.value)
  console.log("Examp", localStorage.getItem("comunaId"))
  let comunaObj= this.state.comunas.find(x => x.id === e.target.value)
   console.log("OBJETO COMUNA",comunaObj)
  // console.log("comu",comu)
  localStorage.setItem("comunaN",comunaObj.nombre)
  localStorage.setItem("regionN",comunaObj.nombreRegion)

}

componentDidMount=() =>{
  const request = {
    apid: conf.access.apid
  }
  var comu=[]
  let token = JSON.parse(sessionStorage.getItem('user'));
  setTimeout(() => {
  fetch(conf.url.base + conf.endpoint.getRegiones, {
      method: 'post',
      body: JSON.stringify(request),
      headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' + token.access_token
      }
  }).then(res => res.json())
      .catch(function (error) {
          console.log('Error:', error);
      })
      .then((response)=> {
          console.log('REGIONESSSSSSSSS:', response);

          if (response != undefined) {
          this.setState({
              regiones: response.regiones
          }) 
        
        
        }
          
          if (response === undefined) {
              //setSubmitting(false);
              return console.log('Log: ', { errorCode: 1, errorMessage: 'No existe respuesta' })
          }
}) },1000);

console.log("Request GETCOMUNAS",request)
fetch(conf.url.base + conf.endpoint.getComunas, {
  method: 'post',
  body: JSON.stringify(request),
  headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token.access_token
  }
}).then(res => res.json())
  .catch(function (error) {
      console.log('Error:', error);
  })
  .then((response) => {
      console.log('COMUNASSSS:', response);

      if (response != undefined) {
        this.setState({
          comunas: response.comunas
        }) 
      }
      
      if (response === undefined) {
          //setSubmitting(false);
          return console.log('Log: ', { errorCode: 1, errorMessage: 'No existe respuesta' })
      }

} ) 

}



  render() {
    console.log(this.props)
    const { value, index, field,id, handleRegiones,name,indice,className,regionSelected,comunaSelected} = this.props;
    const { selected, hasError } = this.state;
  
   
    return (
      
        [
        <div className="r" >
        <FormControl  className="col-lg-5 b" error={hasError}
          >
       <InputLabel id="demo-simple-select-label">Región*</InputLabel>

        <Select
          value={value}
          style={{ fontSize: "inherit", width:"100%"}}
          className="col-md-12"
          className={className}
          id={id}
          onChange={(e)=> this.getComunas(e)}
          error={hasError}
          defaultValue={regionSelected}
          required
        >
        
          {
          this.state.regiones.map((region, index) => (
            <MenuItem  key={id} value={region.id} name={region.name}>
            <div id="reg"> {region.nombre }</div> 
            </MenuItem>
          ))}
        </Select>
        {hasError && <FormHelperText>This is required!</FormHelperText>}
      </FormControl>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<FormControl className="col-lg-6 b" error={hasError}>
     <InputLabel id="demo-simple-select-label">Comuna*</InputLabel>

       <Select
         value={value}
         style={{ fontSize: "inherit", width:"100%"}}
         className="col-md-12"
        //  onLoad={handleComuna}
         className={className}
         id="com"
         onChange={(e)=> this.setComuna(e)}
         defaultValue={comunaSelected}
         required>
       {
         
         this.state.comunas.map((comuna, index) => (
           <MenuItem  key={id} value={comuna.id}>
             {comuna.nombre}
           </MenuItem>
         )) 
        } 
        
       </Select>
       {hasError && <FormHelperText>This is required!</FormHelperText>}
     </FormControl></div>]

    );
  }
}

export default Regiones;
