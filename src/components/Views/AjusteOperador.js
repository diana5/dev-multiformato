import React, { PureComponent, Fragment } from 'react';
import { Form, Formik, ErrorMessage } from 'formik';
import InputField from '../Ui/Forms/InputField';
import MainCardLead from '../Shared/Main/MainCardLead';
import MainNotaReport from '../Shared/Main/MainNotaReport';
import { Fn } from '../../Utils';
import Raised from '../Ui/Buttons/Raised';
import Loader from '../Loader';
import Modal from '../Shared/Modal/Modal';
import ModalHeader from '../Shared/Modal/ModalHeader';
import ModalMain from '../Shared/Modal/ModalMain';
import ModalFooter from '../Shared/Modal/ModalFooter';
import ModalButtonRaised from '../Shared/Modal/ModalButtonRaised';
import M from 'materialize-css';
import DateFnsUtils from "@date-io/date-fns";
import esLocale from 'date-fns/locale/es';
import './ReportAll.css';
import { NoteService } from '../../services/NoteService';
import { TicketService } from '../../services/TicketService';
import PDF_NC from  '../Ui/Pdf/PDF_NC';
import PDF_Detalle from  '../Ui/Pdf/PDF_Detalle';
import {Excel,ExcelYup} from  '../Ui/Excel/Excel';
import { Func } from '../../func/func';
import conf from '../../config.json';
import "rc-tabs/assets/index.css";
import Tabs, { TabPane } from "rc-tabs";
import TabContent from "rc-tabs/lib/SwipeableTabContent";
import ScrollableInkTabBar from "rc-tabs/lib/ScrollableInkTabBar";
import jsPDF from 'jspdf'
import 'jspdf-autotable'
 

const defaultTabKey = "1";

const PanelContent = ({ children }) => (
  <div style={{ height: 90, overflowY: "hidden", overflowX:"hidden", border:"0px" }}>{children}</div>
);


const DateFormat = value => {
    let formatDay = null
    let formatMonth = null
    let date = new Date(value);
    let year = date.getUTCFullYear()
    let day = date.getDate()
    let month = (date.getMonth()) + 1
    if (day <= 9)
        formatDay = '0' + day.toString()

    if (month <= 9)
        formatMonth = '0' + month.toString()

    const fullDate = (year.toString()) + (formatMonth === null ? month.toString() : formatMonth.toString()) + (formatDay === null ? day.toString() : formatDay.toString())
    return fullDate
}


const THead3 = () => (
	<thead>
		<tr>
			<th>Local</th>
			<th>Op</th>
			<th>Nombre y Apellido
                </th>
			<th>Fecha Dif</th>
			<th>Monto Dif</th>
			<th>Tipo</th>
			<th>Motivo de Ajuste</th>
			
			<th>Rut Personal</th>
			<th>Fecha Dif</th>
			<th>Fecha Dif
                </th>
			<th>Codigo Ajuste</th>
			<th>Valor Ajuste</th>
			<th>Moneda</th>
			<th>Fecha Dif</th>
			<th>Dif Ajustada Final</th>
			
		</tr>
	</thead>
)
const FillTableAjuste = (props) => (
	<Fragment>
		<div className="row mb-0 w">
			<div className="col-12 col-md-12">
			</div>
			{/* <div className="col-auto">
				<ul className="tabs z-depth-1">
					<li className="tab"><a href="#one">Local {props.numLocalState}</a></li>
					<li className="tab"><a href="#two">Desde otros locales</a></li>
				</ul>
			</div> */}
		</div>
		<div className="row">
			<div className="col-lg-12 col-md-12">
			{
				props.inCheck &&
				<div id="one">
					
					<div className="row">
					<div className="col-lg-5"><h6 className="u-fw-500 topBoleta"><p className="ncLeft">Boleta</p></h6></div>
					<div className="col-lg-5"><h6 className="u-fw-500"><p className="ncRight">Nota de Crédito</p>{console.log("LOC",props)}</h6></div>
					<div className="col-lg-2"><input className="form-control  buscadorReporte fontAwesome " placeholder="&#xF002; Buscar"  /></div>
					{/* <div className="col-lg-3"><input className="form-control buscador fontAwesome " placeholder="&#xF002; Buscar Producto" value={this.state.text} onChange={(text) => this.filter(text, products)} /></div> */}


				</div>
					<table className="Product-table Local responsive-table striped" id="myTable">
				
							<THead3/>
							
							<tbody>
							<div className="u-scrollX-auto u-Maxheight-226">

								{ 
									props.ncLocal 										?
										props.ncLocal.map((itemm,index)=> {
											return (
												<tr key={index}>
													<td>{itemm.originalTransaction.formato}</td>
													<td>{itemm.originalTransaction.mercado}</td>
													<td>{itemm.originalTransaction.numLocal}</td>
													<td>{Fn.formateaRut(itemm.customerId)}</td>
													<td>{itemm.originalTransaction.folio}</td>
													<td>{Fn.formatDate(itemm.originalTransaction.date)}</td>
													<td className="line">
														{
															itemm.idFile
															? <a className=" fileBoletaEn" onClick={() => props.handleViewTicket(itemm.idFile)}><i class="far fa-paperclip"></i></a>
															: <a className="fileBoleta-disabled" ><i class="far fa-paperclip"></i></a>
														}
													</td>
													<td>{itemm.formato}</td>
													<td>{itemm.mercado}</td>
													<td>{itemm.loginLocal}</td>
													<td>{itemm.originalTransaction.opr}</td>
													<td>{itemm.folio}</td>
									
													<td>{Fn.formatDate(itemm.date)}</td>
													<td>$ {new Intl.NumberFormat().format(itemm.total)}</td>
													<td><a href="#modalReport" className="modal-trigger" onClick={() => props.handleClickGetCreditNoteData(itemm, "emisor")}><i className="fal fa-list-alt u-text-green"></i></a></td>
												</tr>
											)
										})
										: <tr><td colSpan="100" className="red-text center-align p-2">NO EXISTEN DATOS</td></tr>
								}
													</div>

							</tbody>
						</table>
				</div>
			}
			{
				props.otherCheck &&
				<div id="two" className="">
				<div className="row">
					<div className="col-lg-5"><h6 className="u-fw-500 topBoleta"><p className="ncLeft">Boleta{console.log("LOC",props)}</p></h6></div>
					<div className="col-lg-5"><h6 className="u-fw-500"><p className="ncRight">Nota de Crédito</p>{console.log("LOC",props)}</h6></div>
					<div className="col-lg-2"><input className="form-control  buscadorReporte fontAwesome " placeholder="&#xF002; Buscar"  /></div></div>
						<table className="Product-table Local responsive-table striped" >
						
							<THead3/>
							
							<tbody>
							<div className="u-scrollX-auto u-Maxheight-226">

								{
									props.ncRemoto.length > 0 
										?
										props.ncRemoto.map((local,index)=> {
									
											return (
												<tr key={index}>
												<td><p>{local.originalTransaction.formato} </p></td>
												<td><p>{local.originalTransaction.mercado}</p></td>
												<td><p>{local.originalTransaction.numLocal}</p></td>
												<td><p>{Fn.formateaRut(local.customerId)}</p></td>
												<td><p>{local.originalTransaction.folio}</p></td>
												<td><p>{Fn.formatDate(local.originalTransaction.date)}</p></td>
												<td className="line"><p>
													{
														local.idFile
														? <a className=" fileBoletaEn" onClick={() => props.handleViewTicket(local.idFile)}><i class="far fa-paperclip"></i></a>
														: <a className="fileBoleta-disabled"><i class="far fa-paperclip"></i></a>
													} </p>
												</td>
												<td><p>{local.formato}</p></td>
												<td>{local.mercado}</td>
												<td>{local.loginLocal}</td>
												<td>{local.originalTransaction.opr}</td>
												<td>{local.folio}</td>
								
												<td>{Fn.formatDate(local.date)}</td>
												<td>$ {new Intl.NumberFormat().format(local.total)}</td>

												{/* <td>
													{
														itemm.idFile
														? <a className="btn-floating waves-effect waves-light transparent" onClick={() => props.handleViewTicket(itemm.idFile)}><i className="fal fa-file-alt u-text-pink"></i></a>
														: <a className="btn-floating waves-effect waves-light transparent disabled" onClick={() => props.handleViewTicket(itemm.idFile)}><i className="fal fa-file-alt u-text-pink"></i></a>
													}
												</td>
												<td>{itemm.loginLocal}</td>
												<td>{Fn.formateaRut(itemm.username)}</td>
												<td>{itemm.folio}</td>
												<td>{Fn.formatDate(itemm.date)}</td>
												<td>$ {new Intl.NumberFormat().format(itemm.total)}</td> */}
												{/* <td><a href="#modalReport" className="modal-trigger" onClick={() => props.handleClickGetCreditNoteData(itemm)}><i className="fal fa-list-alt fa-2x u-text-gray"></i></a></td> */}
												<td><a href="#modalReport" className="modal-trigger" onClick={() => props.handleClickGetCreditNoteData(local, "emisor")}><i className="fal fa-list-alt u-text-green"></i></a></td>
											</tr>
												// <tr key={index} >
												// 	<td>{local.formato} </td>
												// 	<td>{local.mercado}</td>
												// 	<td>{local.originalTransaction.numLocal}</td>
												// 	<td>{Fn.formateaRut(local.customerId)}</td>
												// 	<td>{local.originalTransaction.folio}</td>
												// 	<td>{Fn.formatDate(local.originalTransaction.date)}</td>
												// 	<td>
												// 		{
												// 			local.idFile
												// 			? <a className="btn-floating waves-effect waves-light transparent " onClick={() => props.handleViewTicket(local.idFile)}><i className="fal fa-file-alt u-text-pink"></i></a>
												// 			: <a className="btn-floating waves-effect waves-light transparent disabled" onClick={() => props.handleViewTicket(local.idFile)}><i className="fal fa-file-alt u-text-pink"></i></a>
												// 		}
												// 	</td>
												// 	<td>{local.formato}</td>
												// 	<td>{local.mercado}</td>
												// 	<td>{local.loginLocal}</td>
												// 	<td>{Fn.formatDate(local.date)}</td>
												// 	<td>{local.folio}</td>
												// 	<td>{local.total}</td>

												// 	<td>$ {new Intl.NumberFormat().format(local.total)}</td>
												// 	<td><a href="#modalReport" className="modal-trigger " onClick={() => props.handleClickGetCreditNoteData(local, "receptor")}><i className="fal fa-list-alt u-text-green"></i></a></td>
												// </tr>

											)}
										
										
										)

										: <tr><td colSpan="100" className="red-text center-align p-2">NO EXISTEN DATOS</td></tr>
								}
							</div>

							</tbody>
						</table>
						

				</div>
				
			}
			</div>
		</div>
		
		

		<div className="row">
		<div className="col-lg-10"></div>
		<div className="col-lg-2 sumaT">	
			<b>Total Monto: </b> $ {props.sumaT}
						</div>
			<div className="col d-flex justify-content-end Excel-Button">
				<a className="waves-effect waves-light btn mr-3 Raised u-bg-pink" onClick={props.handleStatePDF}><i class="fal fa-file-pdf right"></i>Descargar PDF</a>
				{/* <Excel {...props} /> */}
			</div>
		</div>
		<div className="row">
			<div className="col-lg-12 greenResult">
				{props.qtyLocal==0 ?
				<p>Se han encontrado {props.qtyRemoto} Resultados</p>:
				<p>Se han encontrado {props.qtyLocal} Resultados </p> }

			
			</div>
		</div>
	</Fragment>
)

const FormAjuste = props => {
	const user = Func.getUser();
	return(
		<Formik
			initialValues={{
				num_local: user.num_local,
				inLocal: false,
				otherLocal: false,
				getToDate : DateFormat((new Date())),
				getFromDate : DateFormat((new Date())),
				toDate : new Date(),
				fromDate : new Date(),
			}}
			validate={values => {
				let errors = {}

				if (values.num_local.toString().length < 1)
					errors.num_local = "El campo es requerido";
				else if (values.num_local.toString().length > 5)
					errors.num_local = "El campo no puede superar los 5 caracteres";

				if (values.getToDate > values.getFromDate)
					errors.toDate = "La fecha debe ser anterior al siguiente campo";

				// if (!values.inLocal && !values.otherLocal)
				// 	errors.otherLocal = "El campo es requerido";

				return errors
			}}
			onSubmit={(values, { setSubmitting, setErrors }) => {
				console.table(values)
				let token = JSON.parse(sessionStorage.getItem('user'));
				let selector = null;
				// if (values.inLocal && !values.otherLocal)
				// 	selector = 1
				// else if (values.otherLocal && !values.inLocal)
				// 	selector = 2
				// else if (values.inLocal && values.otherLocal)
				// 	selector = 0
				// setTimeout
			

				setTimeout(() => {

					//DETERMINA SI ES POR LOCAL O POR LOCALES 
					let local=0
					let selector=0;
					
					if (props.optionPane===1){
					selector=1;
					local=JSON.parse(localStorage.getItem("Localini"));
						}
					if (props.optionPane===2){

					selector=2;
					local=JSON.parse(localStorage.getItem("Localini")) }

					const request = {
						fromDate: localStorage.getItem("fromD").replace("-","").replace("-",""),
						toDate: localStorage.getItem("toD").replace("-","").replace("-",""),
						// numLocal: values.num_local,
						numLocal: local,
						mercado: null,
						formato: null,
						selector: selector
					}

				fetch(conf.url.base + conf.endpoint.getCreditNotes, {
					method: 'post',
					body: JSON.stringify(request),
					headers: {
						'Content-Type': 'application/json',
						'Authorization': 'Bearer ' + token.access_token
					}
				}).then(res => res.json())
					.catch(function (error) {
					})
					.then(function (response) {
						if (response === undefined) {
							setSubmitting(false);
							return console.log('Log: ', { errorCode: 1, errorMessage: 'No existe respuesta' })
						}

						if (response.errorCode === 0 && !response.errorMessage) {
							props.updateStateTable(response,request);
							setSubmitting(false);
							localStorage.setItem("totalSuma",response.total)
						} else {
							setSubmitting(false);
							switch (response.errorCode) {
								case 1:
									setErrors({ num_local: response.errorMessage })
									break;
								case 2:
									setErrors({ otherLocal: response.errorMessage })
									break;
								case 3:
									setErrors({ fromDate: response.errorMessage })
									break;

								default:
									setErrors({ num_local: response.errorMessage })
									break;
							}
						}
					})  },1000) 
			}}
			render={({ setFieldValue, values, handleChange }) => (
				<Form>
					<div className="row">
						{/* <div className="col-12 col-lg-2">
							<InputField
								id="num_local"
								type="number"
								name="num_local"
								label="N° Local"
								labelclass="active"
								onKeyDown={Fn.keyPrevent}
								labelsize="1.3rem"
								readOnly={true}
							/>
						</div> */}
						<div className="">
							{/* <InputField
								component={CheckboxButton}
								id="inLocal"
								type="checkbox"
								name="inLocal"
								groups={false}
								text="Realizadas en el Local"
							/>
							<InputField
								component={CheckboxButton}
								id="otherLocal"
								type="checkbox"
								name="otherLocal"
								groups={false}
								text="Realizadas desde otros locales"
							/> */}
							{/* <label>
								<input type="checkbox" class="filled-in" name="inLocal" id="inLocal" onChange={handleChange} />
								<span>Realizadas en el local</span>
							</label>
							<label>
								<input type="checkbox" class="filled-in" name="otherLocal" id="otherLocal" onChange={handleChange} />
								<span>Realizadas desde otros locales</span>
							</label> */}
							<ErrorMessage name="otherLocal" component="div" className="red-text" />
						</div>
						<div className="col-12 col-lg-6 mt-5 mt-lg-0 row">
							<div className="col-4 d-flex align-items-center">
								<span>Seleccione  Fechas de las NC Realizadas</span>
							</div>
							<div className="col-4">
									<div className="pickers">
										<InputField
											type="date"
											id="fromDate"
											name="fromDate"
											labelclass='Desde'
											onBlur={(e) => {
												localStorage.setItem("fromD",e.target.value)
											}}
								
										/>
									</div>
								<ErrorMessage name='toDate' component="span" className="red-text" />
							</div>
							<div className="col-4">
									<div className="pickers">
										<InputField 
											id="toDate"
											name="toDate"
											type="date"
											labelclass='Hasta'
											onBlur={(e) => {
												localStorage.setItem("toD",e.target.value)
											}}
											// value={values.fromDate}
											// format="dd/MM/yyyy"
											// placeholder="10/10/2018"
											// mask={value =>
											// 	value
											// 		? [/[0-3]/, /\d/, "/", /0|1/, /\d/, "/", /1|2/, /\d/, /\d/, /\d/]
											// 		: []
											// }
										/>
									</div>
								<ErrorMessage name='fromDate' component="span" className="red-text" />
							</div>
						</div>
						<div className="col-2">
							{/* <Raised className="iconText" isSubmit={true} disabled={props.isSubmitting} text={props.isSubmitting === true ? <Loader /> :  'Buscar'} isIcon={true} icon='search u-fz-16' light={true} ></Raised> */}
							<button type="submit" tabIndex="2" name="action" className="searchB btn btn-searchB"><div className="searchB"><div className="row"><div className="col-lg-1"><i className="fal fa-search u-fz-16"></i></div><div className="col-lg-3">Buscar   &nbsp;  </div></div></div></button>
						</div>
					</div>
				</Form>
			)}
		/>

	)
}


export class ReportLocal extends PureComponent {
	constructor(props) {
		super(props);

    this.state = {
		UI_viewTable:1,
        savePDFDetalle: false,
        savePDF: false,
        isShowTable: false,
		ncLocal: [],
		ncRemoto: [],
        numLocalState: 0,
        list_PDF: [],
        listRemoto_PDF: [],
        listDetalle: [],
        dataExcel1: [],
        dataExcel2: [],
		filtro: null,
		UI_filtro: false,
		detailLocal: 0,
		detailFolio: 0,
		detailDate: "00000000",
		detailListProducts: [],
		printNcLocal: "",
		printNcDate: "",
		printNcFolio: "",
		totalRows: 0,
		stateEmisor: false,
		stateReceptor: false,
		inCheck:false,
		otherCheck:false,
		start: 0,
		tabKey: defaultTabKey,
		sumaT: 0,
		qtyRemoto: 0,
		qtyLocal: 0, 
		text :"",
		detailLocalTr: 0,
		detailFolioTr: 0,
		detailDateTr: 0,
		detailFormatTr : 0,
		detailMarketTr:"",
		detailRut:"",
		detailMarketNc:0,
		detailFormatNc:0,
		detailLocalNc:0,
		userId:"",
		idOperador:"",
		folioNc:"",
		fechaNc:"",
		motivo:"",
		totalMonto:""





	}
	
		
	this.onChange = this.onChange.bind(this);
	this.onTabClick = this.onTabClick.bind(this);
	this.tick = this.tick.bind(this);

	}
    handleState = () => {
        this.setState({ savePDFDetalle: true })
        setTimeout(() => { this.setState({ savePDFDetalle: false }) }, 9005);
    }

    handleStatePDF = () => {
        this.setState({ savePDF: true })
		setTimeout(() => { this.setState({ savePDF: false }) }, 5);
		 let input = document.getElementById("myTable");
   
		// divToPrint.style.pageBreakInside = "auto";
		// var nTr = divToPrint.getElementsByTagName('TR')
		// var i=0;
		// for(i=0; i<nTr.length; i++){
		//    nTr[i].style.pageBreakAfter = 'auto';
		//    nTr[i].style.pageBreakInside = 'avoid';
		// }
		
		// var f=0;
		// var nTh = divToPrint.getElementsByTagName('tHead');
		// for(f=0; f<nTh.length; f++){
		//    nTh[f].style.display = 'table-header-group'
	 
		// }
		// var g=0;
		// var nTf = divToPrint.getElementsByTagName('tFoot');
		// for(g=0; g<nTf.length; g++){
		//    nTf[g].style.display = 'table-header-group'
		// }
	
		// var newWin = window.open("");
		// newWin.document.write('<style>border: 1px</style>')
		// newWin.document.write(divToPrint.outerHTML);
		// newWin.pdf;
		// newWin.close();
		const doc = new jsPDF()
		doc.autoTable({ html: '#myTable' })
		doc.save('table.pdf')
	
		
    }

    setWidth = () => {
        this.props.setWidth("col-lg-12 col-md-12")
    }
    componentDidMount() {
		

		this.setWidth();
        var elems = document.querySelectorAll('.modal');
        var instances = M.Modal.init(elems);
        var elem = document.querySelectorAll('.datepicker');
        var instancesPick = M.Datepicker.init(elem);
    }

	updateStateTable = (response,request) => {

		if (request.selector === 0)
			this.setState({inCheck : true, otherCheck : true })
		else if (request.selector === 1)
			this.setState({inCheck : true, otherCheck : false})
		else if (request.selector === 2)
			this.setState({inCheck : false, otherCheck : true})

		setTimeout(() => {
			var ele = document.querySelectorAll('.tooltipped');
			var instancesTool = M.Tooltip.init(ele);

			var e = document.querySelectorAll('.tabs');
			var instance = M.Tabs.init(e);
		}, 5);

		let list = [];
		let mercadoNC=""
	
	







        response.creditNotesLocal.length > 0
        &&
        response.creditNotesLocal.map(local=> {
		

            list.push([
                local.formato,
                local.mercado,
				local.numLocal,
                Fn.formateaRut(local.customerId),
                local.originalTransaction.folio,
                Fn.formatDate(local.originalTransaction.date),
                // 
                local.loginLocal,
                Fn.formateaRut(local.username),
                local.folio,
                Fn.formatDate(local.date),
                new Intl.NumberFormat().format(local.total)
            ])
        })

        let listRemoto = [];
        response.creditNotesRemoto.length > 0
        &&
        response.creditNotesRemoto.map(local=> {
            listRemoto.push([
                local.formato,
                local.mercado,
                local.numLocal,
                Fn.formateaRut(local.customerId),
                local.originalTransaction.folio,
                Fn.formatDate(local.originalTransaction.date),
                // local.idFile,
				local.loginLocal,
                Fn.formateaRut(local.username),
                local.folio,
                Fn.formatDate(local.date),
                new Intl.NumberFormat().format(local.total)
            ])
        })

		this.setState({
			isShowTable: true,
			ncLocal: response.creditNotesLocal,
			ncRemoto: response.creditNotesRemoto,
            numLocalState: response.numLocal,
            list_PDF: list,
            listRemoto_PDF: listRemoto,
			filtro: response,
			sumaT: response.total,
			qtyRemoto: response.cantidadRemoto,
			qtyLocal:  response.cantidadLocal,
			detailMarketNc:0,
			detailFormatNc:0,
			detailLocalNc:0,
			userId:"",
			idOperador:"",
			folioNc:"",
			fechaNc:"",
			motivo:"",
			
        })

	}

	handleClickGetCreditNoteData = (item, v) => {
		console.log("item+"+item.idTabla)
		const request = {
			numLocal: item.originalTransaction.numLocal,
			date: item.date,
			folio: item.folio,
			idTabla: item.idTabla,
		}

		if (v === "emisor") {
			this.setState({ stateEmisor: true, stateReceptor: false })
		} else {
			this.setState({ stateReceptor: true, stateEmisor: false })
		}
		setTimeout(() => {
			let token = JSON.parse(sessionStorage.getItem('user'));
			fetch(conf.url.base + conf.endpoint.getCreditNoteData, {
				method: 'post',
				body: JSON.stringify(request),
				headers: {
					'Content-Type': 'application/json',
					'Authorization': 'Bearer ' + token.access_token
				}
			}).then(res => res.json())
				.catch(function (error) {
				})
				.then((response) => {
					if (response === undefined) {
						return console.log('Log: ', { errorCode: 1, errorMessage: 'No existe respuesta' })
                    }


					if (response.errorCode === 0) {
						let creditNotesObject=response.creditNote
						
						this.setState({
							detailLocalTr: creditNotesObject.originalTransaction.numLocal,
							detailFolioTr: creditNotesObject.originalTransaction.folio,
							detailDateTr: creditNotesObject.originalTransaction.date,
							detailFormatTr : creditNotesObject.originalTransaction.formato,
							detailMarketTr: creditNotesObject.originalTransaction.mercado,
							detailRut: creditNotesObject.customerId,
							detailListProducts: creditNotesObject.products,
							printNcLocal: creditNotesObject.loginLocal,
							printNcDate: creditNotesObject.date,
							printNcFolio: creditNotesObject.folio,
							detailMarketNc: creditNotesObject.mercado,
							detailFormatNc: creditNotesObject.formato,
							detailLocalNc: creditNotesObject.local,
							userId: creditNotesObject.userid,
							idOperador: creditNotesObject.originalTransaction.opr,
							folioNc: creditNotesObject.folio,
							fechaNc: creditNotesObject.date,
							motivo: creditNotesObject.reasonDescription,
							totalMonto: creditNotesObject.total
                        })

                        const listDetalle = [];
                        this.state.detailListProducts.map(item => {
                            listDetalle.push([
                                item.description,
                                item.deptNbr,
                                item.ean,
                                item.qty,
                                item.weight ? item.grams : 0,
                                "$" + new Intl.NumberFormat().format(item.price)
                            ])
                        })

                        this.setState({listDetalle :  listDetalle})


					} else {
						console.log('Error:', response);
					}
				})
		}, 100);
	}

	handleClickPrintNc = (a, b, c) => {
		NoteService(a, b, c);
	}

	handleViewTicket = (id) => {
		TicketService(id);
	}


	//las funciones del tab
	onChange(key) {
	  }
	
	  onTabClick(key) {
		this.setState({
		  tabKey: key
		});
	  }
	
	  tick() {
		const { start } = this.state;
	
		this.setState({
		  start: start + 10
		});
	  }
	
    render() {
		const { start } = this.state;
		// let products=[]
        // let texto = this.state.text;
        // if (props.ncLocal) {
        //     console.log("entro al 2")
        //     products = props.ncLocal
        // }
        
        // if (props.ncLocal === undefined || texto === "") {
   
        //  products= this.state.productosFactura }
        //  else if(!this.state.producto){
        //      products=this.state.productosFactura
        //  }
       //hasta aqui la logica del buscador

        const Nc_PDF = {
            local : {
                numLocal : this.state.numLocalState,
                tipo : 'Destino',
                fromDate : this.state.filtro !== null ?  Fn.formatDate(this.state.filtro.fromDate) : 'No hay fecha',
                toDate : this.state.filtro !== null ? Fn.formatDate(this.state.filtro.toDate) : 'No hay fecha',
             },
            props : { title: 'Local'+  this.state.numLocalState },
            col : ["Formato","Mercado","Local Receptor","RUT Cliente","Folio Boleta","Fecha Boleta","Local Emisor","RUT Colaborador","Folio NC","Fecha NC","$ NC"],
            row : this.state.list_PDF,
            rowRemoto: this.state.listRemoto_PDF
        }
		const DetalleCol = ["Descripción","Departamento","Código Barra","Unidades","Kilos","Precio"]
		const local=1;
		const todos=2;
		const ajusteO=3;
		const localTitle="Local N° "+ localStorage.getItem("Localini");

        return (
            <div>
                {this.state.savePDF && <PDF_NC N={Nc_PDF.props} col={Nc_PDF.col} row={Nc_PDF.row} Local={Nc_PDF.local} rowRemoto={Nc_PDF.rowRemoto} />}
				{this.state.savePDFDetalle && <PDF_Detalle N={this.state} col={DetalleCol} row={this.state.listDetalle} /> /* PDF detalle */}

                <div className="Card-Main card-content p-5 pt-4 cardH ">
                    <MainCardLead title="Informes de las Notas de Crédito Realizadas" size='u-fz-20 u-fw-500' spark="true" />
					<br></br><br></br>
        
		
                    {/* {
						this.state.isShowTable ?
							<FillTableNc {...this.state} />
						: null
					} */}

					{
						this.state.filtro !== null && <FillTableAjuste {...this.state} handleStatePDF={this.handleStatePDF} handleClickGetCreditNoteData={this.handleClickGetCreditNoteData} filtro={this.state.filtro} handleViewTicket={this.handleViewTicket} />
					}

				</div>
				{/* <div className="Card-Footer card-action p-0 pt-4">
					<span></span>
				</div> */}

                <Modal otherClassName="Modal-LastNC large" id="modalReport" /*Develop={true}*/>
                    <div className="">
					<ModalHeader
                        icon="fal fa-file-invoice"
                        title="Nota de Crédito"
                        color="success"
                    /></div>
                    <ModalMain>
						<div className="col-lg-1 ej">

						 <MainCardLead title="Boleta"  spark="true" className="ej" />
						</div>
						<div className="col-lg-11"></div>
                        <table className="responsive-table mt-3 ReportDetalle-table striped">
                            <thead>
                                <tr>
                                    <td><div className="px-3 valign-wrapper"><b className="mr-2">Formato</b></div></td>
                                    <td><div className="px-3 valign-wrapper"><b className="mr-2 mr-2">Mercado </b></div></td>
                                    <td><div className="px-3 valign-wrapper"><b className="mr-2">Local Receptor </b></div></td>
									<td><div className="px-3 valign-wrapper"><b className="mr-2">Rut Cliente </b></div></td>
									<td><div className="px-3 valign-wrapper"><b className="mr-2">Folio Boleta </b></div></td>
									<td><div className="px-3 valign-wrapper"><b className="mr-2">Fecha Boleta</b></div></td>
                                </tr>
                            </thead>
							<tbody>
                                {
							
										this.state.detailListProducts.map((item,index) => {
										this.setState({ totalRows: this.state.detailListProducts.length })
										if (index===0){
										return (
										
											<tr>
												<td><p className="centerT">{this.state.detailFormatTr}</p></td>
												<td><p className="centerT">{this.state.detailMarketTr}</p></td>
												<td><p className="centerL">{this.state.detailLocalTr}</p></td>
												<td><p className="centerT">{Fn.formateaRut(this.state.detailRut)}</p></td>
												<td><p className="centerT">{this.state.detailFolioTr}</p></td>

												<td><p className="centerT"> {Fn.formatDate(this.state.detailDateTr)}</p></td>

												{/* <td>{"$" + new Intl.NumberFormat().format(item.price)}</td> */}
											</tr>
										) }
									})
								}
                            </tbody>
                        </table>
						<div className="row">
						<div className="col-lg-1">
						 <p className="ncTitle"><MainCardLead title="Nota de Crédito" spark="true"  /></p>
						</div>
						<div className="col-lg-7"></div>
						<div className="col-lg-1 ptL"><p className="productTitle ptL"><MainCardLead title="Producto" spark="true"  /></p></div></div>
                        <table className="responsive-table mt-3 ReportDetalle-table striped">
                            <thead>
                                <tr>
                                    <th><b className="mr-2 for"></b>Formato</th>
                                    <th>Mercado</th>
                                    <th>Local Emisor</th>
                                    <th>Rut Colaborador</th>
                                    <th>ID Operador</th>
                                    <th><p className="centerT">Folio NC</p></th>
									<th>Fecha NC</th>
									<th><p className="centerT">Motivo</p></th>
									<th className="desc"><p className="centerT">Descripcion</p></th>
									<th>Dep</th>
									<th><p className="centerT">EAN</p></th>
									<th>Cantidad</th>
									<th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
									this.state.detailListProducts.map(item => {
										this.setState({ totalRows: this.state.detailListProducts.length })
										return (
											<tr>
												<td className="centerT">{this.state.detailFormatNc}</td>
												<td className="centerT">{this.state.detailMarketNc}</td>
												<td className="centerT">{this.state.printNcLocal}</td>
												<td className="centerT">{JSON.stringify(this.state.userId)>5? Fn.formateaRut(JSON.stringify(this.state.userId)) :this.state.userId}</td>
												<td className="centerT">{this.state.idOperador}</td>
												<td className="centerT">{this.state.folioNc}</td>
												<td className="centerT">{Fn.formatDate(this.state.printNcDate)}</td>
												<td className="centerT">{this.state.motivo}</td>
												<td className="centerT desc">{item.description}</td>
												<td className="centerT">{item.deptNbr}</td>
												<td className="centerT">{item.ean}</td>
											    <td className="centerT">{item.qty}</td>
												<td className="centerT">{"$" + new Intl.NumberFormat().format(item.price)}</td>
												
												</tr>
										)
									})
								}
								<tr><td><p className="monto"><b>TOTAL MONTO:</b> ${this.state.totalMonto}</p></td></tr>
                            </tbody>
                        </table>
                        <div className="row text-right">
                            {/* <small className="col mt-3">{this.state.totalRows + ' resultado(s) encontrado(s)'}</small> */}
                        </div>
                        <div className="row mt-3 d-flex justify-content-between">
						<div className="col-md-4 col-lg-4 mt-md-0 mt-3 center-align"></div>

                            <div className="col-md-1 col-lg-1 mt-md-0 mt-3 center-align">
								<button className="waves-effect waves-light btn Raised buttonNew" onClick={() => this.handleClickPrintNc(this.state.printNcLocal, this.state.printNcDate, this.state.printNcFolio)}>
								<i class="fal fa-print"></i>	Imprimir NC </button>
							</div>
							<div className="col-md-1 col-lg-1 mt-md-0 mt-3 center-align">
                                <button className="waves-effect waves-light btn u-bg-pinkB Raised buttonNew" onClick={this.handleState}>
								<i className="fal fa-file-pdf "></i> Descargar PDF</button>
							</div>
							<div className="col-md-1 col-lg-1 mt-md-0 mt-3 center-align ">
                                <ExcelYup {...this.state} />
                            </div>
                        </div>
                    </ModalMain>
                    <ModalFooter>
                        <ModalButtonRaised name="Cerrar" className="ReportDetalle-btn-cerrar" />
                    </ModalFooter>
                </Modal>
            </div>
        )
    }
}
