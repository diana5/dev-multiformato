import React, { PureComponent } from 'react';
import M from 'materialize-css';
import InputField from '../Ui/Forms/InputField';
import { Formik, Form } from 'formik';
import { find, map } from 'lodash';
import Raised from '../Ui/Buttons/Raised';
import './Product.css';
import conf from '../../config.json';
import BreadCum from '../Shared/Main/BreadCum'
import Checkbox from '@material-ui/core/Checkbox';
import Motivos from "./Motivos";
import MainCardLead from '../Shared/Main/MainCardLead';
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content'
import { render, hydrate } from "react-dom";
import Loader from '../Loader';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import swal from 'sweetalert';
import { handleClickAllLogout } from '../../Utils';

const MySwal = withReactContent(Swal)



// let stepper = React.createRef();


/**
 * Se encargara de evaluar si el producto es valido para ser devuelto
 * a travez de la aplicacion ncmultiformato.
 *
 * @param {Bool} isReturnable
 */
function iconValidation(isReturnable) {
    if (isReturnable) {
        return <i className="fas fa-check-circle fa-2x  tooltipped ml-2" data-position="top" data-tooltip="Habilitado para devolución"></i>
    } else {
        return <i className="fas fa-times-circle fa-2x  lighten-2-text tooltipped ml-2" data-position="top" data-tooltip="No Habilitado para devolución"></i>
    }
}

function gramsValidation(grams,index) {
    localStorage.setItem("grams"+index,grams)
    if (grams>0) {
        return grams
    } 
}

function valuesValidation(grams,index) {
    localStorage.setItem("grams"+index,grams)
    if (grams>0) {
        return grams
    } 
}

function MotivesInfo(){
  return 
  
}

/**
 * Hace la logica de negocio donde evalua si el producto
 * es pesable o no, en caso de NO ser pesable retornara
 * la cantidad y en caso de ser pesable retornara los grms.
 *
 * @param {Bool} isWeight
 * @param {Int} cant
 * @param {Int} grams
 */
function isWeight(isWeight, cant, grams) {
    if (!isWeight) {
        return cant
    } else {
        return grams + 'g'
    }
}


export class Product extends PureComponent {

  displayName = Product.name

    constructor(props) {

        super(props);
        this.state = {
            isGoing: true,
            numberOfGuests: 2,
            verified: false,
            desactivar: false,
            productosFactura: [],
            SquareCheck1: true,
            renderiza: false,
            checked: {},
            reason: {},
            qtys: {},
            loadingChecked: true,
            origen:"",
            enableNext: true,
            motivo:"",
            isHovering: false,
            qtyMotiv:false,
            disableMotivo:false,
            inputVal:{},
            qty:{},
            motivoDis:false
            
          
        };

        this.handleBlurChangeMotiv = this.handleBlurChangeMotiv.bind(this);

    }

    toCurrency=(n)=> {  
       let num= String(n).replace(/(.)(?=(\d{3})+$)/g,'$1,')
        return num.replace(",",".")
    
      }

    verifiedChange = e => {
        // e.preventDefault(); It's not needed
        const { verified } = e.target;
        this.setState({
            verified: !this.state.verified // It will make the default state value(false) at Part 1 to true 
        });
    };

    setWidth = () => {
        this.props.setWidth("col-lg-12 col-md-12")
    }

  

    handleBlurChangeMotiv = (e) =>{

        console.log("entra")
        this.setState({ motivo: e.target.value,
                        motivoDis:true }) 
    console.log("el motivo", this.state.motivo)
    localStorage.setItem("motivoD",e.target.value.nombre)
    localStorage.setItem("motivoSelected", e.target.value.id)

    }

    changeQty= (event,index,value,available,valor) => {
        let qty="";
        console.log("lo que viene:",event.target.value)
        let elementbyid=document.getElementById("inputDevolucion"+index).value
      if ((event.target.value!="") && (event.target.value!=null)){
      this.setState({
          qtyMotiv: true,
         
      })
      localStorage.setItem("ean"+index,value)
    qty= event.target.value
    
    } else{
        this.setState({
            qtyMotiv: true,
           
        })  

        localStorage.setItem("ean"+index,value)
     qty= elementbyid
      }

    

    this.setState({
        qtys: { ...this.state.qty, [index]: qty },
       
    })
  
  }



    //funcion que recibe un indice y retorna el valor del estado actual del motivo
    retornaSeleccionado = (index) => {
        if (this.state.index == index) {
            return this.state.motivo

        }

    }

    getKeyByValue = (object, value) => {
        return Object.keys(object).find(key => object[key] === value);
    }

    componentDidMount() {
        if(localStorage.getItem("regionCustomer")){
            localStorage.removeItem("regionCustomer");
        }
        if(localStorage.getItem("comunaCustomer")){
            localStorage.removeItem("comunaCustomer");
            }
        localStorage.removeItem("rutCliente")
        localStorage.removeItem("rutCliente")
        localStorage.removeItem("marcaResumen")
        localStorage.removeItem("marcaResumen")
        localStorage.setItem("swal","si")
        setTimeout(() => {
            if(localStorage.getItem("reload")==="si"){
                window.location.reload()
            localStorage.removeItem("reload")}
         },2000)

        let simb = '';
        simb += '<div>';
        simb += '	<p class="">Simbología</p>';
        simb += '	<div class="d-flex mb-2">';
        simb += '		<i class="far fa-check-circle fa-lg green-text mr-2"></i>';
        simb += '		<span>Producto habilitado para devolución Multiformato</span>';
        simb += '	</div>';
        simb += '	<div class="d-flex mb-2">';
        simb += '		<i class="far fa-times-circle fa-lg redI mr-2"></i>';
        simb += '		<span>Producto no habilitado para devolución Multiformato</span>';
        simb += '	</div>';
        simb += '</div>';
        let tooltip = document.querySelectorAll('.tooltipped');
        let tooltipInfo = document.querySelectorAll('.tooltippedInfo');
        let tooltipError = document.querySelectorAll('.tooltippedError');
        M.Tooltip.init(tooltip);
        M.Tooltip.init(tooltipInfo, { html: simb });
        M.Tooltip.init(tooltipError);

       
        let motive = '';
        motive += '<p>';
        motive += '<div class="d-flex mb-2"><span><b>Lider – Express de Lider - SBA</b></span></div>';
        motive += '	<div class="d-flex mb-2"><span>- Garantía Legal GM: para productos de GM, en caso de falla de productos. </span></div>';
        motive += '<div class="d-flex mb-2"><span>-Garantía de Satisfacción: para productos que cuenten con garantía de satisfacción. </span></div>';
        motive += '<div class="d-flex mb-2"><span>-Calidad Alimentaria: para productos de alimentación ante una posible mala calidad.</span></div>';
        motive += '<div class="d-flex mb-2"><span>-Diferencia de Precio: sólo cuando exista una diferencia de precio entre la góndola y la caja.</span></div>';
        motive += '<div class="d-flex mb-2"><span>-Error de Marcación: sólo cuando existe un error de la cajera al marcar.</span></div>';
        motive += '<div class="d-flex mb-2"><span>-Revisión de Precio: sólo cuando se necesita revisar los precios de listas (catálogos)</span></div>';
        motive += '<div class="d-flex mb-2"><span>-Procedimiento de Seguridad: sólo procedimiento mecheros.</span></div>';
        motive += '<div class="d-flex mb-2"><span>-Compare y Ahorre: sólo cuando se necesita realizar la publicación de boleta (Solo SBA)</span></div>';

        

        let tooltipM = document.querySelectorAll('.tooltippedMotive');
        let tooltipInfoM = document.querySelectorAll('.tooltippedInfoMotive');
        let tooltipErrorM = document.querySelectorAll('.tooltippedErrorM');
        M.Tooltip.init(tooltipM);
        M.Tooltip.init(tooltipInfoM, { html:  motive });
        M.Tooltip.init(tooltipErrorM);
    
      var origen="";
      setTimeout(() => {

        const request = {
            numLocal: localStorage.getItem("numLocal"),
            date: localStorage.getItem("date"),
            pos: localStorage.getItem("pos"),
            transaction: localStorage.getItem("transaction"),

        }
        let token = JSON.parse(sessionStorage.getItem('user'));
        fetch(conf.url.base + conf.endpoint.getTransaction, {
            method: 'post',
            body: JSON.stringify(request),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token.access_token
            }
        }).then(res => res.json())
            .catch(function (error) {
                console.log('Error:', error);
            })
            .then((response)=> {
                console.log('Succes:', response);
                if (response === undefined) {
                    // setSubmitting(false);
                    return console.log('Log: ', { errorCode: 1, errorMessage: 'No existe respuesta' })
                }

                if (response.errorCode === 0) {

                    this.setState({
                        productosFactura: response.products

                    })

                    localStorage.setItem("origen",response.origen)
                    localStorage.setItem("motivos",JSON.stringify(response.reasons))
                    localStorage.setItem("productosFact", JSON.stringify(response.products))

                }
                else {
                    // setSubmitting(false);
                    console.log('Error:', response);

                }
            })
    }, 800);


    if (true) {
        setTimeout(() => {

            const request = {
                numLocal: localStorage.getItem("numLocal"),
                date: localStorage.getItem("date"),
                pos: localStorage.getItem("pos"),
                transaction: localStorage.getItem("transaction"),

            }
            let token = JSON.parse(sessionStorage.getItem('user'));
            fetch(conf.url.base + conf.endpoint.getCreditNote, {
                method: 'post',
                body: JSON.stringify(request),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token.access_token
                }
            }).then(res => res.json())
                .catch(function (error) {
                    console.log('Error:', error);
                })
                .then(function (response) {
                    console.log('Succes:', response);
                    if (response === undefined) {
                        // setSubmitting(false);
                        return console.log('Log: ', { errorCode: 1, errorMessage: 'No existe respuesta' })
                    }else

                    if (response.errorCode === 0) {
                        localStorage.setItem("productosSeleccionados", JSON.stringify(response.creditNote.products))
                        // console.log("PRODUCTOS SELECCIONADOS: ", response.creditNote.products)
                        // console.log(localStorage.getItem("productosSeleccionados"))


                    }
                    else 						
                    if(response.errorCode===-1){
                        MySwal.fire({

                            text: "Atención",
                            html:
                                "<b>Atención</b><br><hr><br>" +
                                '<h7>Su sesión ha expirado</h7>,<br>' +
                                '<h7>Presiona aceptar para volver al inicio</h7> </div><br><br><hr>',
                            showCancelButton: true,
                            confirmButtonColor: '#fff',
                            cancelButtonColor: '#3085d6',
                            confirmButtonText: 'Cancelar',
                            cancelButtonText: 'Aceptar'
                        }).then((result) => {
                            handleClickAllLogout()
                            
                        });
            
                    }
                })
        }, 1500);

    }


    setTimeout(() => {
        
        const checked = {}
        let prod = [localStorage.getItem("productosSeleccionados").replace('/', '')];
        //Los productos Seleccionados
        let produc =[]
        console.log("el prod",JSON.stringify(prod))
        if (!prod[0]===undefined){
        produc = JSON.parse(prod)
        console.log(JSON.parse(prod)) }
        if (localStorage.getItem("productosFact")){
        const srcData = JSON.parse(localStorage.getItem("productosFact").replace('/', '')); 
        //Seteo los chequeados comparandolos los productos de factura vs los de la saveCredit Note
   
        map(srcData, (data, index) => {
            if (find(produc, prod => prod.ean === data.ean))
                checked[index] = true
        }); }

        console.log("el prodD",localStorage.getItem("productosFact"))
        if(localStorage.getItem("productosFact")){
        this.setState({
            checked,
            loadingChecked: false,
            origen: localStorage.getItem("origen")
        }) }

        console.log("estado en didmount##",this.state.motivos)
    }, 2000)


    // setTimeout(() => {
    //     if (localStorage.getItem("recargaProductos") == "si") {
    //         window.location.reload()
    //         window.location.reload()

    //         localStorage.removeItem("recargaProductos")
    //     }
    // }, 2500)
}




    handleChange = (event) => {
        this.setState({
            selected: !this.state.selected,
            actualizar: false
        });

    }

    getKeyByValue = (object, value) => { 
        let size=0;
        for (var prop in object) { 
            if (object.hasOwnProperty(prop)) { 
                if (object[prop] === value) 
                
                size= size + 1
                if(size===0){
                this.setState({ enableNext: false})}
                else
                this.setState({ enableNext: true})
               
            } 
        } 
    } 


    filter= (event, data) => {
        console.log("entr<",data)
        let datos = JSON.stringify(data)
        var text = event.target.value
        // const otherData = datos.slice(1, -1).replace(/\\/g, '')
        console.log("OTHER",datos)
        const newData = JSON.parse(datos).filter(function (item) {
            const itemDataEan = item.ean.toUpperCase()
            const itemDataDesc = item.description.toUpperCase()
            const campo = itemDataEan + " " + itemDataDesc;
            const textData = text.toUpperCase()
            return campo.indexOf(textData) > -1


        })

        this.setState({
            producto:newData,
            text: text,
        })

    }


    anterior = () => {
        this.props.history.goBack()
    }

    handleInputChange = (event, index, weight,ean) => {
        const { target: { checked } } = event
        // console.log('handleInputChange', name)
        this.setState({
            checked: { ...this.state.checked, [index]: checked },
           
        })

        if (!checked) {
            localStorage.setItem("comment" + index, "")
            localStorage.setItem("qty" + index, "")
        }

        if(checked && weight){
            localStorage.setItem("ean"+index,ean)
        }
    }



    render() {
        const { loadingChecked } = this.state
        const invokeDisable= this.getKeyByValue(this.state.checked,true)
        //La Logica del buscador, no tocar
        //La Logica del buscador, no tocar
        let products=[]
        let texto = this.state.text;
        if (this.state.producto) {
            console.log("entro al 2")
            products = this.state.producto
        }
        
        if (this.state.producto === undefined || texto === "") {
   
         products= this.state.productosFactura }
         else if(!this.state.producto){
             products=this.state.productosFactura
         }
       //hasta aqui la logica del buscador


       //hasta aqui la logica del buscador



        let sizesInput =2000
            let initialVal = {
            inputFile: '',
            inputFileName: '',
            motivo:'',
        };
     
        if(this.state.productosFactura){

  
        for (var i = 0; i < sizesInput; i++) {
            initialVal['inputDevolucion_' + i] = ""; 
            initialVal['inputBarra_' + i] = localStorage.getItem("ean"+i)
            initialVal['inputAvailable_' + i] = //JSON.parse(this.state.productosFactura).find("ean",i)
            initialVal['inputMotivo_' + i] = localStorage.getItem("motivo" + i);
            initialVal['motivo'] ="";
        } }

        return (
            <div className="pad30">
            <div className="Card card cardProducto p-0">
                <div>
                    <span>
                        <BreadCum></BreadCum>
                    </span>


                    <div id="root"></div>
                </div>
                <div className="row">
                    <div className="textL"></div>
                    <div className="col-11 col-lg-8 col-md-12 col-sm-12">
                        <div className="Lead Desktop">
                            <MainCardLead columna=""title="Selecciona y luego ingresa la cantidad de productos a devolver de la boleta del cliente"  spark={true}/>
                        </div>
                        
                        <div className="Lead Phone">
                            <b>Selecciona y luego ingresa la cantidad de productos a devolver de la boleta  </b>
                       
                         <div className="rightMain px-3">   
                         <MainCardLead className="rightMain" title=" del cliente" size="" spark={true}/>
                         </div>
                        </div>
                        
                    </div>
                </div>
                <div className="Card-HeaderP card-titleP center-align pad">

                    <div className="row">
                        <div className="col">
                            {/*-------------------- el buscador -------------------------------- */}

                            <Formik
                                initialValues={initialVal}
                                validate={values => {
                                    let errors = {};

                                    products.map((item, index) => {
                                        if (values['inputDevolucion_' + index] > values['inputAvailable_' + index]) {
                                            let tooltipError = document.querySelectorAll('.tooltippedError');
                                            M.Tooltip.init(tooltipError);
                                            // this.setState({})
                                        }
                                    })

                                    if (values.inputFile != undefined && values.inputFile.name != undefined) {
                                        if (values.inputFile.type != 'application/pdf') {
                                            errors.inputFile = "El archivo a cargar debe ser de extensión .pdf";
                                        }
                                    }

                                    if (values.inputFile.size > 2000000) { // TODO : cambiar tamaño por 2000000 = 2MB
                                        errors.inputFile = "El archivo no puede superar los 2MB";
                                    }

                                    return errors;
                                }}
                                onSubmit={(values, { setSubmitting, setErrors }) => {
                                    console.log("El motivo",this.state.motivo)
                                    if (values.inputFileImage) {
                                        let formData = new FormData();
                                        formData.append('filename', values.inputFile.name);
                                        formData.append('uploadedFile', values.inputFileImage);

                                        var idFile;
                                        let token = JSON.parse(sessionStorage.getItem('user'));
                                        fetch(conf.url.base + conf.endpoint.uploadFile, {
                                            method: 'post',
                                            body: formData,
                                            headers: {
                                                'Authorization': 'Bearer ' + token.access_token
                                            }
                                        }).then(res => res.json())
                                            .catch(function (error) {
                                            })
                                            .then(response=> {
                                                if (response === undefined) {
                                                    setSubmitting(false);
                                                    return console.log('Log: ', { errorCode: 1, errorMessage: 'No existe respuesta' })
                                                }

                                                if (response.errorCode === 0 && !response.errorMessage) {
                                                this.setState({idFile:  response.idFile })
                                                localStorage.setItem("idFile",response.idFile)

                                                } else {
                                                    console.log('Error:', response);
                                                }
                                                let token = JSON.parse(sessionStorage.getItem('user'));
                                          
                                            })
                                    }



                                    setTimeout(() => {
                                        let arr = [];

                                        for (let index = 0; index < sizesInput; index++) {
                                            if (this.state.checked[index]) {
                                              let motivo=""
                                                  if(this.state.motivo[i]){
                                                   motivo=this.state.motivo[i] }
                                                   else{
                                                    motivo=localStorage.getItem("motivo"+index)
                                                   }

                                                   let qty=0
                                                   if (!values['inputDevolucion_' + index]){
                                                   qty=JSON.parse(localStorage.getItem("grams"+index))
                                                   localStorage.removeItem("grams"+index)
                                                   }

                                                   if(values['inputDevolucion_' + index]){
                                                    qty=values['inputDevolucion_' + index]
                                                   }

                                                   if ((qty!=null | qty!="") && (motivo!="")) {
                                                    console.log("ENTRO qty======>",qty,motivo)
                                                    //busco la fila en los productos y retorno el ean
                                                    let item =this.state.productosFactura[index].ean;
                                                    console.log("item ACTUAL=>"+item)
                                                    arr.push({
                                                        fila: index,
                                                        ean: item,
                                                        qty: qty,
    
                                                    })
    
                                                    localStorage.setItem("qty" + index,qty)
    
                                                }
                                            }
                                        }


                                        let usr = JSON.parse(sessionStorage.getItem('user'));
                                        const request = {
                                            numLocal: 0,
                                            idFile: this.state.idFile || '',
                                            customerId:"",
                                            reason: JSON.parse(localStorage.getItem("motivoSelected")),
                                            originalTransaction: {
                                                numLocal: localStorage.getItem("numLocal"),
                                                date: localStorage.getItem("date"),
                                                pos: localStorage.getItem("pos"),
                                                transaction: localStorage.getItem("transaction")
                                            },
                                            products: arr,
                                            payment: {

                                            }
                                        }

                                        const productosSeleccionados = localStorage.getItem("prod")

                                        let desactivar = false
                                        console.log("request",request.products)
                                       

                                        localStorage.setItem("pantallaProductos", JSON.stringify(request))

                                        localStorage.setItem("productosGuardados", JSON.stringify(request.products))
                                        if (request.products.length===0 | request.arr === null) {
                                            desactivar = true;
                                            setSubmitting(false);

                                            swal({
                                                title:"Atención",
                                                text:"Recuerda hacer click sobre el cuadro ubicado al lado izquierdo del código de barra del producto que desea devolver y también agregar la cantidad para así poder avanzar.",
                                                button: {
                                                    text: "Aceptar",
                                                  },
                                        }) }
                                      console.log("largo del array",request.products)
                                        
                                        
                                        if ((request.products.length>0)){
                                            console.log("EMPIEZA A LLAMAR EL SERVICIO GUARDAR")
                                            
                                        fetch(conf.url.base + conf.endpoint.saveCreditNote, {
                                            method: 'post',
                                            body: JSON.stringify(request),
                                            headers: {
                                                'Content-Type': 'application/json',
                                                'Authorization': 'Bearer ' + usr.access_token
                                            }
                                        }).then(resp => resp.json())
                                            .catch(function (err) {
                                                console.log('Error:', err);

                                            })
                                            .then(function (re) {

                                                if (re.errorCode === 0) {
                                                    console.log("respondio el guardarNC=> ",JSON.stringify(re))

                                                     window.location.href = '/dmf/client'

                                                }  if (re.errorCode !== 0){
                                                    localStorage.setItem("respuesta", "no");
                                                    swal({
                                                        title:"Atención",
                                                        text:re.errorMessage,
                                                        button: {
                                                            text: "Aceptar",
                                                          }
                                                    
                                                    })
                                                    console.log("error",re)

                                                }

                                                // this.props.history.push({
                                                //     state: this.state
                                                // })

                                            })



                                      } else
                                      if (this.state.motivo==="" | this.state.motivo===undefined){
                                        swal({
                                            title:"Atención",
                                            text: "Por favor,Ingrese un motivo de devolución",
                                            button: {
                                                text: "Aceptar",
                                              }
                                        
                                        })                                      }
                                    }, 200); 
                                }}
                                render={({ setFieldValue, isSubmitting, status, setStatus, errors }) => (

                                    <Form encType="multipart/form-data">

                                        <div className="row m-11 ">
                                            <div className="col-lg-7 col-md-12 col-12"></div>
                                            <div className="col-lg-4 col-md-10 col-12 file">
                                                <div className="file-field input-field">
                                                    <div className="btn">
                                                        <span>Subir Boleta</span>
                                                        <input
                                                            id="inputFile"
                                                            name="inputFile"
                                                            type="file"
                                                            onBlur={e => {
                                                                var file = e.target.files[0];
                                                                var reader = new FileReader();
                                                                setFieldValue("inputFile", file);
                                                                reader.onload = function (item) {
                                                                    setFieldValue("inputFileImage", item.target.result);
                                                                };
                                                                file ? reader.readAsDataURL(file) : console.log("no")
                                                                if (file !== undefined) {
                                                                    file.size < 2000000 && setStatus({ inputFile: 'El archivo ha sido cargado exitosamente' })
                                                                }
                                                            }}
                                                        />
                                                    </div>
                                                    <div className="file-path-wrapper">
                                                        <input id="inputFileName" className="file-path validate" type="text" placeholder="No hay boletas seleccionadas" />
                                                    </div>
                                                </div>
                                                {errors && errors.inputFile && <div className="red-text">{errors.inputFile}</div>}
                                                {
                                                    !errors.inputFile
                                                        ? status && status.inputFile && <div className="green-text">{status.inputFile}</div>
                                                        : null
                                                }
                                                {/* <ErrorMessage name="inputFile" component="span" className="red-text" /> */}
                                            </div>
                                            <div className="col-12 col-md right-align align-self-center">

                                            </div>
                                        </div>
                                        <div className="col-lg-4"></div>
                                        <div className="contenedor">
                                            <div className="row nomargin details">
                                            <div className="col-lg-3"><p  className="mW" >Detalle Boleta Origen: <b>{this.state.origen}</b></p></div>
                                                <div className="col-lg-1"><p className="mW"></p></div>
                                                <div className="col-lg-3"><i className="fal fa-search buscarLupa"></i><input style={{fontSize:'12px', lineHeight:'12'}} className="form-control buscador fontAwesome " placeholder=" Buscar Producto" value={this.state.text} onChange={(text) => this.filter(text, this.state.productosFactura)} /></div>
                                                <div className="col-lg-2"><p className="mW inf">Información</p></div>
                                                <div className="col-lg-1"></div>
                                                <div className="col-lg-2"><p className="mW dev">Acciones Devolución</p></div>

                                            </div>

                                            <div className="col-lg-12">

                                            </div>
                                            <div className="">
                                                {
                                                    <table className="Principal-table hoverTable">
                                                        <thead style={{height:'31px'}}>
                                                            <tr className="head-tabla-dev">
                                                                <th className="firstP"></th>
                                                                <th>Código de Barra</th>
                                                                <th>Producto</th>
                                                                <th>Cantidad</th>
                                                                <th >Precio</th>
                                                                <th>Promoción</th>
                                                                <th>Descuento</th>
                                                                <th className="">&nbsp;Total</th>
                                                                <th className="hab">Habilitado <i className="fas fa-question-circle fa-lg tooltippedInfo" data-position="top" ></i></th>
                                                                <th className="disp">Disponible</th>
                                                                <th className="" style={{width:'11%'}}>Cantidad de Productos</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            {
                                                                products.map((item, index) => {
                                                                    if (!item.returnable){
                                                                        localStorage.removeItem("qty"+index)
                                                                        localStorage.removeItem("motivo"+index)
                                                                    }
                                                                    let count = isWeight(item.weight, item.qty, item.grams)
                                                                    let price = "$" +  this.toCurrency(item.price);

                                                                    return (
                                                                        <tr key={item.ean}>
                                                                            <td className="firstP">
                                                                                <Checkbox
                                                                                    disabled={!item.returnable}
                                                                                    name={"SquareCheck" + index}
                                                                                    id={"SquareCheck" + index}
                                                                                    onChange={(e) => this.handleInputChange(e, index, item.weight, item.ean)}
                                                                                    checked={this.state.checked[index]}
                                                                                    color="primary"
                                                                                    label="Secondary"
                                                                                >
                                                                                </Checkbox>

                                                                            </td>
                                                                            <td id={"ean"+index}>{item.ean}</td>
                                                                            <td>{item.description}</td>
                                                                            <td><p className="returnable">{count}</p></td>
                                                                            <td ><p className="returnable">{price}</p></td>
                                                                            <td>{item.promoDescription}</td>
                                                                            <td><p className="returnable">{"-$" + new Intl.NumberFormat().format(item.discount)}</p></td>
                                                                            <td className="borde-derecho" ><p className="returnable">{"$" + this.toCurrency(item.total)}</p></td>

                                                                            <td><p className="returnable">{iconValidation(item.returnable)}</p>
                                                                            </td>
                                                                            <td className="borde-derecho"><p className="returnable">{!item.weight ? item.available + ' uds' : item.available + ' g'} </p></td>
                                                                            <td><p className="returnable">
                                                                                <InputField
                                                                                    field={`inputDevolucion_${index}`}
                                                                                    type="number"
                                                                                    name={`inputDevolucion_${index}`}
                                                                                    className="qty"
                                                                                    icon='true'
                                                                                    disabled={!item.returnable | item.grams>0}
                                                                                    readonly
                                                                                    placeholder={gramsValidation(item.grams,index)}
                                                                                    onClick={(e) => this.changeQty(e,index,item.ean,item.available) }
                                                                                    onBlur={(e) => this.changeQty(e,index,item.ean,item.available) }

                                                                                    min="0"
                                                                                    max={item.available}
                                                                                    id={"inputDevolucion"+index}
                                                                                /></p>
                                                                            </td>
                                                                            <td className="d-none borde-derecho">
                                                                                <InputField
                                                                                    field={`inputBarra_${index}`}
                                                                                    type="number"
                                                                                    name={`inputBarra_${index}`}
                                                                                />
                                                                            </td>
                                                                            <td className="d-none">
                                                                                <InputField
                                                                                    field={`inputAvailable_${index}`}
                                                                                    type="number"
                                                                                    name={`inputAvailable_${index}`}
                                                                                    style={{width:"500px"}}
                                                                                    
                                                                                   
                                                                                />
                                                                            </td>
                                                                           
                                                                        </tr>
                                                                        
                                                                    )
                                                                })
                                                            }
                                                        </tbody>
                                                    </table>
                                                }
                                            </div>
                                        </div>
                     <div className="row nomargin motivos">
                                            <div className="col-3 col-md-6 col-lg-6">
                                        
                                            </div>
                                            <div className="col-lg-3 col-md-3"  ><b>Seleccionar Motivo Devolución</b>  &nbsp;<i className="fas fa-question-circle fa-lg  tooltippedInfoMotive "  data-position="top"></i>&nbsp;&nbsp;&nbsp;
                                            </div>
                                            <div className="col-12 col-md-2 col-lg-2 right-align align-self-center">
                                            <Motivos  
                                            field="motivo" 
                                            name="motivo" 
                                            id="motivo" 
                                            motivos={localStorage.getItem("motivos")}  
                                            onChange={(e) => this.handleBlurChangeMotiv(e)}
                                             /></div>
                                            
                    
                                        </div>
                                        <div className="row rowmargin">
                                            <div className="col-lg-3  col-sm-2 col-xs-2">
                                                <button className="Raised btn waves-effect waves-light px-5 pt-2 pb-2" type="button" onClick={(e) => {
                                                    e.preventDefault();
                                                    window.location.href = '/dmf/boleta';
                                                    }}>Anterior
                                                </button>
                                                {/* <Raised text='Anterior' isIcon={false} href='/boleta' /> */}
                                            </div>
                                            <div className="col-lg-6 col-sm-4 col-xs-2 col-md-6"></div>
                                            <div className="col-lg-2 col-sm-2 col-xs-2">
                                                <Raised disabled={isSubmitting  || !this.state.motivoDis} isSubmit={true} text={isSubmitting === true ? <Loader /> : 'Siguiente'} isIcon={false} /></div>
                                            <div id="step" className="z">1</div></div>
                                    </Form>
                                )}
                            />
                        </div>
                    </div>
                </div>
               <div className="">
                    <span></span>
                </div> 
            </div></div>
        )
    }
}