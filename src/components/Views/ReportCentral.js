import React, { Component, Fragment } from "react";
import "./ReportAll.css";
import { throttle, isEmpty } from "lodash";
import { Form, Formik, ErrorMessage, Field } from "formik";
import MainCardLead from "../Shared/Main/MainCardLead";
import { Fn } from "../../Utils";
import Modal from "../Shared/Modal/Modal";
import ModalHeader from "../Shared/Modal/ModalHeader";
import ModalMain from "../Shared/Modal/ModalMain";
import ModalFooter from "../Shared/Modal/ModalFooter";
import ModalButtonRaised from "../Shared/Modal/ModalButtonRaised";
import M from "materialize-css";
import DateFnsUtils from "@date-io/date-fns";
import "./ReportAll.css";
import esLocale from "date-fns/locale/es";
import { Func } from "../../func/func";
import { THeadReport } from "../Tables/THeadReport";
import { TableReport } from "../Tables/TableReport";
import { TBodyReport } from "../Tables/TBodyReport";
import { TicketService } from "../../services/TicketService";
import { THeadReportDetail } from "../Tables/THeadReportDetail";
import { TBodyReportDetailOne } from "../Tables/TBodyReportDetailOne";
import { TBodyReportDetailTwo } from "../Tables/TBodyReportDetailTwo";
import { Excel, ExcelYup } from "../Ui/Excel/Excel";
import PDF_NC from "../Ui/Pdf/PDF_NC";
import PDF_Detalle from "../Ui/Pdf/PDF_Detalle";
import { NoteService } from "../../services/NoteService";
import conf from "../../config.json";
import InputField from "../Ui/Forms/InputField";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import { THeadPersonalizado } from "../Tables/THeadPersonalizado";
import { THeadAjusteOperador } from "../Tables/THeadAjusteOperador";
import { THeadAjusteDañado } from "../Tables/THeadAjusteDañado";
import { TBodyAjusteMerma } from "../Tables/TBodyAjusteMerma";
import { TBodyAjusteOp } from "../Tables/TBodyAjusteOp";
import { TBodyAjustePerso } from "../Tables/TBodyAjustePerso";
import {
	getLocalDetailData,
	getLocalDetail,
	normalizeProducto
} from "../../services/reportes";
import ButtonPDFCentral from "../Ui/Pdf/ButtonPDFCentral";
import ButtonInformeExcel from "../Ui/Excel/Informe";
import normalizePDF from "../Ui/Pdf/normalizePDF";
import IconButton from "@material-ui/core/IconButton";
import InputAdornment from "@material-ui/core/InputAdornment";
import SearchIcon from "@material-ui/icons/Search";
import { TextField } from "@material-ui/core";
import { ValueFormatterService } from "ag-grid-community";

const DateFormat = value => {
	let formatDay = null;
	let formatMonth = null;
	let date = new Date(value);
	let year = date.getUTCFullYear();
	let day = date.getDate();
	let month = date.getMonth() + 1;
	if (day <= 9) formatDay = "0" + day.toString();

	if (month <= 9) formatMonth = "0" + month.toString();

	const fullDate =
		year.toString() +
		(formatMonth === null ? month.toString() : formatMonth.toString()) +
		(formatDay === null ? day.toString() : formatDay.toString());
	return fullDate;
};

const FormCentral = ({
	updateTipoInforme,
	motivosList,
	getMotivos,
	localesList,
	getLocales,
	formatoList,
	getMercados,
	mercadoList,
	getAjusteOpInfo,
	datosAjusteOp,
	TipoInforme,
	getError,
	productListRemoto,
	addRowList,
	addSumaTotal,
	allrows,
	...props
}) => {
	return (
		<Formik
			initialValues={{
				informe: null,
				formato: null,
				mercado: null,
				num_local: null,
				inLocal: false,
				otherLocal: false,
				getToDate: DateFormat(new Date()),
				getFromDate: DateFormat(new Date()),
				toDate: new Date(),
				fromDate: new Date(),
				item: "",
				motivo: ""
			}}
			validate={values => {
				let errors = {};

				if (!values.informe) errors.informe = "El campo es requerido";

				let inputLocal = document.getElementById("inLocal").disabled;
				let inputOther = document.getElementById("otherLocal").disabled;

				if (!inputLocal && !inputOther) {
					if (!values.inLocal && !values.otherLocal)
						errors.otherLocal = "El campo es requerido";
				}

				if (values.getToDate > values.getFromDate)
					errors.toDate =
						"La fecha debe ser anterior al siguiente campo";

				return errors;
			}}
			onSubmit={async (values, { setSubmitting, setErrors }) => {
				const getError = response => {
					switch (response.errorCode) {
						case 1:
							setErrors({ num_local: response.errorMessage });
							break;
						case 2:
							setErrors({ otherLocal: response.errorMessage });
							break;
						case 3:
							setErrors({ fromDate: response.errorMessage });
							break;

						default:
							setErrors({ num_local: response.errorMessage });
							break;
					}
				};
				try {
					const user = Func.getUser();
					console.log("el numero de local:",values.num_local)
					
					const request = {
						fromDate: localStorage
							.getItem("fromD")
							.replace("-", "")
							.replace("-", ""),
						toDate: localStorage
							.getItem("toD")
							.replace("-", "")
							.replace("-", ""),
						numLocal:
							values.num_local === "null" ||
							values.num_local === null ||
							values.num_local === "Todos"
								? "0"
								: values.num_local,
						mercado:
							values.mercado === "null" ||
							values.mercado === null ||
							values.mercado === "Todos"
								? null
								: values.mercado,
						formato:
							values.formato === "null" ||
							values.formato === null ||
							values.formato === "Todos"
								? null
								: values.formato,
						itemNo:
							values.item === "" ||
							values.item === null ||
							values.item === "Todos"
								? null
								: JSON.parse(values.item),
						motivo:
							values.motivo === "null" ||
							values.motivo === null ||
							values.motivo === "Todos"
								? null
								: values.motivo,
						selector: 0
					};

					// INFORME DE AJUSTE OPERADOR; PERMANECE IGUAL
					if (TipoInforme === "2") {
						const rq = {
							fromDate: localStorage
								.getItem("fromD")
								.replace("-", "")
								.replace("-", ""),
							toDate: localStorage
								.getItem("toD")
								.replace("-", "")
								.replace("-", ""),
							numLocal:
								values.num_local === "null" ||
								values.num_local === null ||
								values.num_local === "Todos"
									? "0"
									: values.num_local,
							mercado:
								values.mercado === "null" ||
								values.mercado === null ||
								values.mercado === "Todos"
									? null
									: values.mercado,
							formato:
								values.formato === "null" ||
								values.formato === null ||
								values.formato === "Todos"
									? null
									: values.formato,
							selector: 0
						};

						return getAjusteOpInfo(rq);
					}

					const rcLocales = await getLocalDetail(request);
					console.log("RCLOCALES", rcLocales);

					addSumaTotal(rcLocales.sumaLocal);

					let requestProductoLocal = [];
					rcLocales.creditNotesLocal.map(
						({ originalTransaction, date, folio }) =>
							requestProductoLocal.push({
								numLocal: originalTransaction.numLocal,
								date,
								folio
							})
					);

					let requestProductoRemote= [];
					rcLocales.creditNotesRemoto.map(
						({ originalTransaction, date, folio }) =>
							requestProductoRemote.push({
								numLocal: originalTransaction.numLocal,
								date,
								folio
							})
					);

					const responseProductoLocal = await Promise.all(
						requestProductoLocal.map(
							async request => await getLocalDetailData(request)
						)
					);

					const responseProductoRemote = await Promise.all(
						requestProductoRemote.map(
							async request => await getLocalDetailData(request)
						)
					);

					if(values.num_local==="0"){
					addRowList(normalizeProducto(responseProductoRemote));
					console.log("NCREMOTO",responseProductoRemote)
					}
					else{
					addRowList(normalizeProducto(responseProductoLocal));
					console.log("NCLOCALES",responseProductoLocal)

					}
					return props.updateStateTable(
						rcLocales,
						request,
						values.informe,
						values.inLocal,
						values.otherLocal,
						rcLocales.sumaLocal,
						rcLocales.cantidadLocal,
						rcLocales.cantidadRemoto
					);
				} catch (error) {
					return getError(error);
				} finally {
					setSubmitting(false);
				}
			}}
			render={({ setFieldValue, values, handleChange, handleBlur }) => (
				<Form>
					<div className="row  mb-0 content ">
					
						<div className="tipoInformeInput pick">
							<div className="input-field reporte">
								<FormControl style={{	width:"100%"}}>
									<InputLabel id="demo-simple-select-label pick" style={{marginTop:'-8px', fontSize:'11px'}}>
										Tipo de Informe

									</InputLabel>
									<Select
										id="informe"
										name="informe"
										value={values.informe}
										className="reportcentral"
										onChange={e => {
											console.log(
												"el valor formato",
												e.target.value
											);
											values.informe = e.target.value;
											props.loadTipoAjuste(e);
											updateTipoInforme(e);
										}}
										onBlur={handleBlur}
										
										style={{
											fontSize: "10px",
										
											
										

									
										}}
									>
										<MenuItem value={"3"}>
											Ajuste de Proceso de Dañados
										</MenuItem>
										<MenuItem value={"0"}>
											Control Nota de Crédito
										</MenuItem>
										<MenuItem value={"1"}>
											Notas de Crédito DMF
										</MenuItem>
										<MenuItem value={"2"}>
											Ajuste Operador
										</MenuItem>
									</Select>
								</FormControl>
								{/* <label>Informe</label> */}
							</div>
							{/* <ErrorMessage name='informe' component="span" className="red-text" /> */}
						</div>

						<div className="formatoInput" >
							<div className="input-field">
								<FormControl style={{	width:"100%",	fontSize: "10px"}}>
									<InputLabel id="demo-simple-select-label"  style={{marginTop:'-8px', fontSize:'11px'}}>
										Formato
									</InputLabel>
									<Select
										id="formato"
										className="formato"
										name="formato"
										value={values.formato}
										style={{
											fontSize: "11px",
											
										}}
										onChange={e => {
											let val = e.target.value;
											console.log(
												"el valor formato",
												val
											);
											setFieldValue("formato", val);
											getMercados(val);
											getMotivos(val);
											getLocales(null,val);
											// props.loadMercado(val, props.listMercado)
										}}
										onBlur={handleBlur}
										className="browser-default"
									>
										<MenuItem
											name="Todos"
											value={"Todos"}
											name="Todos"
											key={"0"}
										>
											<div id="formato">{"Todos"}</div>{" "}
										</MenuItem>
										{Object.values(formatoList).map(
											(formato, index) => {
												return (
													<MenuItem
														key={formato.id}
														value={formato.nombre}
														name={formato.nombre}
													>
														<div id="formato">
															{" "}
															{formato.nombre}
														</div>
													</MenuItem>
												);
											}
										)}
									</Select>
								</FormControl>
								{/* <label>Formato</label> */}
							</div>
							{/* <ErrorMessage name='formato' component="span" className="red-text" /> */}
						</div>

						{values.informe != "2" ? (
							<div className="formatoInput mb-0" >
								<div className="input-field reportcentral pick">
									<FormControl style={{	width:"100%", fontSize: "10px"}}>
										<InputLabel id="demo-simple-select-label"  style={{marginTop:'-8px', fontSize:'10px'}}>
											Mercado
										</InputLabel>
										<Select
											id="mercado"
											name="mercado"
											value={values.mercado}
											onChange={e => {
												let val = e.target.value;
												setFieldValue("mercado", val);
												props.loadLocales(
													val,
													props.listLocales
												);
												getLocales(val);
											}}
											onBlur={handleBlur}
											className="browser-default reportcentral"
											style={{
												fontSize: "10px",
											}}
										>
											<MenuItem
												name="Todos"
												value={"Todos"}
												name="Todos"
												key={"0"}
											>
												<div id="mercado">
													{"Todos"}
												</div>{" "}
											</MenuItem>

											{mercadoList &&
												Object.values(mercadoList).map(
													(mercado, index) => {
														return (
															<MenuItem
																key={mercado.id}
																value={
																	mercado.mercado
																}
																name={
																	mercado.mercado
																}
															>
																<div id="mercado">
																	{" "}
																	{
																		mercado.mercado
																	}
																</div>
															</MenuItem>
														);
													}
												)}
										</Select>
									</FormControl>
								</div>
							</div>
						) : (
							<p className="esconder"></p>
						)}

						<div className="col-12  col-sm-12 col-lg-1  mb-0">
							<div className="input-field">
								<FormControl style={{	width:"100%"}}>
									<InputLabel id="demo-simple-select-label"  style={{marginTop:'-8px', fontSize:'11px' }}>
										Nº Local
									</InputLabel>
									<Select
										id="num_local"
										name="num_local"
										value={values.num_local}
										onChange={e => {
											let val = e.target.value;
											setFieldValue("num_local", val);
											getMotivos()
										}}
										onBlur={handleBlur}
										className="browser-default"
										style={{
											fontSize: "inherit",
											
										}}
									>
											<MenuItem
															value={
																"0"
															}
															name={"Todos"}
														>
															<div id="mercado">
																Todos
															</div>
														</MenuItem>

										{localesList &&
											Object.values(localesList).map(
												(local, index) => {
													return (
													
														<MenuItem
															key={local.comuna}
															value={
																local.numLocal
															}
															name={
																local.numLocal
															}
														>
															<div id="mercado">
																{" "}
																{local.numLocal}
															</div>
														</MenuItem>
													);
												}
											)}
									</Select>
								</FormControl>

								{/* <label>N° Local</label> */}
							</div>
							{/* <ErrorMessage name='mercado' component="span" className="red-text" /> */}
						</div>

						{values.informe === "1" ? (
							<div className="pick itemInput"  >
								<div className="input-field item" style={{fontSize:'12px'}}>
									<InputField
										type="text"
										name="item"
										placeholder="Item"
										value={values.item}
										onBlur={e => {
											let val = e.target.value;
											values.item = e.target.value;
											console.log("valor",values.item)
											// props.loadLocales(val, props.listLocales)
										}}

										style={{ 
										fontSize:'12px',
										
										
										
										
												
									}}
									/>
								</div>
							</div>
						) : (
							<p className="esconder"></p>
						)}

						{values.informe === "1" ? (
							
								<div className="inputMotivo">
									<div className="input-field">
										<FormControl style={{	width:"100%"}}>
											<InputLabel id="demo-simple-select-label" style={{
													fontSize:'10px'
													
												}}>
												Motivo
											</InputLabel>
										
											<Select
												id="motivo"
												name="motivo"
												value={values.motivo}
												onChange={e => {
													let val = e.target.value;
													values.motivo =
														e.target.value;
													// props.loadLocales(val, props.listLocales)
												}}
												onBlur={handleBlur}
												className="browser-default"
												style={{
													fontSize: "inherit", 
													fontSize:'10px'
													
												}}
											>
												{motivosList &&
													Object.values(
														motivosList
													).map((reason, index) => {
														return (
															<MenuItem
																key={reason.index}
																value={
																	reason.id
																}
																name={
																	reason.nombre
																}
															>
																<div id="mercado">
																	{" "}
																	{
																		reason.nombre
																	}
																</div>
															</MenuItem>
														);
													})}
											</Select>
										</FormControl>
									</div>
								</div>
							
						) : (
							<p className="esconder"></p>
						)}

						<div className="pick desde" style={{marginRight:'8px'}}>
							<Fragment utils={DateFnsUtils} locale={esLocale}>
								<div className="pickers">
									<InputField
										type="date"
										id="Desde"
										name="fromDate"
										labelclass="Desde"
										placeholder="Sesde"

										onBlur={e => {
											localStorage.setItem(
												"fromD",
												e.target.value
											);
											console.log(
												"fromD",
												localStorage.getItem("fromD")
											);
										}}
										max={localStorage.getItem("hoyR") ? localStorage
											.getItem("hoyR")
											.replace("/", "-")
											.replace("/", "-") :"" }
											
											style={{
												fontSize:'12px',
												lineHeight: '2.2em',
												marginRight:'7px'
												
											}}
											
									/>
								</div>						
									</Fragment></div>

								<div className="desde pick"  >
							<Fragment>
								<div className="pickers">
									<InputField
										type="date"
										id="toDate"
										name="toDate"
										type="date"
										placeholder="desde"
										onBlur={e => {
											localStorage.setItem(
												"toD",
												e.target.value
											);
											console.log(
												"toD",
												localStorage.getItem("toD")
											);
										}}
										max={localStorage.getItem("hoyR")? localStorage
											.getItem("hoyR")
											.replace("/", "-") 
											.replace("/", "-"): ""}
									
											style={{
												fontSize:'12px',	
												lineHeight: '2.2em',
											
												
											}}
										
										
									/>
								</div></Fragment></div>			

						<ErrorMessage
							name="otherLocal"
							component="div"
							className="red-text"
						/>
						<div className="col-lg-2 col-12 col-md-6">
						<button
							type="submit"
							tabIndex="2"
							name="action"
							className="searchB btn btn-searchB"
						>
							<div className="searchB">
								<div className="row">
									<div className="col-lg-3  col-3 col-sm-3 col-md-2 ">
										<i className="fal fa-search u-fz-16"></i>
									</div>
									<div className="col-lg-3  col-5 col-sm-9 col-md-9">
										Buscar &nbsp;{" "}
									</div>
								</div>
							</div>
						</button></div>
							
							{/* <ErrorMessage name='toDate' component="span" className="red-text" /> */}
					
						{values.informe==="1" &&
						<div className=" col-lg-8 bluesquareFChico reporte">
									<b>Notas de Crédito DMF</b>
									<br></br>
									Este reporte te permitirá visualizar la
									información global de las Notas de Créditos
									de DMF realizadas en los locales de los
									distintos formatos de la compañía a
									excepción de central mayorista, donde puedes
									filtrar por ítem y motivo.
									{/* {localStorage.getItem("warning")}  */}
						</div>}
				

					</div>
					<div className="row  mb-0">
						<div className="col-lg-8">
							{values.informe === "0" ? (
								<div className="bluesquareReport">
									<b>Control Nota de Crédito</b>
									<br></br>
									Este reporte te permitirá llevar un control
									global de las Notas de Créditos de DMF
									realizadas en los locales de los distintos
									formatos de la compañía a excepción de
									central mayorista.
									{/* {localStorage.getItem("warning")} */}
								</div>
							) : values.informe === "1" ? (
								<div className="bluesquareFiltroAj">
									<b>Notas de Crédito DMF</b>
									<br></br>
									Este reporte te permitirá visualizar la
									información global de las Notas de Créditos
									de DMF realizadas en los locales de los
									distintos formatos de la compañía a
									excepción de central mayorista, donde puedes
									filtrar por ítem y motivo.
									{/* {localStorage.getItem("warning")}  */}
								</div>
							) : values.informe === "2" ? (
								<div className="bluesquareReport">
									<b>Ajuste Operador</b>
									<br></br>
									Este reporte te permitirá realizar el ajuste
									de cada operador que haya generado Notas de
									Créditos de DMF realizadas en los locales de
									los distintos formatos de la compañía a
									excepción de central mayorista.
									{/* {localStorage.getItem("warning")} */}
								</div>
							) : values.informe === "3" ? (
								<div className="bluesquareReport">
									<b>
										Ajuste Merma o Ajuste de Proceso Dañados
									</b>
									<br></br>
									Este reporte te permitirá llevar un control
									global de las Notas de Créditos de DMF
									realizadas en los locales de los distintos
									formatos de la compañía a excepción de
									central mayorista.{" "}
								</div>
							) : (
								<p></p>
							)}
						</div>
					</div>
				</Form>
			)}
		/>
	);
};

export class ReportCentral extends Component {
	constructor(props) {
		super(props);

		this.state = {
			ncLocal: [], // lista de nc del local x
			ncRemoto: [], // lista de nc de otros locales
			detailLocal: 0, // campo ncLocal -> detalle nc
			detailFolio: 0, // campo ncFolio -> detalle nc
			detailDate: "00/00/0000", // campo ncDate -> detalle nc
			stateEmisor: false, // campo para el check -> detalle nc
			stateReceptor: false, // campo para el check -> detalle nc
			detailListProducts: [], // lista de productos de la nc -> detalle nc
			totalRows: 0, // contador de filas de productos -> detalle nc
			inCheck: false, // indica que se marco en la busqueda
			otherCheck: false, // indica que se marco en la busqueda
			filtro: null,
			informe: "", // nombre para el informe
			savePDF: false, //Para guardar PDF
			list_PDF: [], //Para guardar PDF
			listRemoto_PDF: [], //Para guardar PDF
			listDetalle: [], //Para guardar PDF Detalle
			numLocalState: 0, //Para guardar PDF
			printNcLocal: 0,
			printNcDate: "00000000",
			printNcFolio: 0,
			printNcTienda: 0,
			printNcCliente: 0,
			printNcVoucher: 0,
			printTicked: 0,
			listFormato: [],
			listMercado: [],
			listLocales: [],
			selectFormato: [],
			selectMercado: [],
			selectLocales: [],
			originalSelectorLocal: false,
			originalSelectorOther: false,
			tipoAjuste: "",
			motivos: {},
			formatos: {},
			locales: {},
			mercados: {},
			titleInforme: "",
			tipoInforme: "",
			detailLocalB: null,
			detailFolioB: null,
			emisorB: null,
			receptorB: null,
			fechaB: "",
			mercadoB: "",
			rutCl: "",
			folioB: null,
			formatoB: "",
			otherDetails: {},
			total: null,
			totalDet: null,
			camposAjusteOperador: {},
			datosAjusteOp: {},
			qtyRemoto: 0,
			qtyLocal: null,
			tableLocalList: [],
			tableLocalDetail: [],
			tableLocalListData: [],
			tableLocalDetailData: [],
			productListRemoto: [],
			cantidadLineas: null,
			qtyRemote:0,
			cantidadLocal: 0,
			rowList: [],
			rowListDetalle: [],
			rowListFilter: [],
			searchTerm: "",
			sumaTotal: 0,
			sumaTotalProducto: 0,
			rowAjusteOperador: [],
		};
	}

	setWidth = () => {
		this.props.setWidth("col-lg-12");
	};

	componentDidMount() {
		// this.getLocales("Todos")
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth() + 1; //January is 0!

		var yyyy = today.getFullYear();
		if (dd < 10) {
			dd = "0" + dd;
		}
		if (mm < 10) {
			mm = "0" + mm;
		}

		var today = yyyy + "-" + mm + "-" + dd;

		localStorage.setItem("hoyR", today);

		console.log("fec", localStorage.getItem("hoyR"));

		const request = {
			apid: "83c2c355066d6dcc60f169d10b2c0823"
		};
		this.setWidth();
		var elems = document.querySelectorAll(".modal");
		M.Modal.init(elems);

		var elem = document.querySelectorAll(".datepicker");
		M.Datepicker.init(elem);

		var ele = document.querySelectorAll(".tooltipped");
		M.Tooltip.init(ele);

		let token = JSON.parse(sessionStorage.getItem("user"));
		fetch(conf.url.base + conf.endpoint.getReasons, {
			method: "post",
			body: JSON.stringify(request),
			headers: {
				"Content-Type": "application/json",
				Authorization: "Bearer " + token.access_token
			}
		})
			.then(res => res.json())
			.catch(function(error) {
			})
			.then(function(response) {
				// if (response.errorCode === 0) {
				// 	// this.getRazones=response.reasons
				// }

				console.log("Succes RAZONES:", response);
			});

		fetch(conf.url.base + conf.endpoint.getFormatos, {
			method: "post",
			body: JSON.stringify(request),
			headers: {
				"Content-Type": "application/json",
				Authorization: "Bearer " + token.access_token
			}
		})
			.then(res => res.json())
			.catch(function(error) {
			})
			.then(response => {

				if(response!=undefined){
				if (response.errorCode === 0) {
					localStorage.setItem("formatos", response.formatos);
					this.setState({ formatos: response.formatos });
				}}
			});

		this.setWidth();
		var elems = document.querySelectorAll(".modal");
		M.Modal.init(elems);

		var elem = document.querySelectorAll(".datepicker");
		M.Datepicker.init(elem);

		var ele = document.querySelectorAll(".tooltipped");
		M.Tooltip.init(ele);

		fetch(conf.url.base + conf.endpoint.getStoreList, {
			method: "post",
			body: JSON.stringify(request),
			headers: {
				"Content-Type": "application/json"
			}
		})
			.then(res => res.json())
			.catch(function(error) {
			})
			.then(response => {
				if (response === undefined)
					return console.log("Log: ", {
						errorCode: 1,
						errorMessage: "No cargaron los locales"
					});

				if (response.errorCode === 0 && !response.errorMessage) {
					this.setState({
						listFormato: response.formatos,
						listMercado: response.mercados,
						listLocales: response.locales,
						selectFormato: response.formatos
					});
					var elemss = document.querySelectorAll("select");
					M.FormSelect.init(elemss);
				} else {
				}
			});
	}

	getMercados = formato => {
		let request = {};
		if (formato === "Todos") {
			request = {
				apid: "83c2c355066d6dcc60f169d10b2c0823"
			};
		} else {
			request = {
				apid: "83c2c355066d6dcc60f169d10b2c0823",
				formato
			};
		}

		let token = JSON.parse(sessionStorage.getItem("user"));
		fetch(conf.url.base + conf.endpoint.getMercados, {
			method: "post",
			body: JSON.stringify(request),
			headers: {
				"Content-Type": "application/json",
				Authorization: "Bearer " + token.access_token
			}
		})
			.then(res => res.json())
			.catch(function(error) {
			})
			.then(response => {
				if (response!=undefined){
				if (response.errorCode === 0) {
					localStorage.setItem("formatos", response.mercados);
					this.setState({ mercados: response.mercados });
				} }
			});
	};

	getMotivos = formato => {
		const request = {
			apid: "83c2c355066d6dcc60f169d10b2c0823",
			formato
		};

		let token = JSON.parse(sessionStorage.getItem("user"));
		fetch(conf.url.base + conf.endpoint.getReasons, {
			method: "post",
			body: JSON.stringify(request),
			headers: {
				"Content-Type": "application/json",
				Authorization: "Bearer " + token.access_token
			}
		})
			.then(res => res.json())
			.catch(function(error) {
			})
			.then(response => {
				if (response.errorCode === 0) {
					localStorage.setItem("formatos", response.reasons);
					this.setState({ motivos: response.reasons });
				}
			});
	};

	getLocales = (mercado,formato) => {
		let request = {};
		if (mercado === "Todos") {
			request = {
				apid: "83c2c355066d6dcc60f169d10b2c0823"
			};
		} else {
			request = {
				apid: "83c2c355066d6dcc60f169d10b2c0823",
				mercado
			};
		}

		if ((formato !== null) && (formato !== undefined) && (formato !== "")) {
			if (formato==="Todos"){
				request = {
					apid: "83c2c355066d6dcc60f169d10b2c0823"
				};
			} else{
				request = {
					apid: "83c2c355066d6dcc60f169d10b2c0823",
					formato
				}
				
			}
		
		} 


		let token = JSON.parse(sessionStorage.getItem("user"));
		fetch(conf.url.base + conf.endpoint.getStoreList, {
			method: "post",
			body: JSON.stringify(request),
			headers: {
				"Content-Type": "application/json",
				Authorization: "Bearer " + token.access_token
			}
		})
			.then(res => res.json())
			.catch(function(error) {
			})
			.then(response => {
				if (response.errorCode === 0) {
					localStorage.setItem("formatos", response.mercados);
					this.setState({ locales: response.locales });
				}
			});
	};

	updateSetQ = q => {
		this.setState({ qtyLocal: q });
	};

	updateStateTable = (
		response,
		request,
		informe,
		local,
		other,
		total,
		cantidad,
		cantidadRemoto
	) => {

		this.setState({ total: total, qtyLocal: cantidad, qtyRemote: cantidadRemoto });
		if (request.selector === 0)
			this.setState({ inCheck: true, otherCheck: true });
		else if (request.selector === 1)
			this.setState({ inCheck: true, otherCheck: false });
		else if (request.selector === 2)
			this.setState({ inCheck: false, otherCheck: true });

		if (!local && !other) {
			this.setState({ originalSelectorOther: true });
		} else {
			this.setState({ originalSelectorOther: false });
		}

		let list = [];
		response.creditNotesLocal.length > 0 &&
			response.creditNotesLocal.map(local => {
				list.push([
					local.formato,
					local.mercado,
					local.numLocal,
					Fn.formateaRut(local.customerId),
					local.originalTransaction.folio,
					Fn.formatDate(local.originalTransaction.date),
					// local.idFile,
					local.loginLocal,
					Fn.formateaRut(local.username),
					JSON.stringify(local.folio).slice(0, 8),
					Fn.formatDate(local.date),
					new Intl.NumberFormat().format(local.total)
				]);
			});

		let listRemoto = [];
		response.creditNotesRemoto.length > 0 &&
			response.creditNotesRemoto.map(local => {
				listRemoto.push([
					local.formato,
					local.mercado,
					local.numLocal,
					Fn.formateaRut(local.customerId),
					local.originalTransaction.folio,
					Fn.formatDate(local.originalTransaction.date),
					// local.idFile,
					local.loginLocal,
					Fn.formateaRut(local.username),
					local.folio,
					Fn.formatDate(local.date),
					new Intl.NumberFormat().format(local.total)
				]);
			});

		this.setState({
			ncLocal: response.creditNotesLocal,
			ncRemoto: response.creditNotesRemoto,
			informe: informe,
			numLocalState: response.numLocal,
			list_PDF: list, // PAra guardar PDF
			listRemoto_PDF: listRemoto, // PAra guardar PDF
			filtro: response,
			informe: informe,
			sumaLocalTotal: response.total
		});

		setTimeout(() => {
			var ele = document.querySelectorAll(".tooltipped");
			var instancesTool = M.Tooltip.init(ele);

			var e = document.querySelectorAll(".tabs");
			var instance = M.Tabs.init(e);
		}, 5);
	};

	updateCombox = (razones, formatos, mercados, locales) => {
		this.setState({
			razones: razones,
			formatos: formatos,
			mercados: mercados,
			locales: locales
		});
	};

	getAjusteOperador = async request => {
		let token = JSON.parse(sessionStorage.getItem("user"));
		await fetch(conf.url.base + conf.endpoint.getDiffReport, {
			method: "post",
			body: JSON.stringify(request),
			headers: {
				"Content-Type": "application/json",
				Authorization: "Bearer " + token.access_token
			}
		})
			.then(res => res.json())
			.catch(function(error) {
			})
			.then(response => {
				if (response === undefined) {
					return console.log("Log: ", {
						errorCode: 1,
						errorMessage: "No existe respuesta"
					});
				}

				if (response.errorCode === 0) {
					this.setState({ filtro:response})
					this.setState({ inCheck: true, otherCheck: true });
					console.log("Succes AJUSTEopeRADOR:", response);
					this.setState({ datosAjusteOp: response });
					this.setState({ rowAjusteOperador: response.reportDetail });
					this.setState({ cantidadLineas: response.cantidadLineas });
					

					this.setState({ sumaDif: response.sumaDiferencias})
				}
			});

	};

	handleViewTicket = id => {
		TicketService(id);
	};

	toCurrency = n => {
		let num = String(n).replace(/(.)(?=(\d{3})+$)/g, "$1,");
		return num.replace(",", ".");
	};

	updateTipoInforme = e => {
		console.log("el tipo en el la funcin", e.target.value);
		this.setState({ tipoInforme: e.target.value });
	};

	addRowListDetalle = rowListDetalle => this.setState({ rowListDetalle });
	addTotalProducto = sumaTotalProducto =>
		this.setState({ sumaTotalProducto });

	handleClickGetCreditNoteData = async (item, v) => {
		const request = {
			numLocal: item.originalTransaction.numLocal,
			date: item.date,
			folio: item.folio,
			idTabla: item.idTabla
		};
		console.log("item+"+item.idTabla)
		console.log("request getcreditnotedata+"+JSON.stringify(item))

		if (v === "emisor") {
			this.setState({ stateEmisor: true, stateReceptor: false });
		} else {
			this.setState({ stateReceptor: true, stateEmisor: false });
		}

		let user = Func.getUser();
		await fetch(conf.url.base + conf.endpoint.getCreditNoteData, {
			method: "post",
			body: JSON.stringify(request),
			headers: {
				"Content-Type": "application/json",
				Authorization: "Bearer " + user.access_token
			}
		})
			.then(res => res.json())
			.catch(function(error) {
			})
			.then(response => {
				if (response === undefined) {
					return;
				}

				if (response.errorCode === 0) {
					this.addRowListDetalle(response.creditNote);
					this.addTotalProducto(response.creditNote.total);
					this.setState({
						detailLocal: response.creditNote.numLocal,
						detailFolio: response.creditNote.folio,
						detailDate: response.creditNote.date,
						detailListProducts: response.creditNote.products,
						otherDetails: response.creditNote,
						totalRows: response.creditNote.products.length,
						printNcLocal: response.creditNote.numLocal,
						printNcDate: response.creditNote.date,
						printNcFolio: response.creditNote.folio,
						folioB: response.creditNote.originalTransaction.folio,
						emisorB:
							response.creditNote.originalTransaction.loginLocal,
						receptorB:
							response.creditNote.originalTransaction.numLocal,
						fechaB: response.creditNote.originalTransaction.date,
						mercadoB:
							response.creditNote.originalTransaction.mercado,
						rutCl: response.creditNote.customerId,
						formatoB:
							response.creditNote.originalTransaction.formato,
						total: response.total
					});

					const listDetalle = [];
					this.state.detailListProducts.map(item => {
						listDetalle.push([
							item.description,
							item.deptNbr,
							item.ean,
							item.qty,
							item.weight ? item.grams : 0,
							"$" + new Intl.NumberFormat().format(item.total)
						]);
					});

					this.setState({ listDetalle: listDetalle });
				} else {
					console.log("Error:", response);
				}
			});
	};

	handleStatePDFDetalle = () => {
		// Para Guardar PDF Detalle
		this.setState({ savePDFDetalle: true });
		setTimeout(() => {
			this.setState({ savePDFDetalle: false });
		}, 5);
	};

	handleStatePDF = () => {
		// Para Guardar PDF
		this.setState({ savePDF: true });
		// setTimeout(() => { this.setState({ savePDF: false }) }, 5);
	};

	handleClickPrintNc = (a, b, c, d, e, f, g) => {
		NoteService(a, b, c, d, e, f, g);
	};
	loadMercado = (value, mercado) => {
		const list = [];
		mercado.map(item => {
			if (value === item.formato) {
				list.push(item);
			}
		});
		this.setState({
			selectMercado: list
		});
	};
	loadLocales = (value, local) => {
		const list = [];
		local.map(item => {
			if (value === item.mercado) {
				list.push(item);
			}
		});
		this.setState({
			selectLocales: list
		});
	};

	loadTipoAjuste = e => {
		console.log("funciona", e.target.value);
		this.setState({
			tipoAjuste: e.target.value
		});
	};

	addSumaTotal = sumaTotal => this.setState({ sumaTotal });
	addRowList = rowList => {
		this.setState({ rowListFilter: rowList });
		return this.setState({ rowList });
	};

	setSearch = searchTerm => this.setState({ searchTerm });

	searchArray = (array = [], searchKey) =>
		array &&
		array.filter(
			data =>
				JSON.stringify(data)
					.toLowerCase()
					.indexOf(searchKey.toLowerCase()) !== -1
		);

	searchRow = searchKey => {
		const { ncLocal, tipoInforme, datosAjusteOp } = this.state;
		if (parseInt(tipoInforme) === 2) {
			if (isEmpty(searchKey) && parseInt(tipoInforme) === 2) {
				this.setState({
					rowAjusteOperador: datosAjusteOp.reportDetail
				});
			}
			let rowAjusteOperador = this.searchArray(
				datosAjusteOp.reportDetail,
				searchKey
			);

			this.setState({ rowAjusteOperador });
			return;
		}

		if (isEmpty(searchKey) && parseInt(tipoInforme) !== 2) {
			this.setState({ rowListFilter: ncLocal });
		}
		let rowListFilter = this.searchArray(ncLocal, searchKey);
		this.setState({ rowListFilter });
	};

	componentDidUpdate(prevProps, prevState) {
		if (prevState.searchTerm !== this.state.searchTerm) {
			return this.searchRow(this.state.searchTerm);
		}
		return;
	}

	render() {
		const tHeadDetail = {
			tableTwo: [
				"Formato",
				"Mercado",
				"Local Emisor",
				"Rut Colaborador",
				"ID Operador",
				"Folio NC",
				"Fecha NC",
				"Motivo",
				"Descripción",
				"Dep.",
				"EAN",
				"Cantidad",
				"Total"
			]
		};

		const tHeadDetailItem = {
			tableTwo: [
				"Formato",
				"Mercado",
				"Local Emisor",
				"Rut Colaborador",
				"ID Operador",
				"Folio NC",
				"Fecha NC",
				"Motivo",
				"Descripción",
				"Item",
				"Dep.",
				"EAN",
				"Cantidad",
				"Total"
			]
		};

		const tHeadDetailO = {
			tableOne: [
				"Formato",
				"Mercado",
				"Local Receptor",
				"Rut Cliente",
				"Folio Boleta",
				"Fecha Boleta"
			]
		};
		const Nc_PDF = {
			local: {
				numLocal: this.state.numLocalState,
				tipo: "Destino",
				fromDate:
					this.state.filtro !== null
						? Fn.formatDate(this.state.filtro.fromDate)
						: "No hay fecha",
				toDate:
					this.state.filtro !== null
						? Fn.formatDate(this.state.filtro.toDate)
						: "No hay fecha"
			},
			props: { title: "Local" + this.state.numLocalState },
			col: [
				"Formato",
				"Mercado",
				"Local Receptor",
				"RUT Cliente",
				"Folio Boleta",
				"Fecha Boleta",
				"Local Emisor",
				"RUT Colaborador",
				"Folio NC",
				"Fecha NC",
				"$ NC"
			],
			row: this.state.list_PDF,
			rowRemoto: this.state.listRemoto_PDF
		};
		const DetalleCol = [
			"Descripción",
			"Departamento",
			"Código Barra",
			"Unidades",
			"Kilos",
			"Precio"
		];
		return (
			<div className="card Card reporte p-0 pad30">
				<div className="Card-Main card-content p-5 pt-4 cardH">
					<span className="strong Desktop titlePhone es">
						<MainCardLead
							title="Informes de las Notas de Crédito Realizadas"
							size="u-fz-20 u-fw-500"
							spark="true"
						/>
					</span>
					<div className="Phone col-12 reporte">
					<p className="titlePhone">Informes de las Notas de Crédito</p>
					<MainCardLead
						title=" Realizadas"
						size="u-fz-20 u-fw-500"
						spark="true"
					/>
					</div>
					
					<div className="resultrow">

					

														{this.state.qtyLocal>0 && (
														
														
														this.state.tipoInforme !=="1" ?
														<div className="col-lg-12 greenResultCentral">
															{
																<p>
																	Se han
																	encontrado{" "}
																	{this.state
																		.tipoInforme ===
																	"2" 
																		? this
																				.state
																				.cantidadLineas
																		: this
																				.state
																				.qtyLocal}{" "}
																	Resultados
																</p>
															} 
														</div> :
															<div className="col-lg-12 greenResultCentralDMF">
															{
																<p>
																	Se han
																	encontrado{" "}
																	{this.state
																		.tipoInforme ===
																	"2" 
																		? this
																				.state
																				.cantidadLineas
																		: this
																				.state
																				.qtyLocal}{" "}
																	Resultados
																</p>
															} 
														</div> )}


														{this.state.qtyLocal===0 && (
														
														
														this.state.tipoInforme !=="1" ?
														<div className="col-lg-12 greenResultCentral">
															{
																<p>
																	Se han
																	encontrado{" "}
																	{this.state
																		.tipoInforme ===
																	"2" 
																		? this
																				.state
																				.cantidadLineas
																		: this
																				.state
																				.qtyRemote}{" "}
																	Resultados
																</p>
															} 
														</div> :
															<div className="col-lg-12 greenResultCentralDMF">
															{
																<p>
																	Se han
																	encontrado{" "}
																	{this.state
																		.tipoInforme ===
																	"2" 
																		? this
																				.state
																				.cantidadLineas
																		: this
																				.state
																				.qtyRemote}{" "}
																	Resultados
																</p>
															} 
														</div> )}
													</div>

					<FormCentral
						{...this.props}
						addRowList={this.addRowList}
						addSumaTotal={this.addSumaTotal}
						updateTipoInforme={this.updateTipoInforme}
						motivosList={this.state.motivos}
						getMotivos={this.getMotivos}
						localesList={this.state.locales}
						getLocales={this.getLocales}
						mercadoList={this.state.mercados}
						getMercados={this.getMercados}
						formatoList={this.state.formatos}
						updateStateTable={this.updateStateTable}
						{...this.state}
						loadLocales={this.loadLocales}
						loadMercado={this.loadMercado}
						loadTipoAjuste={this.loadTipoAjuste}
						getAjusteOpInfo={this.getAjusteOperador}
						datosAjusteOp={this.state.datosAjusteOp}
						TipoInforme={this.state.tipoAjuste}
						format={this.toCurrency}
						getError={this.getError}
						productListRemoto={this.state.productListRemoto}
						allrows={this.state.allrows}
					/>

					{this.state.filtro !== null && (
						<Fragment>
							<div className="row mb-0 px-3">
								<div className="col-12 col-lg">
									{this.state.informe === "0" ? (
										<MainCardLead
											title={"Control Nota de Crédito"}
										/>
									) : (
										<p></p>
									)}

									{this.state.informe === "1" ? (
										<MainCardLead
											title={"Notas de Crédito DMF"}
										/>
									) : (
										<p></p>
									)}

									{this.state.informe === "2" ? (
										<MainCardLead
											title={"Ajuste Operador"}
										/>
									) : (
										<p></p>
									)}

									{this.state.informe === "3" ? (
										<MainCardLead
											title={"Ajuste de Dañados"}
										/>
									) : (
										<p></p>
									)}
								</div>
							</div>

							<div className="row">
								<div className="col">
									{this.state.inCheck && (
										<div id="one">
											<div className="row mb-1">
												<div className="col-lg-4">
													<h6 className="">
														{this.state
															.tipoInforme !==
														"2" ? (
															<p className="ncLeft">
																Boleta
															</p>
														) : (
															<p></p>
														)}
													</h6>
												</div>
												<div className="col-lg-6 mb-3">
													<h6 className="u-fw-500">
														{this.state
															.tipoInforme ===
														"3" ? (
															<p style={{paddingTop: '10px',
																paddingLeft: '58px'}}>
																Nota de Crédito
															</p>
														) : this.state
																.tipoInforme ===
														  "0" ? (
															<p className="ncRight moreright">
																Nota de Crédito
															</p>
														) : this.state
																.tipoInforme ===
														  "1" ? (
															<p style={{paddingLeft: '30px' }}>
																Nota de Crédito
															</p>
														) : (
															<p></p>
														)}
													</h6>
												</div>
												<div className="col-lg-2">
													<TextField
														className="buscadorReporte" 
														InputProps={{
															startAdornment: (
																<InputAdornment position="start">
																<SearchIcon />
																</InputAdornment>
															),
														  }}
														style={{fontSize:'5px'}}
														placeholder=" Buscar"
														value={
															this.state
																.searchTerm
														}
														onChange={event => {
															this.setSearch(
																event.target
																	.value
															);
														}}
														onKeyPress={event => {
															if (
																event.key ===
																"Enter"
															) {
																console.log(
																	this.state
																		.searchTerm
																);
																return this.searchRow(
																	this.state
																		.searchTerm
																);
															}
														}}
													/>
													{this.state
																.tipoInforme ===
														  "2" ? (
															<div><br></br><br></br></div>
														) : (
															<p></p>
														)}
												</div>
												{/* <div className="col-lg-3"><input className="form-control buscador fontAwesome " placeholder="&#xF002; Buscar Producto" value={this.state.text} onChange={(text) => this.filter(text, products)} /></div> */}
											</div>

											<div className="">
												<TableReport className="Product-table Local striped up">
													{this.state.tipoInforme ===
													"0" ? (
														<THeadReport
															tipoInforme={
																this.state
																	.tipoInforme
															}
															{...this.props}
															className=""
														/>
													) : (
														<p className="esconder"></p>
													)}
													{this.state.tipoInforme ===
													"1" ? (
														<THeadPersonalizado
															tipoInforme={
																this.state
																	.tipoInforme
															}
															{...this.props}
														/>
													) : (
														<p className="esconder"></p>
													)}
													{this.state.tipoInforme ===
													"2" ? (
														<THeadAjusteOperador
															tipoInforme={
																this.state
																	.tipoInforme
															}
															{...this.props}
														/>
													) : (
														<p className="esconder"></p>
													)}
													{this.state.tipoInforme ===
													"3" ? (
														<THeadAjusteDañado
															tipoInforme={
																this.state
																	.tipoInforme
															}
															{...this.props}
														/>
													) : (
														<p className="esconder"></p>
													)}
													{this.state.tipoInforme ===
													"0" ? (
														<TBodyReport
															data={
																this.state
																	.rowListFilter
															}
															emirece="emisor"
															handleViewTicket={
																this
																	.handleViewTicket
															}
															handleClickGetCreditNoteData={
																this
																	.handleClickGetCreditNoteData
															}
															esto={
																this.state.total
															}
															format={
																this.toCurrency
															}
														/>
													) : (
														<p className="esconder"></p>
													)}
													{this.state.tipoInforme ===
													"1" ? (
														<TBodyAjustePerso
															data={
																this.state
																	.rowListFilter
															}
															emirece="emisor"
															handleViewTicket={
																this
																	.handleViewTicket
															}
															handleClickGetCreditNoteData={
																this
																	.handleClickGetCreditNoteData
															}
															esto={
																this.state.total
															}
															format={
																this.toCurrency
															}
															tipoInforme={
																this.state
																	.tipoInforme
															}
														/>
													) : (
														<p className="esconder"></p>
													)}
													{this.state.tipoInforme ===
													"2" ? (
														<TBodyAjusteOp
															data={
																this.state
																	.rowAjusteOperador
															}
															emirece="emisor"
															handleViewTicket={
																this
																	.handleViewTicket
															}
															handleClickGetCreditNoteData={
																this
																	.handleClickGetCreditNoteData
															}
															total={
																this.state.total
															}
															format={
																this.toCurrency
															}
														/>
													) : (
														<p className="esconder"></p>
													)}
													{this.state.tipoInforme ===
													"3" ? (
														<TBodyAjusteMerma
															data={
																this.state
																	.rowListFilter
															}
															emirece="emisor"
															handleViewTicket={
																this
																	.handleViewTicket
															}
															handleClickGetCreditNoteData={
																this
																	.handleClickGetCreditNoteData
															}
															total={
																this.state.total
															}
															format={
																this.toCurrency
															}
														/>
													) : (
														<p className="esconder"></p>
													)}
													{/* <div className="row">
														{ this.state .tipoInforme !=="1" ?
														<div className="col-lg-12 greenResultCentral">
															{
																<p>
																	Se han
																	encontrado{" "}
																	{this.state
																		.tipoInforme ===
																	"2" 
																		? this
																				.state
																				.cantidadLineas
																		: this
																				.state
																				.qtyLocal}{" "}
																	Resultados
																</p>
															} 
														</div> :
															<div className="col-lg-12 greenResultCentralDMF">
															{
																<p>
																	han
																	encontrado{" "}
																	{this.state
																		.tipoInforme ===
																	"2" 
																		? this
																				.state
																				.cantidadLineas
																		: this
																				.state
																				.qtyLocal}{" "}
																	Resultados
																</p>
															} 
														</div> }
													</div> */}
													<p></p>
												</TableReport>
											</div>
										</div>
									)}
								</div>
							</div>
							<div className="row">
								<div className="col d-flex justify-content-end Excel-Button bigFootCentral">
									<ButtonInformeExcel
										rowList={this.state.rowList}
										rowOperador={this.state.datosAjusteOp}
										sumaTotal={this.state.sumaTotal}
										informeType={parseInt(
											this.state.tipoInforme
										)}
										element={
											<div className="">
												<a class="waves-effect waves-light btn Raised u-bg-green excelH">
													<i className="fal fa-file-excel right "></i>
													Descargar Excel
												</a>
											</div>
										}
										cantidadLineas={
											this.state.sumaDif
										}
									/>
								</div>
							</div>
						</Fragment>
					)}
				</div>

				<Modal
					otherClassName="Modal-LastNC small"
					id="modalReport" /*Develop={true}*/
				>
					<ModalHeader
						icon="fal fa-file-invoice"
						title="Detalle Nota de Crédito"
						color="success"
					/>
					<ModalMain>
						<div className="col-lg-1 ej">
							<MainCardLead
								title="Boleta"
								spark="true"
								className="ej"
							/>
						</div>
						<TableReport className=" mt-3 ReportDetalle-table striped">
							<THeadReportDetail thead={tHeadDetailO.tableOne} />
							<TBodyReportDetailOne
								LocalB={this.state.detailLocalB}
								EmisorB={this.state.emisorB}
								ReceptorB={this.state.receptorB}
								FechaB={this.state.fechaB}
								MercadoB={this.state.mercadoB}
								RutCl={this.state.rutCl}
								FolioB={this.state.folioB}
								FormatoB={this.state.formatoB}
								MercadoB={this.state.mercadoB}
								format={this.toCurrency}
							/>
						</TableReport>
						
						<div className="row px-3">
							<div className="col-lg-2 titW ">
								<MainCardLead
									title="Nota de Crédito"
									spark="true"
								/>
							</div>
							<div className="col-lg-8"></div>
							{this.state.tipoInforme === "1" ? (
								<div className="col-lg-2 titWW prI">
									<MainCardLead
										title="Productos"
										spark="true"
									/>
								</div>
							) : (
								<div className="col-lg-2 titWW pr" style={{marginLeft:"-10%"}}>
									<MainCardLead
										title="Productos"
										spark="true"
									/>
								</div>
							)}
						</div>
						<TableReport className=" mt-3 ReportDetalle-table striped">
							{this.state.tipoInforme === "1" ? (
								<THeadReportDetail
									thead={tHeadDetailItem.tableTwo}
								/>
							) : (
								<THeadReportDetail
									thead={tHeadDetail.tableTwo}
								/>
							)}
							<TBodyReportDetailTwo
								detailListProducts={
									this.state.detailListProducts
								}
								detailListOthers={this.state.otherDetails}
								format={this.toCurrency}
								tipoInforme={this.state.tipoInforme}
							/>
						</TableReport>

						{/* hasta aqui el footer */}
					</ModalMain>
				

					<ModalFooter>
						<div className="footOptions footModalCentral">
							<br></br>
							<div className="row mt-3 d-flex justify-content-between">
							<div className="col-md-4 col-lg-4">

									<div className="cerrarBotonCentral">
										<ModalButtonRaised
											name="Cerrar"
											className="ReportDetalle-btn-cerrar"
											 style={{padding:'8%', paddingRight:'19%', paddingLeft:'19%', marginTop:'3%'}}
										/>
									</div>
								
							</div>


							<div className="col-md-2 col-lg-2 mt-md-0 mt-3 center-align">
								<button
									className="waves-effect waves-light btn  mr-4 Raised  footModal  buttonNew some"
									onClick={() =>
										this.handleClickPrintNc(
											this.state.receptorB,
											this.state.printNcDate,
											this.state.printNcFolio,
											this.state.printNcCliente,
											this.state.printNcTienda,
											this.state.printNcVoucher,
											this.state.printTicked
										)
									}
								>
									Imprimir NC{" "}
									<i class="fal fa-print ml-2"></i>
								</button>
							</div>
							<div className="col-md-2 col-lg-2 resp">
								<ButtonPDFCentral
									rowList={this.state.rowListDetalle}
									sumaTotal={this.state.sumaTotalProducto}
									informeType={parseInt(
										this.state.tipoInforme
									)}
									isDetalle={true}
								/>
							</div>
							<div className="col-md-2  col-lg-2 mt-md-0 mt-3 center-align">
								<ButtonInformeExcel
									rowList={this.state.rowListDetalle}
									sumaTotal={this.state.sumaTotalProducto}
									informeType={parseInt(
										this.state.tipoInforme
									)}
									isDetalle={true}
									cantidadLineas={this.state.cantidadLineas}
								/>
							</div> </div>
						</div>
						
					</ModalFooter>
				</Modal>
			</div>
		);
	}
}
