import React from "react";
import { render } from "react-dom";
import PropTypes from "prop-types";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import conf from '../../config.json';
import InputLabel from '@material-ui/core/InputLabel';


const request = {
	apid: conf.access.apid
}

class Ciudades extends React.Component {

    constructor(props){
        super(props);
        this.state={
            ciudades:[],
            ciudadSelected:""
        }
        this.logChange=this.logChange.bind(this)

    }
  static propTypes = {
    value: PropTypes.string.isRequired,
    index: PropTypes.number.isRequired,
    change: PropTypes.func.isRequired
  };


 
  logChange = (event) => {
   // this.props.onSelectCiudad(event.target.value.nombre)
    console.log(event.target.value)
    localStorage.setItem("ciudad", event.target.value)
}


componentWillMount=() =>{
    var ciu=[]
    let token = JSON.parse(sessionStorage.getItem('user'));
    setTimeout(() => {
    fetch(conf.url.base + conf.endpoint.getCiudades, {
        method: 'post',
        body: JSON.stringify(request),
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token.access_token
        }
        
    }).then(res => res.json())
        .catch(function (error) {
            console.log('Error:', error);
        })
        .then(function (response) {
            console.log('Succes:', response);
            console.log()
            if (response != undefined) {
            component.setState({
                ciudades: response.ciudades
            })}
            
            if (response === undefined) {
                //setSubmitting(false);
             
                return console.log('Log: ', { errorCode: 1, errorMessage: 'No existe respuesta' })
            }
  }) }, 1200);
    var component = this; 

}


  render() {
    console.log(this.props)
    const { value, index, change } = this.props;
  
   
    return (
        <FormControl>
       <InputLabel id="demo-simple-select-label">Ciudad</InputLabel>
 
        <Select
          id="ciudad"
          value={value}
          style={{ fontSize: "inherit", width:"150px" }}   onChange={this.logChange} >
          {
          
          this.state.ciudades.map((motivo, index) => (
            <MenuItem key={index} value={motivo.nombre}>
              {motivo.nombre}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    );
  }
}

export default Ciudades;
