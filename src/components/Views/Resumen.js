import React, { PureComponent, Fragment } from 'react';
import M from 'materialize-css';
import { Form, Formik } from 'formik';
import InputField from '../Ui/Forms/InputField';
import Raised from '../Ui/Buttons/Raised';
import MainCardLead from '../Shared/Main/MainCardLead';
import boleta from '../../images/imagen.jfif';
import './Boleta.css';
import { setTimeout } from 'timers';
import Loader from '../Loader';
import { LocalService } from '../../services/LocalService';
import Modal from '../Shared/Modal/Modal';
import ModalHeader from '../Shared/Modal/ModalHeader';
import ModalMain from '../Shared/Modal/ModalMain';
import ModalFooter from '../Shared/Modal/ModalFooter';
import ModalButtonRaised from '../Shared/Modal/ModalButtonRaised';
import Outline from '../Ui/Buttons/Outline';
import { handleClickLogout,handleClickNote, Fn } from '../../Utils';
import conf from '../../config.json';
import { NoteService } from '../../services/NoteService';
import BreadCum from '../Shared/Main/BreadCum'
import { PropTypes } from 'react'
import { Ciudades } from './Ciudades';
import { withRouter } from 'react-router-dom';
import Moment from 'react-moment';
import swal from 'sweetalert';
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'
import { handleClickAllLogout } from '../../Utils';

const MySwal = withReactContent(Swal)
let stepper = React.createRef();
var classTr = "";
var classLoc = "";
var classCaj = "";
var classDa = "";

Moment.globalFormat = 'DD-MM-YYYY';
Moment.globalLocale = '';


export class Resumen extends PureComponent {
    displayName = Resumen.name

    constructor(props) {
        super(props);
        this.state = {
            isError: false,
            errorCode: 0,
            errorMessage: '',
            buttonClose: false,
            buttonReset: false,
            buttonNc: false,
            listNc: [],
            listNcCount: 0,
            activeStep: 2,
            completed: new Set(),
            skipped: new Set(),
            productos: [],
            inputRut: '',
            inputNombre: '',
            inputTelefono: '',
            inputEmail: '',
            inputDireccion: '',
            inputComuna: '',
            inputCiudad: '',
            inputRegion: '',
            nombre: '',
            rut: '',
            rutAtendido: '',
            isChange: true,
            description: '',
            clase: '',
            classCaj: "",
            classLoc: "",
            className: "",
            classTr: "",
            classDa: "",
            isError: false,
            errorCode: 0,
            errorMessage: '',
            buttonClose: false,
            buttonReset: false,
            buttonNc: false,
            listNc: [],
            listNcCount: 0,
            activeStep: 2,
            completed: new Set(),
            skipped: new Set(),
            messageErr: "",
            inputCaja: "",
            productos: [],
            totalAmount: 0,
            totalQty: '',
            isLoaded: false,
            devolucion: localStorage.getItem("tipoPagoDesc"),
            numT:0,
            productosNc:[]
        }



        this.handleClickBack = this.handleClickBack.bind(this)
        this.nextStep = this.nextStep.bind(this)
        this.reloadPage = this.reloadPage.bind(this)


    }



    reloadPage = () => {
        // setTimeout(() => {
        //     window.location.reload();
        // }, 700)
    }


    handleClickBack = () => {
        window.location.href = '/dmf/client'


    }

    setWidth = () => {
        this.props.setWidth("col-12 col-md-12 ")
    }

    toCurrency =(number) =>{
        const formatter = new Intl.NumberFormat("sv-SE", {
          style: "decimal",
          currency: "SEK"
        });
      
        console.log("EL FORMATIO=>", formatter.format(number))
        return formatter.format(number);
      }



    nextStep = () => {
       
        setTimeout(() => {
            //	this.setStateBreadcum({activeStep:2 });
            //props.handleNext();
            let token = JSON.parse(sessionStorage.getItem('user'));
            const request = {
                numLocal: localStorage.getItem("numLocal"),
                date: localStorage.getItem("date").replace("-", ""),
                pos: localStorage.getItem("pos"),
                transaction: localStorage.getItem("transaction"),
            }
            
            const prod=JSON.parse(localStorage.getItem("productosGuardados"))
            console.log("LOS PRODUCTOS==>",JSON.stringify(prod))

            //GUARDO LA NOTA DE CREDITO PARA PODER AGREGARLE EL TIPO DE PAGO
            const req = {
                numLocal: localStorage.getItem("numLocal"),
                idFile: localStorage.getItem("idFile")|| '',
                customerId:localStorage.getItem("rutCliente").replace("-","").replace(".","").replace(".",""),
                medioPago: JSON.parse(localStorage.getItem("medioPago")),
                originalTransaction: {
                    numLocal: localStorage.getItem("numLocal"),
                    date: localStorage.getItem("date"),
                    pos: localStorage.getItem("pos"),
                    transaction: localStorage.getItem("transaction")
                },
                products:prod,
                payment: {

                }
            }

            const productosSeleccionados = localStorage.getItem("prod")

            console.log("EL REQUEST ANTE DE guardAR LA NOTA")
            console.log(req)
            let desactivar = false
            if (request.arr === undefined | request.arr === null) {
                desactivar = true;
            }

            localStorage.setItem("pantallaProductos", JSON.stringify(request))
            let usr = JSON.parse(sessionStorage.getItem('user'));

            console.log("peticion=> "+JSON.stringify(req))
            fetch(conf.url.base + conf.endpoint.saveCreditNote, {
                method: 'post',
                body: JSON.stringify(req),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + usr.access_token
                }
            }).then(resp => resp.json())
                .catch(function (err) {
                    console.log('Error:', err);

                })
                .then(function (re) {
                
                }) 
  

            fetch(conf.url.base + conf.endpoint.injectCreditNote, {
                method: 'post',
                body: JSON.stringify(request),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token.access_token
                }
            }).then(res => res.json())
                .catch(function (error) {
                    console.log('Error:', error);
                })
                .then(response=> {
                    console.log("request del INJECT",JSON.stringify(request))
                    console.log('Succes INJECT:', response);
                    if (response === undefined) {
                        // setSubmitting(false);
                        return console.log('Log: ', { errorCode: 1, errorMessage: 'No existe respuesta' })
                    }

                    if (response.errorCode === 0) {
														localStorage.removeItem(
															"marcaResumen"
														);

														console.log(
															"eeeeeeeeeeestoooooooo"
														);
														console.log(request);
														console.log(
															localStorage.getItem(
																"customerId"
															)
														);
														let stepper = React.createRef();
														localStorage.setItem(
															"response",
															response
														);
														localStorage.setItem(
															"crearNota",
															true
														);

														if(this.state.devolucion==="efectivo"){
														window.location="/dmf/note?n=1"}
														else{
														    window.location="/dmf/note?n=2"
														}
														localStorage.setItem(
															"folio",
															response.folio
														);
														//		props.changeStep(3);
													} else {   if (response.errorCode === 0 || response.folio !== 0) {
                        console.log(JSON.stringify(req))
                         console.log('Succes en SaveCreditNote', response);
 
                     } else if (response.errorCode === 6) {
                         console.log('Error:', response);
                         localStorage.setItem("respuesta", "no");
                         console.log('Error:', response);
                         swal(response.errorMessage)
 
                     }
                     if (response.errorCode===5){
                         swal(response.errorMessage)
 
                     }
                     if ((response.errorCode!=5) && (response.errorCode!=6) && (response.errorCode!=0)){
                         swal(response.errorMessage)
 
                     }
                            
                    }
                })

              
        }, 1500);


    }

    handleNext = () => {
        console.log("antes")
        console.log(this.state)
        this.setState(state => ({
            activeStep: 2,

        }));
        console.log("entra a la funcion")
        this.forceUpdate()
        console.log("despues")
        console.log(this.state)

    }


    componentDidMount() {
        localStorage.removeItem("screenProducto")
        localStorage.setItem("screenCliente", "si")


        if (localStorage.getItem("productosSeleccionados")) {

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!

            var yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }

            var today = yyyy + mm + dd

            localStorage.setItem("hoy", today)


            this.setWidth()
            LocalService();
            console.log(localStorage.getItem("rutCliente"))
            const request = {
                customerId: localStorage.getItem("rutCliente")
            }
            const nose = {
                inputRut: '',
                inputNombre: '',
                inputTelefono: '',
                inputEmail: '',
                inputDireccion: '',
                inputComuna: '',
                inputCiudad: '',
                nombre: '',
                rut: '',
            }

            const productos = []

            setTimeout(() => {
                console.log("cosas",localStorage.getItem("comunaN"),localStorage.getItem("regionN"))
                const request = {
                    numLocal: localStorage.getItem("numLocal"),
                    date: localStorage.getItem("date").replace("-", ""),
                    pos: localStorage.getItem("pos"),
                    transaction: localStorage.getItem("transaction"),

                }
                let token = JSON.parse(sessionStorage.getItem('user'));

                fetch(conf.url.base + conf.endpoint.getCreditNote, {
                    method: 'post',
                    body: JSON.stringify(request),
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + token.access_token
                    }
                }).then(res => res.json())
                    .catch(function (error) {

                        console.log('Error:', error);
                    })
                    .then(response=> {
                        console.log('Succes:', response);
                        if (response === undefined) {
                            // setSubmitting(false);
                            return console.log('Log: ', { errorCode: 1, errorMessage: 'No existe respuesta' })
                        }

                        if (response.errorCode === 0) {
                            this.setState({ total: response.creditNote.total,
                                            productosNc: response.creditNote.products})
                            console.log("entro a cero")
                            console.log("LA NOTA DE CREDITO==>", response.creditNote)
                            localStorage.setItem("total", response.creditNote.total)
                            localStorage.setItem("totalQty", response.creditNote.totQty)

                            localStorage.removeItem("productos");
                            if (response.creditNote.reasonDescription!=undefined)
                            localStorage.setItem("motivoD",response.creditNote.reasonDescription)
                           console.log("el serv",response.creditNote.tipoPagoDescription)
                            this.setState({ devolucion:response.creditNote.tipoPagoDescription})

                            localStorage.setItem("productosH", JSON.stringify(response.creditNote.products));
                            console.log(localStorage.getItem("productosH"))


                        } else             
                              if(response.errorCode===-1){
                            MySwal.fire({
    
                                text: "Atención",
                                html:
                                    "<b>Atención</b><br><hr><br>" +
                                    '<h7>Su sesión ha expirado</h7>,<br>' +
                                    '<h7>Presiona aceptar para volver al inicio</h7> </div><br><br><hr>',
                                showCancelButton: true,
                                confirmButtonColor: '#fff',
                                cancelButtonColor: '#3085d6',
                                confirmButtonText: 'Cancelar',
                                cancelButtonText: 'Aceptar'
                            }).then((result) => {
                                handleClickAllLogout()
                                
                            });
                
                        }



                    })
            }, 300);


            setTimeout(() => {
                let token = JSON.parse(sessionStorage.getItem('user'));

                fetch(conf.url.base + conf.endpoint.getCustomer, {
                    method: 'post',
                    body: JSON.stringify(request),
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + token.access_token
                    }
                }).then(res => res.json())
                    .catch(function (error) {
                        console.log('Error:', error);
                    })
                    .then(function (response) {
                        console.log('Success:', response)
                        if (response.errorCode === 0 && !response.errorMessage) {
                            nose.nombre = response.nombre;
                            nose.rut = response.customerId;
                            nose.inputRut = response.customerId;
                            nose.inputNombre = response.nombre;
                            nose.inputTelefono = response.telefono;
                            nose.inputEmail = response.email;
                            nose.inputDireccion = response.direccion;
                            nose.inputComuna = response.comuna;
                            nose.inputCiudad = response.ciudad;
                            
                         
                          

                        } else {
                            console.log('Error:', response);

                        }
                    })
            }, 600)



            setTimeout(() => {

                let token = JSON.parse(sessionStorage.getItem('user'));
                let moment = require('moment');
                moment().format();
                console.log("El mtivo",nose.reasonDesc)

                this.setState({
                    inputRut: localStorage.getItem("rutCliente"),
                    inputNombre: localStorage.getItem("nombreCliente"),
                    inputTelefono: localStorage.getItem("telefono"),
                    inputEmail: localStorage.getItem("email"),
                    inputDireccion: localStorage.getItem("direccion"),
                    inputComuna: localStorage.getItem("comunaN"),
                    inputCiudad: localStorage.getItem("ciudad"),
                    inputRegion: localStorage.getItem("regionN"),
                    nombre: nose.nombre,
                    rutAtendido: token.nombre,
                    rut: nose.rut,
                    isChange: true,
                    description: 'Completa los datos requeridos',
                    inputCaja: localStorage.getItem("pos"),
                    clase: 'mostrarForm',
                    numLocal: localStorage.getItem("numLocal"),
                    date: localStorage.getItem("date"),
                    dateDevolution: moment,

                    totalQty: localStorage.getItem("totalQty"),
                    motivoDescripcion: localStorage.getItem("motivoD"),
                    numT:localStorage.getItem("transaction")


                })
            }, 300)



            let crearNota = localStorage.getItem("crearNota")

            //si esta RecargaResumen recarga y luego remueve el item para q sea solo una vez
            if (localStorage.getItem("recargarResumen") == "si") {
                console.log("ESTA ENTRANDO AL SI")
                setTimeout(() => {
                    //();


                }, 1800)
                localStorage.removeItem("recargarResumen")

            }
        }


    }



    obtenerParametros = () => {
        this.setState({ total: localStorage.getItem("total") })
        console.log("total=>=>=>=>=>=>=>=>=>=>=>", this.state.total)


    }


    openModalErr = (message, code) => {
        this.setState({
            isError: true,
            errorCode: code,
            errorMessage: message,
            buttonClose: code === 1 || 3 ? true : false,
            buttonReset: code === 3 ? true : false,
            buttonNc: code === 2 ? true : false
        })
        var elem = document.querySelector('.modal');
        var instance = M.Modal.init(elem);
        instance.open();
    }
    handleClickReset = () => {
        document.querySelector('form').reset();
    }
    responseNc = (response) => {
        this.setState({
            listNc: response
        })
    }
    handleClickNc = () => {
        var elem = document.querySelector('.Modal-LastNC');
        var instance = M.Modal.init(elem);
        instance.open();
    }
    handleClickPrintNc = (a, b, c) => {
        NoteService(a, b, c);
    }

    toCurrency=(n)=> {  
        let num= String(n).replace(/(.)(?=(\d{3})+$)/g,'$1,')
         return num.replace(",",".")
     
       }


    render() {
        const totalAmount = this.state.total
        const totalQty = this.state.totalQty
        let h = this.state.productos
        const Total = this.obtenerParametros()


        return (

            <div className="Card card Principal p-0 pad30">

                    <div className="Card-HeaderP card-titleP center-align ">
                        <span>
                            <BreadCum handleNext={this.handleNext.bind(this)} ></BreadCum>
                        </span>
                        <div id="root"></div>
                    </div>
                    <div className="Card-Main card-content px-5" onLoad="">

                    <div className="col-md-6 pl-md-5">
                        <MainCardLead title="Resumen de devolución de productos" spark={true} />
                        <div className="inputSpace"></div>
                    </div>


                    <div className="Card-Main card-content topDetalle">
                        <div className="card">
                            <div className="row  card-main widthHead">
                                <div className="col-lg-4  pl-md-5 ">Atendido por: {this.state.rutAtendido}</div>
                                <div className="col-lg-4">N° Local de Devolución: {this.state.numLocal}</div>
                                <div className="col-lg-4">Fecha de Devolución: <Moment>{localStorage.getItem("hoy")}</Moment></div>
                            </div>
                        </div>
                        <div className="row topDetalle">
                            <div className="col-lg-6 col-md-12 col-sm-12  rightw">
                                <div class="card cardone">
                                    <div className="card-header">
                                        <b className="centerTitle">Información de Cliente </b>
                                    </div>
                                    <div className="cardBody">
                                       
                                            <div className="row">
                                            <div className="col-md-5 col-lg-4 col-6 thtbl"> Nombre Cliente: </div><div className="col-md-7 col-lg-8 col-6 thtbl">  {this.state.inputNombre}</div>
                                            <div className="col-md-5 col-lg-4  col-4 thtbl">Correo Electrónico:</div><div className="col-md-7 col-lg-8 col-6 thtbl">{this.state.inputEmail}</div>
                                           <div className="col-md-4 col-lg-4 col-12 thtbl">Domicilio:</div><div className="col-md-8 col-lg-8 col-6 thtbl">{this.state.inputDireccion}, {this.state.inputCiudad}, {this.state.inputComuna}, {this.state.inputRegion}</div>
                                           <div className="col-md-5 col-lg-4 col-6">Tipo de Devolución:</div><div className="col-md-4 col-lg-8 col-6"> {this.state.devolucion}</div>
                                            </div> </div>
                                </div>
                            </div>
                            <div className="col-lg-6 col-md-12 col-sm-12 leftw">
                                <div class="card cardone boleta" >
                                    <div className="card-header">
                                        <b className="centerTitle">Información de Boleta</b>
                                    </div>
                                    <div className="cardBody row">

                                            <div className="col-md-6 col-lg-6 col-6 thtbl"> N° Transacción:</div><div className="col-md-6 col-lg-6 col-6 thtbl"> {this.state.numT} </div>
                                          <div className="col-md-6 col-lg-6 col-6 thtbl"> N° Local:</div><div className="col-md-6 col-lg-6 col-6 thtbl">{this.state.numLocal} </div>
                                        <div className="col-md-6 col-lg-6 col-6 thtbl">N° Caja: </div><div className="col-md-6 col-lg-6 col-6 thtbl" >{this.state.inputCaja} </div>
                                          <div className="col-md-6 col-lg-6 col-6">Fecha Boleta: </div><div className="col-md-6 col-lg-6 col-6"><Moment>{this.state.date}</Moment> </div>
                                       </div>
                                </div>
                            </div></div>
                        <div className="row quickDet">
                            <div className="col-md-12  col-lg-12 col-sm-12 ">
                                <div class="card Det" >
                                    <div className="card-header">
                                        <b className="centerTitle "> Detalle de Devolución</b>
                                    </div>
<div className=" cardbody scroll">
                                    <div class="row thtbl">
                                        <div class="col-md-2 p-l">
                                            <b className="p-3 centerContent">Código de Barra</b>
                                        </div>
                                        <div class="col-md-2">
                                            <b className="centerTitle prod"> Producto</b>
                                        </div>
                                        <div class="col-md-2">
                                            <b className="centerTitle precio">  Precio</b>
                                        </div>
                                        <div class="col-md-2">
                                            <b className="centerTitle precio"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Descuento</b>
                                        </div>

                                        <div class="col-md-2">
                                            <b className="centerTitle">&nbsp; Promoción</b>
                                        </div>

                                     

                                        <div class="col-md-1">
                                            <b className="centerTitle">&nbsp; &nbsp; Total</b>
                                        </div>
                                        
                                    </div>
                                    <div className="cardBody">


                                        {this.state.productosNc.map(producto => (
                                            

                                            <div class="row tbl">
                                                <div class="col-md-2 centerEan">
                                                    &nbsp;    {producto.ean}
                                                </div>
                                                <div class="col-md-2">
                                                &nbsp;&nbsp; {producto.description}

                                                </div>
                                                <div class="col-md-2">
                                                &nbsp; &nbsp;&nbsp; $ {this.toCurrency(producto.price)}
                                                </div>


                                                <div class="col-md-2">
                                             
                                                 <div style={{ marginLeft:"50px" }}>  $ {this.toCurrency(producto.discount)}</div>
                                                </div>

                                                <div class="col-md-2">
                                                    {producto.promoDescription}
                                                </div>

                                                <div class="col-md-2">
                                                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; $ {this.toCurrency(producto.total)}
                                                </div>
                                                <div class="col-md-3">
                                                </div>
                                            </div>))}
                                        <div class="row p-zero">
                                            <div class="col-md-3"></div>
                                            <div class="col-md-3"></div>
                                            <div class="col-md-2"></div>

                                            <div class="col-lg-2 col-8 col-md-8"><b>&nbsp; &nbsp;&nbsp; &nbsp;MONTO TOTAL:</b></div>
                                            <div class="col-lg-1 col-4 col-md-3" > &nbsp; &nbsp;   ${this.toCurrency(this.state.total)}
                                        </div>

                                        </div>
                                        <div className="row">
                                        <div className="col-md-8"></div>

                                        <div className="col-md-4"><b>&nbsp; &nbsp;&nbsp;&nbsp; MOTIVO DE DEVOLUCIÓN: </b>
                                        &nbsp; &nbsp; &nbsp;  &nbsp; {this.state.motivoDescripcion}</div>

                                        </div>
                                        <div className="row">
                                        <div className="col-md-8"></div>   
                                        </div>
                                    </div>

                                </div></div>
                            </div></div>

                            <div className="row quickDet  Responsivo">
                            <div className="col-md-12 col-sm-12 " style={{paddingLeft: '15px', paddingRight: '0px'}}>
                                <div class="card"  >
                                    <div className="card-header">
                                        <b className="centerTitle"> Detalle de Devolución</b>
                                    </div>
                                    <div className="cardBody">


                                        {this.state.productosNc.map(producto => (
                                            

                                            <div class=" tbl row">
                                                   <b className="col-7 p-4 centertitle p-4">Código de Barra:</b> <p className="col-4 detResp">  {producto.ean}</p> 
                                                <b className="col-7">Producto: </b><p className="col-4"> {producto.description}</p>

                                                <b className="col-8">Precio: </b> <p className="col-4 ">$ {this.toCurrency(producto.price)}</p>


                                             
                                                 <b  className="col-8"> Descuento: </b><p className="col-4"> $ {this.toCurrency(producto.discount)}</p>

                                                <b className="col-8"> Promoción: </b> <p className="col-4">{producto.promoDescription} </p>

                                                 <b className="col-8">   Total</b> <p className="col-4"> $ {this.toCurrency(producto.total)} </p>
                                                
                                                <div class="col-md-3">
                                                </div>
                                            </div>))}
                                        <div class="row">
        

                                            <div class="col-md-8 col-8 col-sm-6"><b>&nbsp; &nbsp;&nbsp; &nbsp;MONTO TOTAL:</b></div>
                                            <div class="col-md-2 col-4 col-sm-6">  ${this.toCurrency(this.state.total)}
                                        </div>

                                        </div>
                                        <div className="row">
                                        <div className="col-md-8"></div>

                                        <div className="col-md-4"><b>&nbsp; &nbsp;&nbsp;&nbsp; MOTIVO DE DEVOLUCIÓN: </b>
                                        &nbsp; &nbsp; &nbsp;  &nbsp; {this.state.motivoDescripcion}</div>

                                        </div>
                                        <div className="row">
                                        <div className="col-md-8"></div>   
                                        </div>
                                    </div>

                                </div>
                            </div></div>




                        <div className="row centerTitle">

                            {/* <div className="col-lg-3">
							<button className="Raised btn waves-effect waves-light px-5 pt-2 pb-2 beforeButton">Anterior</button>
						</div> */}							<div className="row justify-content-center mt-5">

                                {/* <Raised isSubmit={true} disabled text='Finalizar' isIcon={false}></Raised> */}
                            </div>
                            <div className="col-md-2 col-lg-3 col-4 col-sm-3"><button className="Raised btn waves-effect waves-light px-5 pt-2 pb-2 anterior botonAntResumen" onClick={() => this.handleClickBack()}>Anterior</button></div>
                            <div className="col-md-7 col-lg-7 col-2 col-sm-4"></div>
                            <div className="col-md-2 col-lg-2 col-3 col-sm-3"><button isIcon={false} className="Raised btn waves-effect waves-light px-5 pt-2 pb-2 anterior" onClick={() => this.nextStep()}> Generar </button></div>

                            <div className="col-lg-6">

                            </div>
                            <div className="col-lg-3">
                            </div>

                        </div>

                    </div> </div>


                <Modal size="small">
                    <ModalHeader
                        icon="fal fa-exclamation-circle"
                        title="Atención"
                        color="warning"
                    />
                    <ModalMain>
                        <h6>{this.state.errorMessage}</h6>
                        <hr className="mt-5" />
                    </ModalMain>
                    <ModalFooter>
                        {
                            this.state.buttonNc
                                ?
                                <Fragment>
                                    <Outline className="mr-2 green" name="Ver NC Anteriores" handleClick={this.handleClickNc} />
                                    <Outline className="mr-2 orange" name="Hacer otra devolución" handleClick={this.handleClickReset} />
                                </Fragment>
                                :
                                null
                        }
                        {
                            this.state.buttonReset ? <Outline className="mr-2 orange" name="Nueva NC" handleClick={this.handleClickReset} /> : null
                        }
                        <ModalButtonRaised name={this.state.errorCode === 2 ? "Cerrar Sesión" : "Cerrar"} handleClick={this.state.errorCode === 2 ? () => handleClickLogout(this.props) : null} />
                        {/* {
							this.state.buttonClose
							?
								<ModalButtonRaised name="Cerrar" />

							:
								null
						} */}
                    </ModalFooter>
                </Modal>


                <Modal otherClass="Modal-LastNC medium" /*Develop={true}*/>
                    <ModalHeader
                        icon="fal fa-file-invoice"
                        title="Notas de crédito anteriores"
                        color="success"
                    />
                    <ModalMain>
                        <table className="LastNC-table responsive-table" onChange={this.reloadPage}>
                            <thead>
                                <tr>
                                    <th>RUT Cliente</th>
                                    <th>Nro Local</th>
                                    <th>Fecha</th>
                                    <th>Folio</th>
                                    <th>Monto</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                {

                                    this.state.listNc.map((item, index) => {
                                        this.setState({ listNcCount: this.state.listNc.length })
                                        return (
                                            <tr key={item.folio}>
                                                <td>{Fn.formateaRut(item.customerId)}</td>
                                                <td>{item.numLocal}</td>
                                                <td>{Fn.formatDate(item.date)}</td>
                                                <td>{item.folio}</td>
                                                <td>{"$" + new Intl.NumberFormat().format(item.total)}</td>
                                                <td className="text-right">
                                                    <button className="Raised btn waves-effect waves-light px-5 pt-2 pb-2" onClick={() => this.handleClickPrintNc(item.numLocal, item.date, item.folio)}>Imprimir</button>
                                                </td>
                                            </tr>
                                        )
                                    })
                                }
                            </tbody>
                        </table>

                        <div className="text-right">
                            <small className=""><b>{this.state.listNcCount}</b> registros encontrados</small>
                        </div>

                    </ModalMain>
                    <ModalFooter>
                        <Outline className="mr-2 orange" name="Hacer otra devolución" handleClick={this.handleClickReset} />
                        <ModalButtonRaised name="Cerrar Sesión" handleClick={() => handleClickLogout(this.props)} />
                    </ModalFooter>
                    <div id="step">3</div>
                </Modal>


            </div>

        )
    }
}
export default withRouter(Resumen)