import React, { Component } from 'react';
import { render } from 'react-dom';
// import Hello from './Hello';
import './style.css';

export class Search extends Component {
  constructor() {
    super();
    this.state = {
     productos: [],
      items: []
    };

    this.filterList = this.filterList.bind(this);

  }

  componentWillMount(){
    this.setState({
      items: this.state.initialItems
    });

    const request={
        numLocal: localStorage.getItem("numLocal"),
        date:localStorage.getItem("date").replace("-", ""),
        pos: localStorage.getItem("pos"),
        transaction: localStorage.getItem("transaction"),

        }

           setTimeout(() =>{
        fetch(conf.url.base + conf.endpoint.getCreditNote, {
            method: 'post',
            body: JSON.stringify(request),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token.access_token
            }
        }).then(res => res.json())
            .catch(function (error) {
               
                
                console.log('Error:', error);
            })
            .then(function (response) {
                if (response === undefined) {
                    // setSubmitting(false);
                    return console.log('Log: ', { errorCode: 1, errorMessage: 'No existe respuesta' })
                }

                if (response.errorCode === 0) { 
             
                   // retorna el arreglo
                    // let arreglo=Object.keys(JSON.parse(response.creditNote.products))
                   localStorage.setItem("productos",JSON.stringify(response.creditNote.products));
            
                 
                    
                } else {
                    // setSubmitting(false);
                    console.log('Error:', response);} })
                
                }, 1000);
                
                this.setState({initialItems: localStorage.getItem("productos")})
            
            } 

  filterList(e){
    let updateList = this.state.initialItems;
    updateList = updateList.filter(item => {
      return item.toLowerCase().search(
        e.target.value.toLowerCase()
        ) !== -1;
    });

    this.setState({
      items: updateList
    });
  }

  render() {
    return (
      <React.Fragment>
        <input type="text" onChange={this.filterList} />
        <ul>
          {this.state.items.map(item => {
            return <li key={item}>{item}</li>
          })}
        </ul>
      </React.Fragment>
    );
  }
}