import React from "react";
import { render } from "react-dom";
import PropTypes from "prop-types";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import conf from '../../config.json';
import InputLabel from '@material-ui/core/InputLabel';

const request = {
	apid: conf.access.apid
}

class TipoPago extends React.Component {

    constructor(props){
        super(props);
        this.state={
            payment:[],
        }

        console.log(this.state)

    };

  static propTypes = {
    value: PropTypes.string.isRequired,
    index: PropTypes.number.isRequired,
    change: PropTypes.func.isRequired
  };



  change=(e,index)=>{
  console.log(e, index)
  }
 
  componentDidMount=() =>{

    const request={
        numLocal: localStorage.getItem("numLocal"),
        date: localStorage.getItem("date").replace("-", "").replace("-", ""),
        pos: localStorage.getItem("pos"),
        transaction: localStorage.getItem("transaction")
      }
    let token = JSON.parse(sessionStorage.getItem('user'));
    setTimeout(()=>{
    fetch(conf.url.base + conf.endpoint.getTransaction, {
        method: 'post',
        body: JSON.stringify(request),
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token.access_token
        }
    }).then(res => res.json())
        .catch(function (error) {
            console.log('Error:', error);
        })
        .then(function (response) {
            console.log('Succes:', response);
           // props.handleNext(3);
            if (response === undefined) {
                return console.log('Log: ', { errorCode: 1, errorMessage: 'No existe respuesta' })
            }

            if (response.errorCode === 0) {
                let stepper = React.createRef();
                component.setState({
                    payment: response.tiposPago
                })
                console.log(response.tiposPago)
               
            } else {
                console.log('Error:', response);

            }
        })  },1000)
    var component = this;

}
  render() {
    console.log("TODO",this.props.defaultValue)
    const { value, index, change,defaultValue,handleTipoPago,id } = this.props;


    // const es=JSON.parse(localStorage.getItem("tipoPago"))
  
   console.log("valor tipopago",this.props)
    return (
        <FormControl  className="col-lg-12">
                 <InputLabel id="demo-simple-select-label">Tipo de Devolución*</InputLabel>

        <Select
          value={value}
          style={{ fontSize: "inherit", width:"100%", marginTop:"20px" }}
          onChange={handleTipoPago}
          id={id}
          defaultValue={defaultValue}
          required
          defaultValue={"selecciona una opcion"}
          >
          {
 
          this.state.payment.map((pay, id) => (
            <MenuItem  value={pay.id}   >
              {pay.nombre}  
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    );
  }
}

export default TipoPago;
