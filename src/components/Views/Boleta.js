import React, { PureComponent, Fragment } from 'react';
import M from 'materialize-css';
import { Form, Formik } from 'formik';
import InputField from '../Ui/Forms/InputField';
import Raised from '../Ui/Buttons/Raised';
import MainCardLead from '../Shared/Main/MainCardLead';
import boleta from '../../images/imagen.jfif';
import './Boleta.css';
import { setTimeout } from 'timers';
import Loader from '../Loader';
import { LocalService } from '../../services/LocalService';
import Modal from '../Shared/Modal/Modal';
import ModalHeader from '../Shared/Modal/ModalHeader';
import ModalMain from '../Shared/Modal/ModalMain';
import ModalFooter from '../Shared/Modal/ModalFooter';
import ModalButtonRaised from '../Shared/Modal/ModalButtonRaised';
import Outline from '../Ui/Buttons/Outline';
import { handleClickLogout, Fn } from '../../Utils';
import conf from '../../config.json';
import { NoteService } from '../../services/NoteService';
import BreadCum from '../Shared/Main/BreadCum'
import { PropTypes } from 'react'
import { Ciudades } from './Ciudades';
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'
import { CleanCreditNoteService } from '../../services/CleanCreditNoteService';
import Moment from 'react-moment';
import { handleClickAllLogout } from '../../Utils';
const MySwal = withReactContent(Swal)
const MySwal2 = withReactContent(Swal)

let stepper = React.createRef();
var classTr = "";
var classLoc = "";
var classCaj = "";
var classDa = "";


Moment.globalFormat = 'D-MM-YYYY';
Moment.globalLocale = '';
let moment = require('moment');
let fec= moment(localStorage.getItem("date")).format('D/MM/YYYY');
let fechaDefault =  fec.replace("/","-").replace("/","-")

const FormBoleta = (props) => (
	<Formik

		initialValues={{

			inputNumTransaccion: '',
			inputLocal: '',
			inputCaja: '',
			inputFecha: ''
		}}
		validate={values => {
			let errors = {};

			if (!values.inputNumTransaccion) {
				errors.inputNumTransaccion = "Dato Incorrecto";
				this.classTr = "usuario-error"
			}
			else if (values.inputNumTransaccion.toString().length > 4) {
				errors.inputNumTransaccion = "El campo no puede superar los 4 caracteres";
				this.classTr = "usuario-error"
			} else {
				this.classTr = ""
			}
			if (!values.inputLocal) {
				errors.inputLocal = "Dato Incorrecto";
				this.classLoc = "usuario-error";
			}
			else if (values.inputLocal.toString().length > 5) {
				errors.inputLocal = "El campo no puede superar los 5 caracteres";
				this.classLoc = "usuario-error";
			} else {

				this.classLoc = ""
			}

			if (!values.inputCaja) {
				errors.inputCaja = "Dato Incorrecto";
				this.classCaj = "usuario-error";

			}
			else if (values.inputCaja.toString().length > 3) {
				errors.inputCaja = "El campo no puede superar los 3 caracteres";
				this.classCaj = "usuario-error";

			} else {
				this.classCaj = "";
			}
			if (!values.inputFecha) {
				errors.inputFecha = "Dato Incorrecto";
				this.classDa = "usuario-error"
			} else {
				this.classDa = ""
			}

			return errors;
		}


		}
		onSubmit={(values, { setSubmitting, setErrors }) => {
		
			
			let totalProd=0
			const request = {
				numLocal: values.inputLocal,
				date: values.inputFecha.replace("-", "").replace("-", ""),
				pos: values.inputCaja,
				transaction: values.inputNumTransaccion
			}

			localStorage.setItem("numLocal", request.numLocal);
			localStorage.setItem("date", request.date.replace("-", ""));
			localStorage.setItem("pos", request.pos)
			localStorage.setItem("transaction", request.transaction);

		

		

			setTimeout(() => {
				let token = JSON.parse(sessionStorage.getItem('user'));
				fetch(conf.url.base + conf.endpoint.getCreditNote, {
					method: 'post',
					body: JSON.stringify(request),
					headers: {
						'Content-Type': 'application/json',
						'Authorization': 'Bearer ' + token.access_token
					}
				}).then(res => res.json())
					.catch(function (error) {
					})
					.then(function (response) {
						console.log(response)
						props.handleNext(3);
						if (response === undefined) {
							setSubmitting(false);
							return console.log('Log: ', { errorCode: 1, errorMessage: 'No existe respuesta' })
						}

						if (response.errorCode === 0) {

							if (response.creditNote.products) {
								if (localStorage.getItem("swal")!=null){
								totalProd=response.creditNote.productos;
								MySwal.fire({

									text: "Atención",
									html:
										"<b>Atención</b><br><hr><br>" +
										'<h7> Esta boleta tiene una devolución sin terminar</h7>,<br>' +
										'<h7>¿Quieres <b>Retomar</b> o <b>Eliminar</b> la devolución sin terminar?</h7> </div><br><br><hr>',
									showCancelButton: true,
									confirmButtonColor: '#fff',
									cancelButtonColor: '#3085d6',
									confirmButtonText: 'Eliminar',
									cancelButtonText: 'Retomar'
								}).then((result) => {
									if (result.value) {
										localStorage.removeItem("numLocal")
										localStorage.removeItem("date")
										localStorage.removeItem("pos")
										localStorage.removeItem("transaction")
										localStorage.removeItem("rutCliente")
										localStorage.removeItem("marcaResumen")
										localStorage.removeItem("screenProducts")
										localStorage.removeItem("screenCliente")
										localStorage.removeItem("swal")
										window.location.reload()



									} 
									else {
										if(localStorage.getItem("screenProducto")==="si"){
											window.location.href="/dmf/product"
										}
										if (localStorage.getItem("screenCliente")==="si"){
										window.location.href = "/dmf/product" }

										else{
											window.location.href = "/dmf/product"
										}

									
								

									}
								});
					


							};}
						
						}
						

							fetch(conf.url.base + conf.endpoint.getTransaction, {
								method: 'post',
								body: JSON.stringify(request),
								headers: {
									'Content-Type': 'application/json',
									'Authorization': 'Bearer ' + token.access_token
								}
							}).then(res => res.json())
								.catch(function (error) {
								})
								.then(function (response) {
							
									if (response === undefined) {
										setSubmitting(false);
										return console.log('Log: ', { errorCode: 1, errorMessage: 'No existe respuesta' })
									}
			
									//si existe la boleta pero no tiene productos la nota de credito, entonces a vamos a productos
									if ((response.errorCode === 0) && (totalProd===0)) {
										window.location.href="/dmf/product"
			
									} else {
			
										setSubmitting(false);
										console.log('Error:', response);
			
										switch (response.errorCode) {
											case 1:
												// props.openModalErr(response.errorMessage, response.errorCode)
												MySwal2.fire({
													icon: 'error',
													title: 'Esta Boleta No existe',
													footer: '<button class="Raised btn waves-effect" onClick=swal.close()>Cerrar</button>',
													showCloseButton: false,
													showCancelButton: false,
													focusConfirm: false,
													showConfirmButton:false,
													timer:1900

												  })
												break;
											case 2:
												MySwal2.fire({
													icon: 'warning',
													title: 'Lo sentimos, esta boleta no tiene productos para devolver',
													footer: '<button class="Raised btn waves-effect" onClick=swal.close()>Cerrar</button>',
													showCloseButton: false,
													showCancelButton: false,
													focusConfirm: false,
													showConfirmButton:false,
													timer:2500

												  })
												  break;
											 case -1:
												MySwal2.fire({
    
													text: "Atención",
													html:
														"<b>Atención</b><br><hr><br>" +
														'<h7>Su sesión ha expirado</h7>,<br>' +
														'<h7>Presiona aceptar para volver al inicio</h7> </div><br><br><hr>',
													showCancelButton: true,
													confirmButtonColor: '#fff',
													cancelButtonColor: '#3085d6',
													confirmButtonText: 'Cancelar',
													cancelButtonText: 'Aceptar'
												}).then((result) => {
													handleClickAllLogout()
													
												});
												break;
											case 3:
												MySwal2.fire({
													icon: 'warning',
													title: 'Lo sentimos, esta boleta no tiene productos para devolver',
													footer: '<button class="Raised btn waves-effect" onClick=swal.close()>Cerrar</button>',
													showCloseButton: false,
													showCancelButton: false,
													focusConfirm: false,
													showConfirmButton:false,
													timer:2500

												  })
												break;
											default:
												setErrors({ inputFecha: response.errorMessage })
												break;
										}
			
			
									}
								})
			


					


					})


			}, 1000);

		}}
		render={props => (

			<Form>
				<div className="row justify-content-center">
					<div id="step" className="inputSpace">0</div>
					<div className="col" >
						<div>
							<InputField
								id="inputNumTransaccion"
								type="text"
								name="inputNumTransaccion"
								label="1. N° Transacción*"
								onKeyDown={Fn.keyPreventTrans}
								max="9999"
								min="1"
								labelclass="active"
							/>
							<div className="row">
								<div className="col">
									<InputField
										id="inputLocal"
										type="text"
										name="inputLocal"
										label="2. N° Local*"
										yio="m-0"
										onKeyDown={Fn.keyPrevent}
										max="99999"
										min="1"
										labelclass="active"
									/>
									{/* <InputFieldAutocomplete {...props} /> */}
								</div>
								<div className="col">
									<InputField
										id="inputCaja"
										type="text"
										name="inputCaja"
										label="3. N° Caja*"
										yio="m-0"
										onKeyDown={Fn.keyPreventCaja}
										max="999"
										min="1"
										labelclass="active"
									/>
								</div>
							</div>
							<InputField
								id="inputFecha"
								type="date"
								name="inputFecha"
								label="4. Fecha boleta*"
								labelclass="active"
								defaultValue={JSON.stringify(fechaDefault)}

							/>

							
						</div>
						<div className="col-lg-6">
							<p className="required">Campos Obligatorios*</p>
						</div>
					</div>
				</div>
				<span className="topButton">
					<div className="row topButton">

						<div className="col-lg-3 col-md-3 col-sm-3">
						</div>
						<div className="col-lg-4 col-md-3 col-sm-3">

						</div>
						<div className="col-lg-3 col-md-3 col-sm-3">
							<span >
								<Raised isSubmit={true} disabled={props.isSubmitting} text={props.isSubmitting === true ? <Loader /> : 'Siguiente'} isIcon={false}></Raised>
							</span>
						</div>
						<div className="col-lg-1"></div>

					</div></span>
			</Form>
		)}
	/>
)

export class Boleta extends PureComponent {
	displayName = Boleta.name
	state = {
		classCaj: "",
		classLoc: "",
		className: "",
		classTr: "",
		classDa: "",
		isError: false,
		errorCode: 0,
		errorMessage: '',
		buttonClose: false,
		buttonReset: false,
		buttonNc: false,
		listNc: [],
		listNcCount: 0,
		activeStep: 2,
		completed: new Set(),
		skipped: new Set(),
	}






	constructor(props) {
		super(props);
		this.state = {
			isError: false,
			errorCode: 0,
			errorMessage: '',
			buttonClose: false,
			buttonReset: false,
			buttonNc: false,
			listNc: [],
			listNcCount: 0,
			activeStep: 2,
			completed: new Set(),
			skipped: new Set(),
		};


		// create a ref to store the textInput DOM element
	}


	setWidth = () => {
		this.props.setWidth("col-12 col-md-12 ")
	}
	componentDidMount() {
		localStorage.setItem("reload","si")
		this.setWidth()
		LocalService();
		//	this.changeStep(2);


	}

	handleNext = () => {
		console.log("antes")
		console.log(this.state)
		this.setState(state => ({
			activeStep: 2,

		}));
		console.log("entra a la funcion")
		this.forceUpdate()
		console.log("despues")
		console.log(this.state)

	}

	openModalErr = (message, code) => {
		this.setState({
			isError: true,
			errorCode: code,
			errorMessage: message,
			buttonClose: code === 1 || 3 ? true : false,
			buttonReset: code === 3 ? true : false,
			buttonNc: code === 2 ? true : false
		})
		var elem = document.querySelector('.modal');
		var instance = M.Modal.init(elem);
		instance.open();
	}
	handleClickReset = () => {
		document.querySelector('form').reset();
	}
	responseNc = (response) => {
		this.setState({
			listNc: response
		})
	}
	handleClickNc = () => {
		var elem = document.querySelector('.Modal-LastNC');
		var instance = M.Modal.init(elem);
		instance.open();
	}
	handleClickPrintNc = (a, b, c) => {
		NoteService(a, b, c);
	}
	render() {
		return (
			<div className="pad30">
			<div className="Card card cardboleta p-0">
				<div className="Card-HeaderP card-titleP center-align">
					<span>
						<BreadCum handleNext={this.handleNext.bind(this)} ></BreadCum>
					</span>
					<div id="root"></div>
				</div>
				<div className="Card-Main card-content px-5">
					<div className="row">
						<div className="col-md-6 col-sm-12 pr-md-5 u-br-1">
							<img src={boleta} className="responsive-img mb-4 z-depth-2" alt="Boleta Walmart" width={850} height={990} />
						</div>


						<div className="col-md-6 col-sm-12 pl-md-5 pt-md-0 pt-4">
							<div className="row justify-content-center">
								<div className="col-lg-11 col-md-12 col-sm-12" >

									<MainCardLead title="Completar los datos de la boleta del cliente" spark={true} />
									<div className="inputSpace"></div>
									<FormBoleta {...this.props} {...this} openModalErr={this.openModalErr} responseNc={this.responseNc} />
								</div>
							</div>

						</div>
					</div>
					<div className="row ">

						{/* <div className="col-lg-3">
							<button className="Raised btn waves-effect waves-light px-5 pt-2 pb-2 beforeButton">Anterior</button>
						</div> */}
						<div className="col-lg-6">

						</div>
						<div className="col-lg-3">
						</div>

					</div>

				</div>
				{/* <div className="Card-FooterF card-actionF p-0 pt-3 border-top-0">
					<span></span>
				</div> */}
				{/* <Modal size="small">
					<ModalHeader
						icon="fal fa-exclamation-circle"
						title="Atención"
						color="warning"
					/>
					<ModalMain>
						<h6>{this.state.errorMessage}</h6>
						<hr className="linea" />
					</ModalMain>
					<ModalFooter>
						{
							this.state.buttonNc
								?
								<Fragment>
									<Outline className="mr-2 green" name="Ver NC Anteriores" handleClick={this.handleClickNc} />
									<Outline className="mr-2 orange" name="Hacer otra devolución" handleClick={this.handleClickReset} />
								</Fragment>
								:
								null
						}
						{
							this.state.buttonReset ? <Outline className="mr-2 orange" name="Nueva NC" handleClick={this.handleClickReset} /> : null
						}
						<ModalButtonRaised name={this.state.errorCode === 2 ? "Cerrar Sesión" : "Cerrar"} handleClick={this.state.errorCode === 2 ? () => handleClickLogout(this.props) : null} />
					
					</ModalFooter>
				</Modal>
 */}

				<Modal otherClass="Modal-LastNC medium" /*Develop={true}*/>
					<ModalHeader
						icon="fal fa-file-invoice"
						title="Notas de crédito anteriores"
						color="success"
					/>
					<ModalMain>
						<table className="LastNC-table responsive-table">
							<thead>
								<tr>
									<th>RUT Cliente</th>
									<th>Nro Local</th>
									<th>Fecha</th>
									<th>Folio</th>
									<th>Monto</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
								{

									this.state.listNc.map((item, index) => {
										this.setState({ listNcCount: this.state.listNc.length })
										return (
											<tr key={item.folio}>
												<td>{Fn.formateaRut(item.customerId)}</td>
												<td>{item.numLocal}</td>
												<td>{Fn.formatDate(item.date)}</td>
												<td>{item.folio}</td>
												<td>{"$" + new Intl.NumberFormat().format(item.total)}</td>
												<td className="text-right">
													<button className="waves-effect waves-light btn btn-pink mr-4" onClick={() => this.handleClickPrintNc(item.numLocal, item.date, item.folio)}>Imprimir</button>
												</td>
											</tr>
										)
									})
								}
							</tbody>
						</table>
						{/* <div className="Card-FooterF card-actionF p-0 pt-3 border-top-0">
							<span></span>
						</div> */}
						<div className="text-right">
							<small className="mt-2"><b>{this.state.listNcCount}</b> registros encontrados</small>
						</div>
					</ModalMain>
					<ModalFooter>
						<Outline className="mr-2 orange" name="Hacer otra devolución" handleClick={this.handleClickReset} />
						<ModalButtonRaised name="Cerrar Sesión" handleClick={() => handleClickLogout(this.props)} />
					</ModalFooter>
				</Modal>
			</div></div>
		)
	}
}
