import React from "react";
import { Func } from '../../func/func';
import './tables.css'

export const TBodyAjusteOp = ({ data, ...props }) => {
	return (
		<tbody>
			{data &&
				data.map(item => {
					return (
						<tr>
							<td>
								<div className="line"></div>
								{item.loginLocal}
							</td>
							<td>{item.userid}</td>
							<td>{item.nombre}</td>
							<td>{Func.formatDate(item.date)}</td>
							<td>{"$" + String(item.montoDif).replace(/(.)(?=(\d{3})+$)/g, "$1,").replace(",",".") }</td>
							<td>{item.tipo}</td>
							<td>{item.motivoAjuste}</td>
							<td>{Func.formatRut(item.docNbr)}</td>
							<td>{Func.formatDate(item.date)}</td>
							<td>{Func.formatDate(item.date)}</td>
							<td>{item.codigoAjuste}</td>
							<td>{"$" +String(item.valorAjuste).replace(/(.)(?=(\d{3})+$)/g, "$1,").replace(",",".")}</td>

							<td>&nbsp;&nbsp;&nbsp; {item.moneda}</td>
							<td>&nbsp;&nbsp;&nbsp;&nbsp; {Func.formatDate(item.date)}</td>
							<td>&nbsp;&nbsp;&nbsp; {"$"+String(item.difAjustadaFinal).replace(/(.)(?=(\d{3})+$)/g, "$1,").replace(",",".")}</td>
						</tr>
					);
				})}
		</tbody>
	);
};
