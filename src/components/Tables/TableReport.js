import React from 'react';

export const TableReport = (props) => {
	return(
		<table {...props}>
			{props.children}
		</table>
	)
}
