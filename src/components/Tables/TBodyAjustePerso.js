import React from 'react';
import { Func } from '../../func/func';
import './tables.css'

export const TBodyAjustePerso = (props) => {
	const { data,tipoInforme,esto,className,classO } = props;

	if(esto!=undefined){
		localStorage.setItem("total",esto);

		
	}
	return(
		<tbody>
			{
				data.map(item => {
					return (
						<tr>
                            <td><div className=""> {item.originalTransaction.mercado}</div></td>

                            <td><div className=""> {item.originalTransaction.formato}</div></td>
							<td><div className=""> {item.originalTransaction.numLocal}</div></td>
							<td><div className=""> {Func.formatRut(item.customerId)}</div></td>
							<td><div className=""> {JSON.stringify(item.originalTransaction.folio).slice(0,8)}</div></td>
                            <td className=" line"><div className="">{Func.formatDate(item.originalTransaction.date)}</div></td>
							<td><div className=" s"> {item.originalTransaction.formato}</div></td>
                            <td><div className="">&nbsp;&nbsp;&nbsp;{item.mercado}&nbsp;</div></td>
                            <td><div className="">  {item.loginLocal}</div></td>
							<td><div className=""> {Func.formatRut(JSON.stringify(item.userid))}</div></td>
                            <td><div className="">  &nbsp; &nbsp;&nbsp; &nbsp;<p className="center">{item.originalTransaction.opr}</p></div></td>
                            <td><div className=""> <p>{item.reasonDescription}</p></div></td>
                            <td className=""><div className=""> <p>&nbsp; &nbsp;{JSON.stringify(item.folio).slice(0,8)}</p></div></td>
                            <td><div className=""> {Func.formatDate(item.date)}</div></td>
                            <td><div className=""></div> <p>{"$" + String(item.total).replace(/(.)(?=(\d{3})+$)/g, "$1,").replace(",",".")}</p></td>
							<td style={{width:'6%'}}><div className="" > <a href="#modalReport" className="modal-trigger" onClick={() => props.handleClickGetCreditNoteData(item, props.emirece)}><i className="fal fa-list-alt u-text-green"></i></a></div></td>
						</tr>
						
					)
				})
			}
				<tr><td><p className="monto"><b>TOTAL MONTO:</b> ${String(localStorage.getItem("total")).replace(/(.)(?=(\d{3})+$)/g, "$1,").replace(",",".")}</p></td></tr>

		</tbody>
	)
}
