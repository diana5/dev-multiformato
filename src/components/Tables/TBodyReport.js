import React from 'react';
import { Func } from '../../func/func';
import './tables.css'
export const TBodyReport = (props) => {


	const { data,tipoInforme,esto,className, classO,format} = props;
	if(esto!=undefined){
		localStorage.setItem("total",esto);
	}
	return(
		<tbody className="">
			{
				data.map(item => {
					return (
						<tr>
							<td><div className=""> {item.formato}</div></td>
							<td><div className=""> {item.mercado}</div></td>
							<td><div className="">&nbsp; {item.originalTransaction.numLocal}</div></td>
							<td><div className=""> {Func.formatRut(item.customerId)}</div></td>
							<td><div className=""> &nbsp; &nbsp;{JSON.stringify(item.originalTransaction.folio).slice(0,8)}</div></td>
							<td><div className=""> {Func.formatDate(item.originalTransaction.date)}</div></td>
							<td  className="line"><div className="px-3 px-44 valign-wrapper"> 
								{
									item.idFile
										? <a className="fileBoletaEn px-3 " onClick={() => props.handleViewTicket(item.idFile)}><i className="far fa-paperclip"></i></a>
										: <a className="fileBoleta-disabled px-3 " onClick={() => props.handleViewTicket(item.idFile)}><i class="far fa-paperclip"></i></a>
								}
							</div></td>
						
							<td><div className=""> {item.formato}</div></td>
							<td><div className=""> {item.mercado}</div></td>
							<td><div className="">{item.loginLocal}</div></td>
							<td><div className=""> <p className="rightL"> {Func.formatRut(JSON.stringify(item.userid))} </p></div></td>
							<td><div className=""> <p className="center">&nbsp; &nbsp;{item.originalTransaction.opr}</p></div></td>
							<td className="" ><div className=""> <p className="rightL">{JSON.stringify(item.folio).slice(0, 8)}</p></div></td>
							<td><div className="">  {Func.formatDate(item.date)} </div></td>
							<td><div className="">   &nbsp;&nbsp; $ {String(item.total).replace(/(.)(?=(\d{3})+$)/g, "$1,").replace(",",".")} </div></td>
							<td style={{width:'6%'}}><div className=""> <a href="#modalReport" className="modal-trigger" onClick={() => props.handleClickGetCreditNoteData(item, props.emirece)}><i className="fal fa-list-alt u-text-green"></i></a></div></td>

						</tr>
						
					)
				})
			}
				<tr><td><p className="monto"><b>TOTAL MONTO:</b> ${String(localStorage.getItem("total")).replace(/(.)(?=(\d{3})+$)/g, "$1,").replace(",",".")}</p></td></tr>

		</tbody>
	)
}
