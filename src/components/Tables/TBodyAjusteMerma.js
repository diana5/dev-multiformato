import React from 'react';
import { Func } from '../../func/func';
import './tables.css'

export const TBodyAjusteMerma = (props) => {
	const { className,data,tipoInforme,total, classO } = props;
	if(total!=undefined){
		localStorage.setItem("todo",total)
	}
	return(
		<tbody>
			{
				data.map(item => {
					return (
						<tr>
							<td >{item.formato}</td>
							<td>{item.mercado}</td>
							<td>{item.originalTransaction.numLocal}</td>
							<td>{Func.formatRut(item.customerId)}</td>
							<td>{item.originalTransaction.folio}</td>
							<td className="line">{Func.formatDate(item.originalTransaction.date)}</td>
							<td>{item.formato}</td>
							<td>{item.mercado}</td>
							<td>{item.loginLocal}</td>
							<td>{Func.formatRut(JSON.stringify(item.userid))}</td>
							<td >{item.originalTransaction.opr}</td>
							<td className="centro"><div ></div>{JSON.stringify(item.folio).slice(0,8)}</td>
							<td>{Func.formatDate(item.date)}</td>
							<td>$ {String(item.total).replace(/(.)(?=(\d{3})+$)/g, "$1,").replace(",",".")}</td>
							<td style={{width:'5%'}}><a href="#modalReport" className="modal-trigger" onClick={() => props.handleClickGetCreditNoteData(item, props.emirece)}><i className="fal fa-list-alt u-text-green"></i></a></td>

						</tr>
						
					)
				})
			}
				<tr><td><p className="monto"><b>TOTAL MONTO:</b> ${String(localStorage.getItem("todo")).replace(/(.)(?=(\d{3})+$)/g, "$1,").replace(",",".")}</p></td></tr>

		</tbody>
	)
}
