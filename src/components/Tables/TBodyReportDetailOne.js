import React from 'react';
import { Func } from '../../func/func';
import { Fn } from "../../Utils";


export const TBodyReportDetailOne = (props) => {
	const { LocalB, EmisorB, ReceptorB, FechaB, MercadoB, RutCl, FolioB, FormatoB } = props;
	

	return(
		<tbody>
			<tr>
				<td>
					<div className="px-3 valign-wrapper"><p>{FormatoB}</p></div>
				</td>
				<td>
					<div className="px-3 valign-wrapper"><p>{MercadoB}</p></div>
				</td>
				<td>
<div className="px-3 valign-wrapper"> &nbsp; &nbsp;&nbsp; &nbsp;<p>{ReceptorB}</p></div>
				</td>
				<td>
					<div className="px-3 valign-wrapper"><p>{Fn.formateaRut(RutCl)}</p></div>
				</td>
				<td>
					<div className="px-3 valign-wrapper"><p>{FolioB}</p></div>
				</td>
				<td>
					<div className="px-3 valign-wrapper">{Fn.formatDate(FechaB)}</div></td>
			</tr>
		</tbody>
	)
}
