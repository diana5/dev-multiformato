import React from 'react';

export const THeadAjusteDañado = (className, tipoInforme,...props) => {
	return(
		<thead className={className}>

			
			<tr>
				<th>Formato</th>
				<th>Mercado</th>
				<th>
				<div className="justify-content-center"> 
				<a
					className="tooltipped  u-text-gray "
					data-position="top"
					data-tooltip="Local a quien se le imputa la Nota de Crédito"
				>
					<i className="far fa-info-circle"></i>
				</a></div>
					Local Receptor
	
				</th>
				<th>RUT Cliente</th>
				<th>Folio Boleta</th>
				<th>Fecha Boleta</th>
				<th>Formato</th>
				<th>Mercado</th>
				<th>
				<div className="justify-content-center"> 
				<a
					className="tooltipped  u-text-gray"
					data-position="top"
					data-tooltip="Local quien emite la Nota de Crédito"
				>
					<i className="far fa-info-circle"></i>
				</a></div>  
					Local Emisor

				</th>
				<th>RUT Colaborador</th>
				<th>ID Operador</th>
				<th className="leftT">Folio NC</th>
				<th className="leftT">&nbsp;&nbsp;&nbsp;&nbsp;Fecha NC</th>
				<th className="leftT">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; $ NC</th>
				<th className="not leftT">&nbsp; Detalle NC</th>
			</tr>
		</thead>
	)
}
