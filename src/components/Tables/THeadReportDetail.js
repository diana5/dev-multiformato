import React from 'react';

export const THeadReportDetail = (props) => {
	return(
		<thead>
			<tr>
				{
					props.thead.map(item => {
						if (item==="Formato" | item==="Mercado" | item==="Folio NC" | item==="Fecha NC" | item==="Motivo" | item==="EAN" |item==="Rut Cliente")
						return (
							<th  key={item}><p className="px-3" >{item}</p></th>
						)
						else	return (
							<th  key={item}><p className="" >{item}</p></th>
						)
					})
				}
			</tr>
		</thead>
	)
}
