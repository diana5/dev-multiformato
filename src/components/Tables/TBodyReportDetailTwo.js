import React from 'react';
import { Func } from '../../func/func';
import { Fn } from "../../Utils";

export const TBodyReportDetailTwo = (props) => {
	const { detailListProducts, detailListOthers,tipoInforme } = props;

     let montoTot="MONTO TOTAL:"



	
	return (
		<tbody>
			{detailListProducts.map(item => {
				return (
					
					<tr>
						<td className="centerT"> &nbsp; {detailListOthers.formato}</td>
						<td  className="centerT">{detailListOthers.mercado}</td>
						<td  className="centerT"> &nbsp; &nbsp;&nbsp; {detailListOthers.numLocal}</td>
						<td  className="centerT">{Fn.formateaRut(detailListOthers.customerId)}</td>
						<td  className="centerT">&nbsp;&nbsp;{detailListOthers.originalTransaction.opr}</td>
						<td  className="centerT">{detailListOthers.folio}</td>
						<td  className="centerT">{Fn.formatDate(detailListOthers.date)}</td>
						<td>
							{detailListOthers.reasonDescription}
						</td>
						<td className="desc"> &nbsp;{item.description}</td>
						{tipoInforme==="1" &&
						<td>&nbsp;&nbsp;&nbsp;{item.itemNo}</td>}
						<td>&nbsp;&nbsp;&nbsp; {item.deptNbr}</td>
						<td>{item.ean}</td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;{item.grams>0 ?  Fn.formatGrams(item.grams) : item.qty }</td>
						<td>{"$" + props.format(item.total)}</td>

						{/* <td>{detailListProducts.weight ? detailListProducts.grams : 0}</td>
							<td>{"$" + new Intl.NumberFormat().format(detailListProducts.price)}</td> */}
					</tr>
				);
			})}
			<tr>
				<td className="centerT"></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				
				{tipoInforme==="1" &&
				<td></td> }
				<td>
				<p>	<b >MONTO TOTAL:</b></p>{" "}
				</td>
				<td>{" "}
				{"$"+ props.format(detailListOthers.total)} </td>
			</tr>
		</tbody>
	);
}
