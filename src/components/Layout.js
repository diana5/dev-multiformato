import React, { PureComponent } from 'react';
import {
	Switch,
	Route,
	Link,
	Redirect,
	BrowserRouter as Router,
} from 'react-router-dom';
import { Authorized } from "react-authorize";
import './Layout.css';
import { Header } from './Shared/Header/Header';
import { HeaderNav } from './Shared/Header/HeaderNav';
import { Main } from './Shared/Main/Main';
import { Footer } from './Shared/Footer/Footer';
import { FooterTop } from './Shared/Footer/FooterTop';
import { MainTitle } from './Shared/Main/MainTitle';
import { MainTitleLead } from './Shared/Main/MainTitleLead';
import { MainBody } from './Shared/Main/MainBody';
import { Login } from './Views/Login';
import { Boleta } from './Views/Boleta';
import { Product } from './Views/Product';
import { Client } from './Views/Client';
import { Resumen } from './Views/Resumen';

import { PrivateRoute } from './PrivateRoute';
import { Error } from './Error';
import { Note } from './Views/Note';
import { LoginRoute } from './LoginRoute';
import { ReportLocal } from './Views/ReportLocal';
import { ReportCentral } from './Views/ReportCentral';
import { LocalesReport } from './Views/LocalesReport';


const AuthorizedRoute = ({
	component: Component,
	render,
	permissions,
	authority,
	redirectPath,
	...rest
}) => {
	const user = sessionStorage.getItem('user');
		return(
			user
				? (
					<Authorized
						permissions={permissions}
						authority={authority}
						unauthorized={(
							<Route
								{...rest}
								render={() => <Redirect to={{ pathname: redirectPath }} />}
							/>
						)}
					>
						<Route
							{...rest}
							render={props => Component ? <Component {...props} /> : render(props)}
						/>
					</Authorized>
				)
				: (
					<Route {...rest} render={props => (
							<Redirect to={{ pathname: '/', state: { from: props.location } }} />
					)} />
				)
		)

}

const user = JSON.parse(sessionStorage.getItem('user'));

export class Layout extends PureComponent {
	displayName = Layout.name
	state = {
		width: null,
		perfil: user !== null ? user.perfil.toString() : "0",
		height:null
	}
	setWidth = (width,height) => {
		this.setState({
			width: width,
			height: height
		});
	}
	setStateLogin = () => {
		let user = JSON.parse(sessionStorage.getItem('user'));
		this.setState({
			perfil: user.perfil.toString()
		})
	}
	render() {
		return (
			<div>
				<Header>
					<HeaderNav />
				</Header>
				<Main>
					<MainTitle>
						<MainTitleLead />
					</MainTitle>
					<MainBody setWidth={this.setWidth,this.setHeight} widthCard={this.state.width} heightCard={this.state.height}> 
						<Router basename={process.env.PUBLIC_URL}>
							<Switch>
								<LoginRoute
									exact
									path='/'
									component={(props) => <Login {...props} setWidth={this.setWidth} widthCard={this.state.width} setStateLogin={this.setStateLogin} />}
								/>
								<AuthorizedRoute
									permissions="1"
									
									authority={this.state.perfil}
									redirectPath="/401"
									path="/boleta"
									component={ (props) => <Boleta {...props} setWidth={this.setWidth} widthCard={this.state.width} /> }
								/>
										<AuthorizedRoute
									permissions="4"
									
									authority={this.state.perfil}
									redirectPath="/401"
									path="/boleta"
									component={ (props) => <Boleta {...props} setWidth={this.setWidth} widthCard={this.state.width} /> }
								/>
								<AuthorizedRoute
									permissions="1"
									authority={this.state.perfil}
									redirectPath="/401"
									path="/product"
									component={ (props) => <Product {...props} setWidth={this.setWidth} widthCard={this.state.width} /> }
								/>
								<AuthorizedRoute
									permissions="1"
									authority={this.state.perfil}
									redirectPath="/401"
									path="/client"
									component={ (props) => <Client {...props} setWidth={this.setWidth} widthCard={this.state.width} /> }
								/>
								<AuthorizedRoute
									permissions="1"
									authority={this.state.perfil}
									redirectPath="/401"
									path="/note"
									component={ (props) => <Note {...props} setWidth={this.setWidth} widthCard={this.state.width} /> }
								/>
								<AuthorizedRoute
									permissions="2"
									authority={this.state.perfil}
									redirectPath="/401"
									path="/reportlocal"
									component={ () => <ReportLocal setWidth={this.setWidth} /> }
								/>
								<AuthorizedRoute
									permissions="3"
									authority={this.state.perfil}
									redirectPath="/401"
									path="/reportcentral"
									component={ () => <ReportCentral setWidth={this.setWidth} /> }
								/>
								<AuthorizedRoute
									permissions="4"
									authority={this.state.perfil}
									redirectPath="/401"
									path="/localesreport"
									component={ () => <LocalesReport setWidth={this.setWidth} /> }
								/>
								<AuthorizedRoute
									permissions="1"
									authority={this.state.perfil}
									redirectPath="/401"
									path="/resumen"
									component={ () => <Resumen setWidth={this.setWidth} /> }
								/>
								<Route path="/401" component={ () => <Error type="401" message="UPS ! LO SENTIMOS, NO CUENTA CON PERMISOS PARA VER ESTA PÁGINA" /> } />
								<Route component={ () => <Error type="404" message="UPS ! LO SENTIMOS, LA PÁGINA SOLICITADA NO EXISTE" /> } />
							</Switch>
						</Router>
						
					</MainBody>
				</Main>
				<Footer>
					<FooterTop />
				
				</Footer>
			</div>
		)
	}
}
