import React from 'react';
import { Route, Redirect } from 'react-router-dom';

// const user = JSON.parse(sessionStorage.getItem('user'));

export const PrivateRoute = ({ component: Component, ...rest }) => {

	return(
		<Route {...rest} render={props => (
			sessionStorage.getItem('user')
				? <Component {...props} />
				: <Redirect to={{ pathname: '/', state: { from: props.location } }} />
		)} />
	)
}
