﻿import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import brandLider from '../../../images/brand-lider.png';
import brandExpress from '../../../images/logo-express.svg';
import brandAcuenta from '../../../images/logo-acuenta.svg';
import brandEkono from '../../../images/logo-lidercl.svg';

export class FooterTop extends Component {
	displayName = FooterTop.name

	render() {
		return (
			<section id="FooterTop" className="Footer-FooterTop FooterTop ">
				<section className="container">
					<section className="row">
						<article className="col">
							<div className="center-align">
								<Link to="https://lider.cl/" rel="external" target="_Blank">
									<img src={brandLider} alt="Lider" width={120} />
								</Link>
							</div>
						</article>
						<article className="col">
							<div className="center-align">
								<Link to="https://lider.cl/" rel="external" target="_Blank">
									<img src={brandExpress} alt="Express" width={120} />
								</Link>
							</div>
						</article>
						<article className="col">
							<div className="center-align">
								<Link to="https://www.superbodegaacuenta.cl/" rel="external" target="_Blank">
									<img src={brandAcuenta} alt="Acuenta" width={120} />
								</Link>
							</div>
						</article>
						<article className="col">
							<div className="center-align">
								<Link to="https://www.ekono.cl/" rel="external" target="_Blank">
									<img src={brandEkono} alt="Ekono" width={120} />
								</Link>
							</div>
						</article>
					</section>
				</section>
			</section>
		)
	}
}
