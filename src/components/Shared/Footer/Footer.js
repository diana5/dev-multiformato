﻿import React, { Component } from 'react';

export class Footer extends Component {
	displayName = Footer.name

	render() {
		return (
			<footer id="Footer" className="Footer page-footer transparent">
				{this.props.children}
			</footer>
		)
	}
}
