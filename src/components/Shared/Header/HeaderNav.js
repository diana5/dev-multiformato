import React, { Component } from 'react';
import brandWalmart from '../../../images/brand-walmart.png';
import { handleClickAllLogout } from '../../../Utils';
import { Func } from '../../../func/func';
import './HeaderNav.css';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import PopupState, { bindTrigger, bindMenu } from 'material-ui-popup-state/index';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';

export class HeaderNav extends Component {
	displayName = HeaderNav.name

	render() {
		let user = Func.getUser();
		return (
			
			<section id="Nav" className="Header-Nav Nav">
				<nav className="container-fluid pr-0">
					<div className="nav-wrapper">
					<div className="right hide-on-med-and-down brand-logo">
						
			
							{
							user &&
						<Button variant="contained" className="welcome" >
							
					Bienvenid@ &nbsp; <div>{user.nombre}  </div>
						</Button>}
						{
							user &&
						<Button>
							<a onClick={handleClickAllLogout}><ExitToAppIcon className="whiteIcon" /></a>

						</Button>}


				
				
				</div>
						<a href="#!" className="brand-logo hide-on-med-and-down"><img src={brandWalmart} className="responsive-img" alt="Walmart" width={160} style={{width:'20%'}} /></a>
						<div className="devTitle"><h1 className="center-align m-0 h5 ">Devolución Multiformato</h1></div>
						
							
						
		

					</div>
				</nav>
			</section>
		)
	}
}
