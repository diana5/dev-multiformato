﻿import React, { Component } from 'react';

export class Header extends Component {
	displayName = Header.name

	render() {
		return (
			<header id="Header" className="Header">
				{this.props.children}
			</header>
		)
	}
}
