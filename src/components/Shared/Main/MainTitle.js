import React, { Component } from 'react';

export class MainTitle extends Component {
	displayName = MainTitle.name

	render() {
		return (
			<section id="MainTitle" className="MainTitle Main-MainTitle">
				<section className="">
					<section className="row justify-content-center">
						<article className="col-12 col-md-12 white-text py-4">
							{this.props.children}
						</article>
					</section>
				</section>
			</section>
		)
	}
}
