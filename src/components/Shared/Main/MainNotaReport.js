import React from 'react';
import sparkMitad from '../../../images/spark-mitad.svg';
import './MainCardLead.css';


export default function MainNotaReport(props) {
	return (
		<div className="row mb-0">
            <div className="col-lg-5"></div>
			<div className="col-lg-4">
				<div className="Lead ">
					<div className="Lead-Spark">
						<p className={props.size}><div className="fontS">{props.title}</div></p>
						{props.spark && <img src={sparkMitad} alt="Spark Walmart" className="Lead-Spark-img" />}
					</div>
				</div>
			</div>
            <div className="col-lg-3"></div>
            <div className="col-lg-12"><hr></hr></div>
		</div>
	)
}
