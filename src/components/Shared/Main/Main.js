﻿import React, { Component } from 'react';

export class Main extends Component {
	displayName = Main.name

	render() {
		return (
			<main id="Main" className="Main">
				{this.props.children}
			</main>
		)
	}
}
