import React from 'react';
import sparkMitad from '../../../images/spark-mitad.svg';
import './MainCardLead.css';


export default function MainCardLead(props) {
	return (
		<div className="row mb-0">
			<div className={props.columna}>
				<div className="Lead mb-9">
					<div className={props.columna+" Lead-Spark"}>
						<p className={props.size}>{props.title}</p>
						{props.spark && <img src={sparkMitad} alt="Spark Walmart" className="Lead-Spark-img" />}
						<span></span>
					</div>
				</div>
			</div>
		</div>
	)
}
