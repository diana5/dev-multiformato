import React, { Component } from 'react';

export class MainBody extends Component {
	displayName = MainBody.name

	render() {
		let width = this.props.widthCard;
		let height= this.props.heightCard
		return (
			<section id="MainBody" className="MainBody Main-MainBody">
				<section className="">
					<section className="justify-content-center centrar">
						<div className={width}> 
							<div className="">
								{this.props.children}
							</div>
						</div>
					</section>
				</section>
			</section>
		)
	}
}
