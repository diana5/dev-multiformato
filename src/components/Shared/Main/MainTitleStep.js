import React, { Component } from 'react';

export class MainTitleStep extends Component {
	displayName = MainTitleStep.name

	render() {
		return (
			<div>
				<div id="Step" className="Step d-flex justify-content-center">
					<div className="Step-Link active">1</div>
					<div className="Step-Link">2</div>
					<div className="Step-Link">3</div>
					<div className="Step-Link">4</div>
				</div>
			</div>
		)
	}
}
