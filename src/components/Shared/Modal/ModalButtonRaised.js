import React from 'react';

export default function ModalButtonRaised(props) {
	return (
		<a style={props.style} className={
			'cerrar modal-action modal-close waves-effect closeButton'
			+ (props.className ? props.className : '')
		} onClick={props.handleClick}>{props.name}</a>
	)
}
