import React from 'react';

export default function ModalMain(props) {
	return (
		<div className="modal-content center">
			{props.children}
		</div>
	)
}
