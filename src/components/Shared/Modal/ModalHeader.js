import React from 'react';
import MainNotaReport from '../../Shared/Main/MainNotaReport';


export default function ModalHeader(props) {
	return (
		<div className={"Card-Main card-content  pt-4  cardNote" + props.color}>
			<MainNotaReport title={props.title}  spark="true"  />
		</div>
	)
}
