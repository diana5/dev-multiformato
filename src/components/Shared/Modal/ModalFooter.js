import React from 'react';

export default function ModalFooter(props) {
	return (
		<div className="modal-footer  center-align">
			{/* <a href="#!" className="Raised modal-action modal-close waves-effect btn btn-large">Cerrar</a> */}
			{props.children}
			
		</div>
	)
}
