import React from 'react';
import './Modal.css';

export default function Modal(props) {
	return (
		<div id={props.id ? props.id : 'modal1'} className={"modal " + props.size + ' ' + props.otherClass + ' ' +(props.Develop ? " modal-view-onlyDevelop":"")}>
			{props.children}
		</div>
	)
}
