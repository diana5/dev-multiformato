import React, { PureComponent } from 'react';
import { Form, Formik } from 'formik';
import InputField from '../Ui/Forms/InputField';
import InputFieldAutocomplete from '../Ui/Forms/InputFieldAutocomplete';
import conf from '../../config.json';
import './eye.module.css';

const user = JSON.parse(sessionStorage.getItem('user'));

export class Eye extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      hidden: true,
      className: props.className
    };
    this.toggleShow = this.toggleShow.bind(this);

    
  }

  componentDidMount() {
    console.log('all props', this.props);
    console.log(document.getElementById("password").nodeValue)

  }

  toggleShow() { 
    this.setState(prevState => ({ 
      hidden: ! prevState.hidden

     }));
    console.log(this.state)
  }

  toggleHide() {
    this.setState({ hidden: true ,  shown: false});
    console.log(this.state)
  }

	displayName = Eye.name
	state = {
		width: null,
		perfil: user !== null ? user.perfil.toString() : "0"
  }

  

  render(){
    
    
    return (
      <div class="row">
      <div class="col-12">
    	<InputField
								id="password"
								type={this.state.hidden ? "password" : "text"}
								name="password"
								label="Contraseña"
								autoComplete="current-password"
                maxLength="20"
                className={this.props.className}
                 {...this.props} /> { this.state.hidden ? <i class="fas fa-eye-slash" onClick={this.toggleShow} ></i>: true}
                 { !this.state.hidden ? <i class="fas fa-eye" onClick={this.toggleShow} ></i>: true }
              </div></div>
  )}

 

}