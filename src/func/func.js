export const Func = {




	getSelector : (inLocal, otherLocal) => {
		let selector = null;
		if (inLocal && !otherLocal)
			return selector = 1
		else if (otherLocal && !inLocal)
			return selector = 2
		else if (inLocal && otherLocal)
			return selector = 0
	},
	getUser : () => {
		return JSON.parse(sessionStorage.getItem('user'));
	},
	formatRut : (rut) => {
		var actual = rut.replace(/^0+/, "");
		if (actual !== '' && actual.length > 1) {
			var sinPuntos = actual.replace(/\./g, "");
			var actualLimpio = sinPuntos.replace(/-/g, "");
			var inicio = actualLimpio.substring(0, actualLimpio.length - 1);
			var rutPuntos = "";
			var i = 0;
			var j = 1;
			for (i = inicio.length - 1; i >= 0; i--) {
				var letra = inicio.charAt(i);
				rutPuntos = letra + rutPuntos;
				if (j % 3 === 0 && j <= inicio.length - 1) {
					rutPuntos = "." + rutPuntos;
				}
				j++;
			}
			var dv = actualLimpio.substring(actualLimpio.length - 1);
			rutPuntos = rutPuntos + "-" + dv;
		}
		return rutPuntos;
	},
	formatDate : function(date) {
		let year = date.substr(0, 4);
		let month = date.substr(4, 2);
		let day = date.substr(6, 2);
		return day + '/' + month + '/' + year
	},



	
}
