import 'materialize-css/dist/css/materialize.css';
import 'bootstrap/dist/css/bootstrap-grid.css';
import '@fortawesome/fontawesome-pro/css/all.css';
import 'materialize-css';
import './index.css';
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import WebFont from 'webfontloader';

WebFont.load({
  google: {
    families: ['Roboto:300,400,700', 'sans-serif']
  }
});


const baseUrl = document.getElementsByTagName('base')[0].getAttribute('href');
const rootElement = document.getElementById('root');

ReactDOM.render(
    <BrowserRouter basename={baseUrl}>
        <App />
    </BrowserRouter>,
    rootElement);

    if (module.hot) {
        module.hot.accept()
    }

registerServiceWorker();
