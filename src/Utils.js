export var Fn = {
	toCurrency: number => {
		if (typeof number === undefined) {
			return 0;
		}
		const string = `${number}`;
		const format = string.replace(/\B(?=(\d{3})+(?!\d))/g, ".");
		return `$${format}`;
	},
	validaRut: function(rutCompleto) {
		rutCompleto = rutCompleto.split(".").join("");
		rutCompleto = rutCompleto.replace(".", "");

		if (!/^[0-9]+[-|‐]{1}[0-9kK]{1}$/.test(rutCompleto)) return false;
		var tmp = rutCompleto.split("-");
		var digv = tmp[1];
		var rut = tmp[0];
		if (digv == "K") digv = "k";
		return Fn.dv(rut) == digv;
	},
	dv: function(T) {
		var M = 0,
			S = 1;
		for (; T; T = Math.floor(T / 10))
			S = (S + (T % 10) * (9 - (M++ % 6))) % 11;
		return S ? S - 1 : "k";
	},
	separator: function(rut) {
		rut = rut.replace(".", "");
		rut = rut.replace(".", "");
		let tmp = rut.split("-");
		let rutSinDv = tmp[0];
		return rutSinDv;
	},
	keyPrevent: function(event) {
		var x = event.keyCode;
		if (
			x == 69 ||
			x == 186 ||
			x == 187 ||
			x == 189 ||
			x == 190 ||
			x == 188
		) {
			event.preventDefault();
		}

		var max = 4;
		var value = event.target.value;

		if (value.length >= max) {
			event.target.value = event.target.value.substr(0, max);
		}
	},
	keyPreventTrans: function(event) {
		var x = event.keyCode;
		if (
			x == 69 ||
			x == 186 ||
			x == 187 ||
			x == 189 ||
			x == 190 ||
			x == 188
		) {
			event.preventDefault();
		}

		var max = 3;
		var value = event.target.value;

		if (value.length >= max) {
			event.target.value = event.target.value.substr(0, max);
		}
	},
	keyPreventCaja: function(event) {
		var x = event.keyCode;
		if (
			x == 69 ||
			x == 186 ||
			x == 187 ||
			x == 189 ||
			x == 190 ||
			x == 188
		) {
			event.preventDefault();
		}

		var max = 2;
		var value = event.target.value;

		if (value.length >= max) {
			event.target.value = event.target.value.substr(0, max);
		}
	},
	formateaRut: function(rut) {
		console.log("RutDentero", rut);
		if (rut != undefined) {
			var actual = rut.replace(/^0+/, "");
			if (actual != "" && actual.length > 1) {
				var sinPuntos = actual.replace(/\./g, "");
				var actualLimpio = sinPuntos.replace(/-/g, "");
				var inicio = actualLimpio.substring(0, actualLimpio.length - 1);
				var rutPuntos = "";
				var i = 0;
				var j = 1;
				for (i = inicio.length - 1; i >= 0; i--) {
					var letra = inicio.charAt(i);
					rutPuntos = letra + rutPuntos;
					if (j % 3 == 0 && j <= inicio.length - 1) {
						rutPuntos = "." + rutPuntos;
					}
					j++;
				}
				var dv = actualLimpio.substring(actualLimpio.length - 1);
				rutPuntos = rutPuntos + "-" + dv;
			}
			return rutPuntos;
		}
	},
	formatDate: function(d) {
		let year = d.substr(0, 4);
		let month = d.substr(4, 2);
		let day = d.substr(6, 2);
		return day + "/" + month + "/" + year;
	},

	formatGrams:function (grams) {
		console.log("entra a la funcion")
		let gramos=""
		if (grams<1000)
		return	gramos="0."+grams
		 else
		if((grams>0) && (grams>1000))
		return	gramos=String(grams).replace(/(.)(?=(\d{3})+$)/g, "$1,").replace(",",".") 

		
		
	},

};

export const handleClickLogout = props => {
	sessionStorage.removeItem("user");
	props.history.push({
		pathname: "/"
	});
};

export const handleClickAllLogout = props => {
	sessionStorage.removeItem("user");
	window.location.reload(true);
};

export const handleClickNote = (props, estado) => {
	props.router.push({
		pathname: "/note",
		state: estado
	});
};
